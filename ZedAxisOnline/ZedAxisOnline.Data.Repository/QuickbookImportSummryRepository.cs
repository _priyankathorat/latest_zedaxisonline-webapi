﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ZedAxisOnline.BusinessObjects.QuickBook;
using ZedAxisOnline.BusinessObjects.User;
using ZedAxisOnline.Data.Abstraction;
using ZedAxisOnline.Data.EF;
using ZedAxisOnline.Data.EF.Models;

namespace ZedAxisOnline.Data.Repository
{
  public  class QuickbookImportSummaryRepository : IQuickbookImportSummaryRepository
    {
        private readonly ApplicationDbContext _applicationDbContext;

        public QuickbookImportSummaryRepository(ApplicationDbContext applicationDbContext)
        {
            this._applicationDbContext = applicationDbContext;
        }

       
        public QuickbookImportSummaryEntity ToBusinessObject(QuickbookImportSummary obj)
        {
            if (obj == null)
                return null;
            QuickbookImportSummaryEntity quickBookEntity = new QuickbookImportSummaryEntity();
            quickBookEntity.MappingId = obj.MappingId;
            quickBookEntity.IsImported = obj.IsImported;
            quickBookEntity.TransactionId = obj.TransactionId;
            quickBookEntity.ImportType = obj.ImportType;
            quickBookEntity.IsShow = obj.IsShow;
            quickBookEntity.Status = obj.Status;
            quickBookEntity.Error = obj.Error;
            quickBookEntity.Status = obj.Status;
            quickBookEntity.ViewUrl = obj.ViewUrl;
            return quickBookEntity;
        }

        public QuickbookImportSummary ToDataObject(QuickbookImportSummaryEntity obj)
        {
            if (obj == null)
                return null;
            QuickbookImportSummary quickBookEntity = new QuickbookImportSummary();
            quickBookEntity.UserId = obj.UserId;
            quickBookEntity.MappingId = obj.MappingId;
            quickBookEntity.TransactionId = obj.TransactionId;
            quickBookEntity.Message = obj.Message;
            quickBookEntity.IsImported = obj.IsImported;
            if (quickBookEntity.TransactionId != null && quickBookEntity.TransactionId != "")
            {
                quickBookEntity.ViewUrl = "https://app.sandbox.qbo.intuit.com/app/invoice" + "?txnId=" + quickBookEntity.TransactionId;
            }
            quickBookEntity.Error = obj.Error;
            quickBookEntity.ImportType = obj.ImportType;
            quickBookEntity.IsShow = true;
            return quickBookEntity;
        }
        public async Task<QuickbookImportSummaryEntity> GetQuickbookImportSummaryEntity(int Id)
        {
            var user = await this._applicationDbContext.QuickbookImportSummary.Include(x=>x.Mapping).ThenInclude(x=>x.QuickbookTransactionType).Where(x => x.Id == Id).FirstOrDefaultAsync();
           return ToBusinessObject(user);
        }


        public async Task<QuickbookImportSummaryEntity> CreateQuickBookImportSummaryAsync(QuickbookImportSummaryEntity quickBookEntity)
        {
            var Connection = ToDataObject(quickBookEntity);
            _applicationDbContext.QuickbookImportSummary.Add(Connection);
            await _applicationDbContext.SaveChangesAsync();
            return await GetQuickbookImportSummaryEntity(Connection.Id);
        }
    }
}
