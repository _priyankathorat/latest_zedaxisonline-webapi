﻿using AutoMapper;
using System.Collections.Generic;
using ZedAxisOnline.BusinessObjects.Connection;
using ZedAxisOnline.BusinessObjects.QuickBook;
using ZedAxisOnline.BusinessObjects.User;
using ZedAxisOnline.Data.EF.Models;

namespace ZedAxisOnline.Data.Repository.Automapper
{
    public class AutoMapperProfiles: Profile
    {
        public AutoMapperProfiles()
        {
            CreateMap<User, UserEntity>();

            CreateMap<AmazonConnection, AmazonConnectionEntity>();
            CreateMap<AmazonConnectionEntity, AmazonConnection>();
            CreateMap<List<AmazonConnection>, List<AmazonConnectionEntity>>();

            CreateMap<List<ZohoConnection>, List<ZohoConnectionEntity>>();
            CreateMap<List<ZohoConnectionEntity>, List<ZohoConnection>>();
            CreateMap<ZohoConnectionEntity, ZohoConnection>();
            CreateMap<ZohoConnection, ZohoConnectionEntity>();
            CreateMap<List<ZohoConnection>, List<ZohoConnectionEntity>>();
            CreateMap<List<ZohoConnectionEntity>, List<ZohoConnection>>();
            
            CreateMap<ZohoInvoice, ZohoInvoiceEntity>();
            CreateMap<ZohoInvoiceItem, ZohoInvoiceItemEntity>();
            CreateMap < List<ZohoInvoice>, List<ZohoInvoiceEntity>>();
            CreateMap <ZohoInvoiceEntity, ZohoInvoice>();
            CreateMap<ZohoInvoiceItemEntity, ZohoInvoiceItem>();
            

             CreateMap<QuickbookTransactionType, QuickbookTransactionTypeEntity>();
            CreateMap<QuickbookTransactionTypeEntity, QuickbookTransactionType>();
            CreateMap<List<QuickbookTransactionType>, List<QuickbookTransactionTypeEntity>>();
            CreateMap<SaveSettingEntity,QuickbookAccountSetting>();
            CreateMap<QuickbookAccountSetting,SaveSettingEntity>();
            

            CreateMap<ConnectionTransactionColumn, ConnectionTransactionColumnEntity>();
            CreateMap<ConnectionTransactionColumnEntity, ConnectionTransactionColumn>();
            CreateMap<List<ConnectionTransactionColumn>, List<ConnectionTransactionColumnEntity>>();

            CreateMap<SaveMappingEntity, Mapping>();
            CreateMap<Mapping, SaveMappingEntity > ();
        }
    }
}
