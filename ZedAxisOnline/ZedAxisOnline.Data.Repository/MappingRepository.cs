﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ZedAxisOnline.BusinessObjects.Connection;
using ZedAxisOnline.BusinessObjects.QuickBook;
using ZedAxisOnline.Configuration;
using ZedAxisOnline.Data.Abstraction;
using ZedAxisOnline.Data.EF;
using ZedAxisOnline.Data.EF.Models;

namespace ZedAxisOnline.Data.Repository
{
    public class MappingRepository : IMappingRepository
    {
        private readonly ApplicationDbContext _applicationDbContext;
        private readonly IMapper _mapper;

        public MappingRepository(ApplicationDbContext applicationDbContext, IMapper mapper)
        {
            _applicationDbContext = applicationDbContext;
            _mapper = mapper;
        }

        public async Task<Mapping>  ToDataObjectForMapping(SaveMappingEntity SaveMappingEntity)
        {

            var mappingEntry = _mapper.Map<Mapping>(SaveMappingEntity);
           
            return mappingEntry;
        }

        public async Task<MappingDetail> ToDataObjectForMappingDetails(MappedFieldEntity mappedFieldEntity,int mappingId)
        {

            var mappingDetailEntry = _mapper.Map<MappingDetail>(mappedFieldEntity);
            await _applicationDbContext.MappingDetail.AddAsync(mappingDetailEntry);
            await _applicationDbContext.SaveChangesAsync();
            return mappingDetailEntry;
        }

       


        public async Task<List<DataForMapppingEntity>> GetDataForMappping(int qboTransactionTypeId)
        {
            List<DataForMapppingEntity> listDataForMapppingEntity = new List<DataForMapppingEntity>();

            var ConTransCol = await _applicationDbContext.QuickbookTransactionColumn.Where(x => x.QuickbookTransactionTypeId == qboTransactionTypeId).ToListAsync();

            if (ConTransCol != null)
            {
                foreach (var item in ConTransCol)
                {
                    DataForMapppingEntity DataForMappping = new DataForMapppingEntity();
                    DataForMappping.Id = item.Id;
                    DataForMappping.QBOTransColumnName = item.Name;
                    DataForMappping.IsMapped = 0;
                    DataForMappping.SelectedItem = MessagesConstant.SelectFieldToMap;
                    DataForMappping.SelectedFunction = MessagesConstant.SelectFunction;
                    listDataForMapppingEntity.Add(DataForMappping);
                }
            }
            return listDataForMapppingEntity;
        }

        public async Task<bool> IsMappingNameValid(string mappingName, int userId)
        {
            var MappingName = await _applicationDbContext.Mapping.Where(x => x.Name == mappingName && x.UserId == userId).FirstOrDefaultAsync();
            if (MappingName == null)
                return true;
            return false;
        }

        public async Task<int> SaveMapping(SaveMappingEntity saveMappingEntity)
        {
            var mappingObject = await _applicationDbContext.Mapping.Where(x => x.UserId == saveMappingEntity.UserId && x.Name == saveMappingEntity.Name  && x.ConnectionTransactionTypeId == saveMappingEntity.ConnectionTransactionTypeId && x.QuickbookTransactionTypeId == saveMappingEntity.QuickbookTransactionTypeId).FirstOrDefaultAsync();
            if (mappingObject == null)
            {
                mappingObject = _mapper.Map<Mapping>(saveMappingEntity);
                mappingObject.CreatedOn = DateTime.Now;
                mappingObject.ModifiedOn = DateTime.Now;
                await _applicationDbContext.Mapping.AddAsync(mappingObject);
                await _applicationDbContext.SaveChangesAsync();
                foreach (var item in saveMappingEntity.MappedField)
                {
                    MappingDetail mappingDetail = new MappingDetail();
                    mappingDetail.ConnectionTransactionColumnId = item.ConnectionTransColumnId;
                    mappingDetail.ConstantValue = item.ConstantValue;
                    mappingDetail.MappingFunctionId = item.SelectedFunctionId;
                    mappingDetail.QuickbookTransactionColumnId = item.QBOTransColumnId;
                    mappingDetail.MappingId = mappingObject.Id;
                    await _applicationDbContext.MappingDetail.AddAsync(mappingDetail);
                }
                await _applicationDbContext.SaveChangesAsync();
            }
            else
            {
                mappingObject.ModifiedOn = DateTime.Now;
                var MappingDetailsList = await _applicationDbContext.MappingDetail.Where(x => x.MappingId == mappingObject.Id).ToListAsync();
                foreach (var item in MappingDetailsList)
                {
                    _applicationDbContext.MappingDetail.Remove(item);
                }
                await _applicationDbContext.SaveChangesAsync();
                foreach (var item in saveMappingEntity.MappedField)
                {
                    MappingDetail mappingDetail = new MappingDetail();
                    mappingDetail.ConnectionTransactionColumnId = item.ConnectionTransColumnId;
                    mappingDetail.ConstantValue = item.ConstantValue;
                    mappingDetail.MappingFunctionId = item.SelectedFunctionId;
                    mappingDetail.QuickbookTransactionColumnId = item.QBOTransColumnId;
                    mappingDetail.MappingId = mappingObject.Id;
                    await _applicationDbContext.MappingDetail.AddAsync(mappingDetail);
                }
                await _applicationDbContext.SaveChangesAsync();
            }
            return mappingObject.Id;
        }

        public async Task<List<ConnectionTransactionType>> GetConnectionTransactionType(int conTypeId)
        {
            var ConTranstypes = await _applicationDbContext.ConnectionTransactionType.Where(x => x.ConnectionTypeId == conTypeId).ToListAsync();
            if (ConTranstypes == null)
                return null;
            return ConTranstypes;

        }
        public async Task<List<QuickbookTransactionColumn>> GetQuickbookTransactionColumn(int qboTransTypeId)
        {
            var QBOTransCol = await _applicationDbContext.QuickbookTransactionColumn.Where(x => x.QuickbookTransactionTypeId == qboTransTypeId).ToListAsync();
            if (QBOTransCol == null)
                return null;
            return QBOTransCol;
        }

        

        public async Task<Mapping> FindMappingByIdAsync(int id)
        {
            return await _applicationDbContext.Mapping.Include(x => x.MappingDetails).ThenInclude(x => x.QuickbookTransactionColumn).Include(x => x.MappingDetails).ThenInclude(x => x.ConnectionTransactionColumn).FirstOrDefaultAsync(x => x.Id == id);
        }

        public async Task<IList<ConnectionTransactionColumnEntity>> GetConnectionTransactionColumnsAsync(int connectionTypeId)
        {
            var list = await _applicationDbContext.ConnectionTransactionColumn.Where(x => x.ConnectionTransactionTypeId == connectionTypeId).ToListAsync();
            var datalist =  _mapper.Map<IList<ConnectionTransactionColumn>, IList<ConnectionTransactionColumnEntity>>(list);
            return datalist;
        }
    }
}
