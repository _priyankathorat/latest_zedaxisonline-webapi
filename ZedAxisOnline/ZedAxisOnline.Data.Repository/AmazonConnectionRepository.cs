﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ZedAxisOnline.BusinessObjects.Connection;
using ZedAxisOnline.BusinessObjects.User;
using ZedAxisOnline.Data.Abstraction;
using ZedAxisOnline.Data.EF;
using ZedAxisOnline.Data.EF.Models;

namespace ZedAxisOnline.Data.Repository
{
    public class AmazonConnectionRepository : IAmazonConnectionRepository
    {
        private readonly ApplicationDbContext _applicationDbContext;
        private readonly IMapper _mapper;

        public AmazonConnectionRepository(ApplicationDbContext applicationDbContext, IMapper mapper)
        {
            _applicationDbContext = applicationDbContext;
            _mapper = mapper;
        }

        public async Task<IList<AmazonConnectionEntity>> GetAmazonConnections(int userId)
        {
            var list = await _applicationDbContext.AmazonConnection.Where(x => x.UserId == userId).ToListAsync();
            var dataList = _mapper.Map<IList<AmazonConnection>, IList<AmazonConnectionEntity>>(list);
            return dataList;
        }

        public async Task SaveOrUpdateAmazonConnection(AmazonConnectionEntity entity)
        {
            var dataObject = await _applicationDbContext.AmazonConnection.FirstOrDefaultAsync(x => x.AmazonUserId == entity.AmazonUserId && x.UserId == entity.UserId);
            if (dataObject == null)
            {
                dataObject = _mapper.Map<AmazonConnection>(entity);
                _applicationDbContext.AmazonConnection.Add(dataObject);
            }
            else
            {
                dataObject.AccessToken = entity.AccessToken;
                dataObject.ModifiedOn = DateTime.Now;
            }
            await _applicationDbContext.SaveChangesAsync();
        }
    }
}