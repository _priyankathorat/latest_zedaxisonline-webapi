﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ZedAxisOnline.BusinessObjects.Connection;
using ZedAxisOnline.BusinessObjects.User;
using ZedAxisOnline.Data.Abstraction;
using ZedAxisOnline.Data.EF;
using ZedAxisOnline.Data.EF.Models;

namespace ZedAxisOnline.Data.Repository
{
    public class ZohoConnectionRepository : IZohoConnectionRepository
    {
        private readonly ApplicationDbContext _applicationDbContext;
        private readonly IMapper _mapper;

        public ZohoConnectionRepository(ApplicationDbContext applicationDbContext, IMapper mapper)
        {
            _applicationDbContext = applicationDbContext;
            _mapper = mapper;
        }



        public async Task AddZohoConnectionAsync(ZohoConnectionEntity objectToSave)
        {
            var zohoConnection = _mapper.Map<ZohoConnection>(objectToSave);
            var record = await _applicationDbContext.ZohoConnection.FirstOrDefaultAsync(x => x.AuthToken == zohoConnection.AuthToken && x.UserId == zohoConnection.UserId);
            if(record == null)
            {
                _applicationDbContext.ZohoConnection.Add(zohoConnection);
                await _applicationDbContext.SaveChangesAsync();
            }
        }

        public async Task<IList<ZohoConnectionEntity>> GetZohoConnectionsAsync(int userId)
        {
            var list = await _applicationDbContext.ZohoConnection.Where(x => x.UserId == userId).ToListAsync();
            var dataList = _mapper.Map<IList<ZohoConnection>, IList<ZohoConnectionEntity>>(list);
            return dataList;
        }

        public Task<bool> IsZohoConnectionExists(string authToken, int userId)
        {
            var result = _applicationDbContext.ZohoConnection.AnyAsync(x => x.AuthToken == authToken && x.UserId == userId);
            return result;
        }

        public async Task UpdateWebhookUrlAsync(int id, string webhookUrl, int userId)
        {
            var connection = await _applicationDbContext.ZohoConnection.FirstAsync(x => x.Id == id && x.UserId == userId);
            connection.WebhookUrl = webhookUrl;
            await _applicationDbContext.SaveChangesAsync();
        }

        public async Task<ZohoConnectionDetailsEntity> FindById(int id)
        {
            var connection = await _applicationDbContext.ZohoConnection.FirstOrDefaultAsync(x => x.Id == id);
            return ToBusinessObject(connection);
        }

        public async Task<IList<ZohoInvoiceEntity>> FindInvoiceByMappingId(int mappingId)
        {
            var connection = await _applicationDbContext.ZohoInvoice.Where(x => x.MappingId== mappingId).ToListAsync();
            return ToZohoInvoiceListEntity(connection);
        }

        public async Task<int> AddZohoInvoiceAsync(ZohoInvoiceEntity zohoInvoiceEntity)
        {
            ZohoInvoice zohoInvoice= ToZohoInvoiceDataObject(zohoInvoiceEntity);
              _applicationDbContext.ZohoInvoice.Add(zohoInvoice);
            await _applicationDbContext.SaveChangesAsync();
            return zohoInvoice.Id;
        }

        public async Task<int> AddZohoInvoiceItemAsync(ZohoInvoiceItemEntity zohoInvoiceEntity)
        {
            ZohoInvoiceItem zohoInvoice = ToZohoInvoiceItemDataObject(zohoInvoiceEntity);
            _applicationDbContext.ZohoInvoiceItem.Add(zohoInvoice);
            await _applicationDbContext.SaveChangesAsync();
            return zohoInvoice.Id;
        }

        public ZohoConnectionDetailsEntity ToBusinessObject(ZohoConnection obj)
        {
            if (obj == null)
                return null;
            ZohoConnectionDetailsEntity ZohoEntity = new ZohoConnectionDetailsEntity();
            ZohoEntity.AuthToken = obj.AuthToken;
            ZohoEntity.OrganizationId = obj.OrganizationId;
            return ZohoEntity;
        }

        public IList<ZohoInvoiceEntity> ToZohoInvoiceListEntity(List<ZohoInvoice> obj)
        {
            if (obj == null)
                return null;
            var dataList = _mapper.Map<IList<ZohoInvoice>, IList<ZohoInvoiceEntity>>(obj);
            return dataList;
        }

        public ZohoInvoice ToZohoInvoiceDataObject(ZohoInvoiceEntity obj)
        {
            if (obj == null)
                return null;
            var dataList = _mapper.Map<ZohoInvoiceEntity, ZohoInvoice>(obj);
            return dataList;
        }

        public ZohoInvoiceItem ToZohoInvoiceItemDataObject(ZohoInvoiceItemEntity obj)
        {
            if (obj == null)
                return null;
            var dataList = _mapper.Map<ZohoInvoiceItemEntity, ZohoInvoiceItem>(obj);
            return dataList;
        }
    }
}