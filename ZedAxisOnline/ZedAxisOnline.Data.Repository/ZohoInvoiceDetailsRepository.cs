﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ZedAxisOnline.BusinessObjects.Connection;
using ZedAxisOnline.Data.Abstraction;
using ZedAxisOnline.Data.EF;
using ZedAxisOnline.Data.EF.Models;

namespace ZedAxisOnline.Data.Repository
{
    public class ZohoInvoiceDetailsRepository :  IZohoInvoiceDetailsRepository
    {
        private readonly ApplicationDbContext _applicationDbContext;
        private readonly IMapper _mapper;

        public ZohoInvoiceDetailsRepository(ApplicationDbContext applicationDbContext, IMapper mapper)
        {
            _applicationDbContext = applicationDbContext;
            _mapper = mapper;
        }

           public async Task<List<ZohoInvoice>> GetZohoInvoiceDetails(int MappingId, bool isImported = false)
        {
            return await _applicationDbContext.ZohoInvoice.Include(x => x.ZohoInvoiceItems).Where(x=>x.MappingId == MappingId && x.IsImported == isImported).ToListAsync();
        }

        //public async Task GetZohoInvoiceDetails(ZohoConnectionEntity objectToSave)
        //{
        //    var zohoConnection = _mapper.Map<ZohoInvoice>(objectToSave);
        //    _applicationDbContext.ZohoInvoice.Add(zohoConnection);
        //    await _applicationDbContext.SaveChangesAsync();
        //}

        public async Task<List<ZohoConnectionEntity>> GetZohoConnectionsAsync(int mappingId)
        {
            var list = await _applicationDbContext.ZohoInvoice.Where(x => x.MappingId == mappingId).ToListAsync();
            var dataList = _mapper.Map<List<ZohoConnectionEntity>>(list);
            return dataList;
        }

        public async Task<List<ZohoInvoiceEntity>> GetZohoInoviceListAsync(int mappingId)
        {
            var list = await _applicationDbContext.ZohoInvoice.Where(x => x.MappingId == mappingId).ToListAsync();
            var dataList = _mapper.Map<List<ZohoInvoiceEntity>>(list);
            return dataList;
        }
    }
}