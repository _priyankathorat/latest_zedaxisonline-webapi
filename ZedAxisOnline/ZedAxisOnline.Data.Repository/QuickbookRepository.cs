﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ZedAxisOnline.BusinessObjects.QuickBook;
using ZedAxisOnline.Data.Abstraction;
using ZedAxisOnline.Data.EF;
using ZedAxisOnline.Data.EF.Models;

namespace ZedAxisOnline.Data.Repository
{
    public class QuickbookRepository : IQuickbookRepository
    {
        private readonly ApplicationDbContext _applicationDbContext;
        private readonly IMapper _mapper;

        public QuickbookRepository(ApplicationDbContext applicationDbContext, IMapper mapper)
        {
            this._applicationDbContext = applicationDbContext;
            this._mapper = mapper;
        }

        public UserQuickbookConnection ToDataObject(QuickBookEntity obj)
        {
            return new UserQuickbookConnection()
            {
                RefreshToken = obj.RefreshToken,
                AccessToken = obj.AccessToken,
                IsActive = obj.IsActive,
                IsCurrentCompany = obj.IsCurrentConnection,
                UserId = obj.UserId,
                CompanyName = obj.CompanyName,
                CountryVersion = obj.CountryVersion,
                ExpireOn = DateTime.Now.AddMinutes(30),
                RealmId = obj.RealmId
            };
        }
        public QuickBookEntity ToBusinessObject(UserQuickbookConnection obj)
        {
            if (obj == null)
                return null;
            QuickBookEntity quickBookEntity = new QuickBookEntity();
            quickBookEntity.Id = obj.Id;
            quickBookEntity.AccessToken = obj.AccessToken;
            quickBookEntity.RefreshToken = obj.RefreshToken;
            quickBookEntity.RealmId = obj.RealmId;
            quickBookEntity.IsActive = obj.IsActive;
            quickBookEntity.CompanyName = obj.CompanyName;
            quickBookEntity.CountryVersion = obj.CountryVersion;
            quickBookEntity.IsCurrentConnection = obj.IsCurrentCompany;
            return quickBookEntity;
        }
        public async Task<QuickBookEntity> GetQuickBookEntity(int Id)
        {
            var user = await this._applicationDbContext.UserQuickbookConnection.Where(x => x.Id == Id).FirstOrDefaultAsync();
            return ToBusinessObject(user);
        }


        public async Task<QuickBookEntity> CreateQboConnectionAsync(QuickBookEntity quickBookEntity)
        {
            var Connection = ToDataObject(quickBookEntity);
            Connection.CreatedOn = DateTime.Now;
            Connection.IsActive = true;
            _applicationDbContext.UserQuickbookConnection.Add(Connection);
            await _applicationDbContext.SaveChangesAsync();
            return await GetQuickBookEntity(Connection.Id);

        }

        public async Task<QuickBookEntity> GetUserQuickbookConnections(int UserId)
        {
            var userQboConnections = await this._applicationDbContext.UserQuickbookConnection.Where(x => x.UserId == UserId).FirstOrDefaultAsync();

            QuickBookEntity quickBookEntity = ToBusinessObject(userQboConnections);
            return quickBookEntity;
        }

        public async Task<QuickBookEntity> GetUserQuickbookCompany(int UserId)
        {
            var userQboConnections = await this._applicationDbContext.UserQuickbookConnection.FirstOrDefaultAsync(x => x.UserId == UserId && x.IsCurrentCompany == true);
            QuickBookEntity quickBookEntity = ToBusinessObject(userQboConnections);
            return quickBookEntity;
        }

        public async Task<QuickBookEntity> GetUserQuickbookConnectionsById(int Id)
        {
            var userQboConnections = await this._applicationDbContext.UserQuickbookConnection.Where(x => x.Id == Id).FirstOrDefaultAsync();

            QuickBookEntity quickBookEntity = ToBusinessObject(userQboConnections);
            return quickBookEntity;
        }

        public async Task<QuickBookEntity> GetAvaiableQuickbookConnections()
        {
            var userConnectionExists = await this._applicationDbContext.UserQuickbookConnection.FirstOrDefaultAsync();

            QuickBookEntity quickBookEntity = ToBusinessObject(userConnectionExists);
            return quickBookEntity;
        }

        public async Task<QuickBookEntity> UpdateQuickbookConnections(QuickBookEntity quickBookEntity)
        {
            var userDataObject = await this._applicationDbContext.UserQuickbookConnection.FindAsync(quickBookEntity.Id);
            if (userDataObject != null)
            {
                userDataObject.CreatedOn = DateTime.Now;
                userDataObject.ExpireOn = DateTime.Now.AddMinutes(30);
                userDataObject.IsCurrentCompany = quickBookEntity.IsCurrentConnection;
                userDataObject.IsActive = true;
                userDataObject.CompanyName = quickBookEntity.CompanyName;
                userDataObject.RefreshToken= quickBookEntity.RefreshToken;
                userDataObject.AccessToken = quickBookEntity.AccessToken;
                userDataObject.RealmId = quickBookEntity.RealmId;
            }
            await this._applicationDbContext.SaveChangesAsync();

            return quickBookEntity;
        }

        public async Task<QuickBookEntity> ValidateAndCreateQboConnectionAsync(QuickBookEntity quickBookEntity)
        {
            var isConnectionExists = await GetUserQuickbookConnections(quickBookEntity.UserId);

            if (isConnectionExists == null)
            {
                var Connection = ToDataObject(quickBookEntity);
                Connection.CreatedOn = DateTime.Now;
                Connection.IsActive = true;
                Connection.CompanyName = quickBookEntity.CompanyName;
                Connection.IsCurrentCompany = true;
                _applicationDbContext.UserQuickbookConnection.Add(Connection);
                await _applicationDbContext.SaveChangesAsync();
                return await GetQuickBookEntity(Connection.Id);
            }
            else
            {
                quickBookEntity.Id = isConnectionExists.Id;
                quickBookEntity.IsActive = true;
                quickBookEntity.IsCurrentConnection = true;
                var addQuickBookEntity = await UpdateQuickbookConnections(quickBookEntity);
                return addQuickBookEntity;
            }

        }

        public async Task<List<QuickbookImportSummary>> GetAllAsync(int userId, int mappingId)
        {
            return await _applicationDbContext.QuickbookImportSummary.Where(x => x.UserId == userId && x.MappingId == mappingId && x.IsShow).ToListAsync();
        }

        public async Task<IEnumerable<QuickbookImportSummaryResponse>> GetQboImportSummaryRecordsAsync(int userId, int mappingId)
        {
            List<QuickbookImportSummary> summaryList = await GetAllAsync(userId, mappingId);
            foreach (var item in summaryList)
            {
                item.IsShow = false;
            }
            await _applicationDbContext.SaveChangesAsync();
            var returnList = summaryList.Select(x => new QuickbookImportSummaryResponse()
            {
                Error = x.Error,
                Id = x.Id,
                ImportType = x.ImportType,
                IsImported = x.IsImported,
                Status = x.Status,
                TransactionId = x.TransactionId,
                ViewUrl = x.ViewUrl
            });
            return returnList;
        }

        public async Task<QuickbookImportSummaryResponse> GetQboImportSummaryRecordById(int userId, int importSummaryId)
        {
            var importSummary = await _applicationDbContext.QuickbookImportSummary.Include(x => x.Mapping).ThenInclude(x => x.QuickbookTransactionType).Where(x => x.Id == importSummaryId && x.UserId == userId).FirstOrDefaultAsync();
            QuickbookImportSummaryResponse quickbookImportSummaryResponse = new QuickbookImportSummaryResponse();

            quickbookImportSummaryResponse.Error = importSummary.Error;
            quickbookImportSummaryResponse.Id = importSummary.Id;
            quickbookImportSummaryResponse.ImportType = importSummary.ImportType;
            quickbookImportSummaryResponse.IsImported = importSummary.IsImported;
            quickbookImportSummaryResponse.Status = importSummary.Status;
            quickbookImportSummaryResponse.TransactionId = importSummary.TransactionId;
            quickbookImportSummaryResponse.ViewUrl = importSummary.ViewUrl;
            return quickbookImportSummaryResponse;
        }

        public async Task<bool> SaveItemSettings(SaveSettingEntity saveSetting)
        {
            var SaveItem = SaveItemToDataObject(saveSetting);
            var isSettingsExists = await _applicationDbContext.QuickbookAccountSetting.FirstOrDefaultAsync(x => x.UserId == saveSetting.UserId);
            if (isSettingsExists != null)
            {
                isSettingsExists.IncomeAccount = SaveItem.IncomeAccount;
                isSettingsExists.IncomeAccountId = SaveItem.IncomeAccountId;
                isSettingsExists.COGSAccount = SaveItem.COGSAccount;
                isSettingsExists.COGSAccountId = SaveItem.COGSAccountId;
                isSettingsExists.AssetAccount = SaveItem.AssetAccount;
                isSettingsExists.AssetAccountId = SaveItem.AssetAccountId;
                isSettingsExists.TaxCode = SaveItem.TaxCode;
                isSettingsExists.TaxCodeId = SaveItem.TaxCodeId;
                isSettingsExists.Type = SaveItem.Type;
                isSettingsExists.CreateOn = DateTime.Now;
                await _applicationDbContext.SaveChangesAsync();
            }
            else
            {
                await _applicationDbContext.QuickbookAccountSetting.AddAsync(SaveItem);
                await _applicationDbContext.SaveChangesAsync();
            }
            return true;
        }

        public QuickbookAccountSetting SaveItemToDataObject(SaveSettingEntity obj)
        {
            var accountSetting = _mapper.Map<QuickbookAccountSetting>(obj);
            return accountSetting;
        }

        public SaveSettingEntity GetItemToEntityObject(QuickbookAccountSetting obj)
        {
            var accountSetting = _mapper.Map<SaveSettingEntity>(obj);
            return accountSetting;
        }

        public async Task<SaveSettingEntity> GetItemSettings(int userId)
        {
            var AccountSetting = await _applicationDbContext.QuickbookAccountSetting.FirstOrDefaultAsync(x => x.UserId == userId);
            var ItemSettingEntity = GetItemToEntityObject(AccountSetting);
            return ItemSettingEntity;
        }

    }
}
