﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ZedAxisOnline.BusinessObjects.User;
using ZedAxisOnline.Data.Abstraction;
using ZedAxisOnline.Data.EF;
using ZedAxisOnline.Data.EF.Models;

namespace ZedAxisOnline.Data.Repository
{
    public class UserRepository : IUserRepository, IMapper<User, UserEntity>
    {
        private readonly ApplicationDbContext _applicationDbContext;

        public UserRepository(ApplicationDbContext applicationDbContext)
        {
            this._applicationDbContext = applicationDbContext;
        }

        public async Task<UserEntity> CreateUserAsync(UserEntity userToRegister)
        {
            var user = ToDataObject(userToRegister);
            user.CreatedOn = DateTime.Now;
            user.IsActive = true;
            user.ModifiedOn = DateTime.Now;
            user.LastActive = DateTime.Now;
            user.RoleId = 1;
            _applicationDbContext.User.Add(user);
            await _applicationDbContext.SaveChangesAsync();
            return await GetUserAsync(user.Email);
        }

        public async Task<UserEntity> GetUserAsync(string Email)
        {
            var user = await this._applicationDbContext.User.Include(x => x.Role).FirstOrDefaultAsync(x => x.Email.ToLower() == Email.ToLower());

          //  var AddZohoConnections = applicationDbContextSeeder.SeedZohoColumnDetails();

            return ToBusinessObject(user);
        }
        public async Task<UserEntity> GetUserByQboSubAsync(string QboSub)
        {
            var user = await this._applicationDbContext.User.Include(x => x.Role).FirstOrDefaultAsync(x => x.QuickbookSub == QboSub);
            return ToBusinessObject(user);
        }

        public async Task<UserEntity> UpdateUserAsync(UserEntity user, int userId)
        {
            var userDataObject = await this._applicationDbContext.User.FindAsync(userId);
            var role = await this._applicationDbContext.Role.FirstOrDefaultAsync(x => x.Name.ToLower() == user.Role.ToLower());
            if(userDataObject != null)
            {
                userDataObject.FirstName = user.FirstName;
                userDataObject.LastName = user.LastName;
                userDataObject.Photo = user.Photo == null ? userDataObject.Photo : user.Photo;
                userDataObject.RoleId = role != null ? role.Id: 1;
            }
            await this._applicationDbContext.SaveChangesAsync();
            return ToBusinessObject(userDataObject);
        }

        public async Task<UserEntity> GetUserAsync(int userId)
        {
            var user = await this._applicationDbContext.User.Include(x => x.Role).FirstOrDefaultAsync(x => x.Id == userId);
            return ToBusinessObject(user);
        }

        public UserEntity ToBusinessObject(User obj)
        {
            if (obj == null)
                return null;
            UserEntity userEntity = new UserEntity();
            userEntity.Id = obj.Id;
            userEntity.Role = obj.Role.Name;
            userEntity.Email = obj.Email;
            userEntity.FirstName = obj.FirstName;
            userEntity.LastName = obj.LastName;
            userEntity.Photo = obj.Photo;
            userEntity.PasswordHash = obj.PasswordHash;
            userEntity.PasswordSalt = obj.PasswordSalt;
            return userEntity;
        }

        public IList<UserEntity> ToBusinessObjectList(IList<User> list)
        {
            throw new NotImplementedException();
        }

        public User ToDataObject(UserEntity obj)
        {
            return new User()
            {
                Email = obj.Email,
                FirstName = obj.FirstName,
                IsActive = obj.IsActive,
                LastName = obj.LastName,
                PasswordHash = obj.PasswordHash,
                PasswordSalt = obj.PasswordSalt,
                QuickbookSub = obj.QuickbookSub
            };
        }

        public IList<User> ToDataObjectList(IList<UserEntity> list)
        {
            throw new NotImplementedException();
        }

        
    }
}