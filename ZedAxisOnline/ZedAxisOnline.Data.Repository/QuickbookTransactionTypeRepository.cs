﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ZedAxisOnline.BusinessObjects.Connection;
using ZedAxisOnline.BusinessObjects.QuickBook;
using ZedAxisOnline.BusinessObjects.User;
using ZedAxisOnline.Data.Abstraction;
using ZedAxisOnline.Data.EF;
using ZedAxisOnline.Data.EF.Models;

namespace ZedAxisOnline.Data.Repository
{
    public class QuickbookTransactionTypeRepository : IQuickbookTransactionTypeRepository
    {
        private readonly ApplicationDbContext _applicationDbContext;
        private readonly IMapper _mapper;

        public QuickbookTransactionTypeRepository(ApplicationDbContext applicationDbContext, IMapper mapper)
        {
            _applicationDbContext = applicationDbContext;
            _mapper = mapper;
        }

        public async Task<IList<QuickbookTransactionTypeEntity>> GetAllAsync()
        {
            var list = await _applicationDbContext.QuickbookTransactionType.ToListAsync();
            var dataList = _mapper.Map<IList<QuickbookTransactionType>, IList<QuickbookTransactionTypeEntity>>(list);
            return dataList;
        }
    }
}