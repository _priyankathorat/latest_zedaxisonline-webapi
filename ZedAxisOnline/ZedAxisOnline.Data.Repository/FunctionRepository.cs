﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ZedAxisOnline.BusinessObjects.QuickBook;
using ZedAxisOnline.Data.Abstraction;
using ZedAxisOnline.Data.EF;
using ZedAxisOnline.Data.EF.Models;

namespace ZedAxisOnline.Data.Repository
{
    public class FunctionRepository : IFunctionRepository
    {
        private readonly ApplicationDbContext _applicationDbContext;

        public FunctionRepository(ApplicationDbContext applicationDbContext)
        {
            this._applicationDbContext = applicationDbContext;
        }

        public FunctionDetailsEntity ToDataObjectForMappingFunctionDetail(MappingFunctionDetail MappingFunctionDetail)
        {
            if (MappingFunctionDetail == null)
                return null;
            FunctionDetailsEntity FunctionDetailsEntity = new FunctionDetailsEntity();

            FunctionDetailsEntity.Id = MappingFunctionDetail.Id;
            FunctionDetailsEntity.FunctionId = MappingFunctionDetail.MappingFunctionId;
            FunctionDetailsEntity.SearchFor = MappingFunctionDetail.SearchFor;
            FunctionDetailsEntity.ReplaceWith = MappingFunctionDetail.ReplaceWith;
            return FunctionDetailsEntity;
        }

       

        public async Task<List<FunctionDetailsEntity>> GetMappingFunctionData(int MappingFunctionId)
        {

            List<FunctionDetailsEntity> listFunctionDetails = new List<FunctionDetailsEntity>();
            var ConTransCol = await _applicationDbContext.MappingFunctionDetail.Where(x => x.MappingFunctionId == MappingFunctionId).ToListAsync();
            if (ConTransCol != null)
            {
                foreach (var item in ConTransCol)
                {
                    listFunctionDetails.Add(ToDataObjectForMappingFunctionDetail(item));
                }
            }
            return listFunctionDetails;
        }

        public async Task<List<MappingFunctionListEntity>> GetMappingFunctionListForUser(int userId)
        {
            MappingFunctionListEntity FunctionDetails = new MappingFunctionListEntity();

            List<MappingFunctionListEntity> FunctionList = new List<MappingFunctionListEntity>();
            var ConTransCol = await _applicationDbContext.MappingFunction.Where(x => x.UserId == userId).ToListAsync();
            if (ConTransCol != null)
            {
                foreach (var item in ConTransCol)
                {
                    FunctionDetails = new MappingFunctionListEntity();
                    FunctionDetails.Id = item.Id;
                    FunctionDetails.Name = item.Name;
                    FunctionList.Add(FunctionDetails);
                }
            }
            return FunctionList;
        }
        public async Task<MappingFunctionDetailsEntity> AddFunctionDetails(MappingFunctionDetailsEntity MappingFunction)
        {
            if(MappingFunction.FunctionId == 0)
            {
                MappingFunction obj = new MappingFunction()
                {
                    CreatedOn = DateTime.Now,
                    ModifiedOn = DateTime.Now,
                    Name = MappingFunction.Name,
                    UserId = MappingFunction.UserId
                };
                _applicationDbContext.MappingFunction.Add(obj);
                await _applicationDbContext.SaveChangesAsync();
                MappingFunction.FunctionId = obj.Id;
            }
            MappingFunctionDetail Functiondetails = new MappingFunctionDetail();
            Functiondetails.MappingFunctionId = MappingFunction.FunctionId;
            Functiondetails.SearchFor = MappingFunction.SearchFor;
            Functiondetails.ReplaceWith = MappingFunction.ReplaceWith;
            await _applicationDbContext.MappingFunctionDetail.AddAsync(Functiondetails);
            await _applicationDbContext.SaveChangesAsync();
            if (MappingFunction.Name != null)
            {
                var Function = await _applicationDbContext.MappingFunction.Where(x => x.Id == MappingFunction.FunctionId).FirstOrDefaultAsync();
                if (Function != null)
                {
                    Function.Name = MappingFunction.Name;
                    await _applicationDbContext.SaveChangesAsync();
                }
            }
            return MappingFunction;
        }
        public async Task<int> UpdateFunctionDetails(MappingFunctionDetailsEntity MappingFunction)
        {
            var MappingFunctionDetails = await _applicationDbContext.MappingFunctionDetail.Where(x => x.Id == MappingFunction.Id).FirstOrDefaultAsync();
                MappingFunctionDetails.SearchFor = MappingFunction.SearchFor;
                MappingFunctionDetails.ReplaceWith = MappingFunction.ReplaceWith;
                await _applicationDbContext.SaveChangesAsync();
            if (MappingFunction.Name != null)
            {
                var Function = await _applicationDbContext.MappingFunction.Where(x => x.Id == MappingFunction.FunctionId).FirstOrDefaultAsync();
                if (Function != null)
                {
                    Function.Name = MappingFunction.Name;
                    await _applicationDbContext.SaveChangesAsync();
                }
            }
            return MappingFunctionDetails.Id;
        }

        public async Task<int> AddNewFunctionDetailsToFunction(MappingFunctionDetailsEntity MappingFunction)
        {
            MappingFunctionDetail newDataObject = new MappingFunctionDetail();
            newDataObject.MappingFunctionId = MappingFunction.FunctionId;
            newDataObject.ReplaceWith = MappingFunction.ReplaceWith;
            newDataObject.SearchFor = MappingFunction.SearchFor;
            _applicationDbContext.MappingFunctionDetail.Add(newDataObject);
            await _applicationDbContext.SaveChangesAsync();
            return newDataObject.Id;
        }

        public async Task<bool> DeleteFunctionDetails(int id)
        {
            var MappingFunctionDetails = await _applicationDbContext.MappingFunctionDetail.Where(x => x.Id == id).FirstOrDefaultAsync();
            _applicationDbContext.Remove(MappingFunctionDetails);
            await _applicationDbContext.SaveChangesAsync();
            return true;
        }


    }
}
