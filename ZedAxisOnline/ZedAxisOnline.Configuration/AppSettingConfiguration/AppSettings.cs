﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ZedAxisOnline.Configuration.AppSettingConfiguration
{
    public class AppSettings
    {
        public string Token { get; set; }
        public string BaseUrl { get; set; }
    }
}
