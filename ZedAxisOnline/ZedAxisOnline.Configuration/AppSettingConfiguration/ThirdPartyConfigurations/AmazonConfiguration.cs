﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ZedAxisOnline.Configuration.AppSettingConfiguration
{
    public class AmazonConfiguration
    {
        public string ClientId { get; set; }
        public string ClientSecret { get; set; }
        public string AllowedOrigins { get; set; }
        public string RedirectUrl { get; set; }
        public string Environment { get; set; }
        public string State { get; set; }
        public string TokenInfoUrl { get; set; }


        public string GetAmazonSignInUrl()
        {
            return string.Format("https://www.amazon.com/ap/oa?client_id={0}&scope=profile&response_type=token&state={1}&redirect_uri={2}", ClientId, State, RedirectUrl);
        }
    }
}
