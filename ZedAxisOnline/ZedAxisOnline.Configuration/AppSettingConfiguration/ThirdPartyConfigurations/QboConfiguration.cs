﻿namespace ZedAxisOnline.Configuration.AppSettingConfiguration
{
    public class QboConfiguration
    {
        public string ClientId { get; set; }
        public string ClientSecret { get; set; }
        public string RedirectUrlIntuitSignIn { get; set; }
        public string RedirectUrl { get; set; }
        public string Environment { get; set; }
        public string LogPath { get; set; }
        public string ServiceContextBaseUrl { get; set; }
        public string MinorVersion { get; set; }
    }
}
