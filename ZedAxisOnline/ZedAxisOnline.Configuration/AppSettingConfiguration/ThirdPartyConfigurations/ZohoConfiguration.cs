﻿namespace ZedAxisOnline.Configuration.AppSettingConfiguration
{
    public class ZohoConfiguration
    {
        public string GetInvoiceUrl { get; set; }
        public string GetAllInvoicesUrl { get; set; }
        public string subscriptionsHeader { get; set; }
        public string SubscriptionOrganizationUrl { get; set; }
        public string InvoiceListUrl { get; set; }
        public string InvoiceDetailsUrl { get; set; }
    }
}
