﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ZedAxisOnline.Configuration.AppSettingConfiguration
{
    public class ThirdPartyConfigurations
    {
        public AmazonConfiguration Amazon { get; set; }
        public QboConfiguration QBO { get; set; }
        public ZohoConfiguration Zoho { get; set; }
    }
}
