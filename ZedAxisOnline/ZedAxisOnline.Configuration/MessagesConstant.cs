﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ZedAxisOnline.Configuration
{
    public static class MessagesConstant
    {
        public const string UserExist = "User Exist";
        public const string UserAlreadyExistMsg = "Email already registered, please login to access your account";
        public const string SomethingWentWrongMsg = "something went wrong. please try again";
        public const string CreationFailedMsg = "Unable to create.";
        public const string InvalidCredentialMsg = "Invalid credentials";

        public const string AmazonAccountCreatedMessage = "Amazon connection Created/Updated succesfully.";
        public const string SelectFieldToMap = "Select field to map";
        public const string SelectFunction = "Select function";

        public const string InvalidUserTokenMessage = "Invalid AuthToken.";
        public const string NoOrganizationAvailableMessage = "Organization is not available, Please add organization and try again.";

        public const string ZohoConnectionAlreadyExistMessage = "Connection is already exist";

        // Item types
        public const string ItemService = "Service";
        public const string ItemInventory = "Inventory";
        public const string ItemNonInventory = "NonInventory";
    }

    public static class QuickbookItemTypeListConstant
    {
        public static readonly List<string> ItemTypeList =  new List<string>() { MessagesConstant.ItemService, MessagesConstant.ItemInventory, MessagesConstant.ItemNonInventory };

    }
}
