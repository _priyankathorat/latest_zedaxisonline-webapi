﻿using System;
using System.Collections.Generic;

namespace ZedAxisOnline.BusinessObjects.Connection
{
  public  class ZohoInvoiceListResponse
    {
        public string code { get; set; }

        public string message { get; set; }

        public List<invoices> invoices { get; set; }

    }

    public class invoices
    {
        public string invoice_id { get; set; }



    }

    public class invoice
    {
        public string invoice_id { get; set; }

        public string customer_name { get; set; }

        public string email { get; set; }
        public DateTime invoice_date { get; set; }
        public string currency_code { get; set; }
        public double total { get; set; }
        public string status { get; set; }
        public invoice_item[] invoice_items { get; set; }
        public billing_address billing_address { get; set; }
        public shipping_address shipping_address { get; set; }


    }

    public class ZohoInvoiceResponse
    {
        public string code { get; set; }

        public string message { get; set; }

        public invoice invoice { get; set; }
    }
    public class invoice_item
    {
        public string item_id { get; set; }

        public string name { get; set; }

        public string description { get; set; }
        public string price { get; set; }
        public string discount_amount { get; set; }
        
        public string quantity { get; set; }
        public string item_total { get; set; }

    }
    public class billing_address
    {


        public string city { get; set; }
        public string state { get; set; }
        public string zip { get; set; }
        public string country { get; set; }


    }

    public class shipping_address
    {
        public string street { get; set; }


        public string city { get; set; }
        public string state { get; set; }
        public string zip { get; set; }
        public string country { get; set; }


    }
}
