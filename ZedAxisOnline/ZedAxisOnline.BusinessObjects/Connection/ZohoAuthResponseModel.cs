﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ZedAxisOnline.BusinessObjects.Connection
{
    public class ZohoAuthResponseModel
    {
        public string code { get; set; }
        public string message { get; set; }
        public Organization[] organizations { get; set; }
    }

    public class Organization
    {
        public string organization_id { get; set; }
        public string name { get; set; }
    }
}
