﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ZedAxisOnline.BusinessObjects.Connection
{
    public class AmazonAuthResponseModel
    {
        public string aud { get; set; }
        public string user_id { get; set; }
        public string iss { get; set; }
        public string exp { get; set; }
        public string app_id { get; set; }
        public string iat { get; set; }
        public string error { get; set; }
    }
}
