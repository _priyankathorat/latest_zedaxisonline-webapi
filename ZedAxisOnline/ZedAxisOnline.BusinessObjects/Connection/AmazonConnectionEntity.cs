﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ZedAxisOnline.BusinessObjects.Connection
{
    public class AmazonConnectionEntity
    {
        public int Id { get; set; }
        public string AccessToken { get; set; }
        public string AmazonUserId { get; set; }
        public DateTime ExpireOn { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime ModifiedOn { get; set; }
        public int UserId { get; set; }
    }
}
