﻿using System;
using System.Collections.Generic;
using System.Text;
using ZedAxisOnline.BusinessObjects.User;

namespace ZedAxisOnline.BusinessObjects.Connection
{
    public class ConnectionTransactionColumnEntity
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
