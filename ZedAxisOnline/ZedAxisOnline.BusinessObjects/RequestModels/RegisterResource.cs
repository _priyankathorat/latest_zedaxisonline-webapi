﻿using System.ComponentModel.DataAnnotations;

namespace ZedAxisOnline.BusinessObjects.RequestModels
{
    public class RegisterResource
    {
        [Required]
        public string FirstName { get; set; }
        [Required]
        public string LastName { get; set; }
        [Required]
        public string Email { get; set; }
        [Required]
        public string Password { get; set; }
    }
}
