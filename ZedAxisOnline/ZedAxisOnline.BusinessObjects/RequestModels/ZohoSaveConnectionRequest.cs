﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ZedAxisOnline.BusinessObjects.RequestModels
{
    public class ZohoSaveConnectionRequest
    {
        public string AuthToken { get; set; }
        public string ConnectionName { get; set; }
        public string WebhookUrl { get; set; }
        public string OrganizationId { get; set; }
        public string OrganizationName { get; set; }
    }
}
