﻿using Microsoft.AspNetCore.Http;
using System.ComponentModel.DataAnnotations;

namespace ZedAxisOnline.BusinessObjects.RequestModels
{
    public class ProfileUpdateRequest
    {
        [Required]
        public string FirstName { get; set; }
        [Required]
        public string LastName { get; set; }
        public string Photo { get; set; }
        public IFormFile PhotoUpload { get; set; }
        public string Role { get; set; }
    }
}
