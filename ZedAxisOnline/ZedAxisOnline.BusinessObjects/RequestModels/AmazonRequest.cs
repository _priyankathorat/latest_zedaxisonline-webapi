﻿using System.ComponentModel.DataAnnotations;

namespace ZedAxisOnline.BusinessObjects.RequestModels
{
    public class AmazonRequest
    {
        [Required]
        public string Code { get; set; }
    }
}
