﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ZedAxisOnline.BusinessObjects.RequestModels
{
    public class ZohoSaveWebhookRequest
    {
        public int ConnectionTypeId { get; set; }
        public int Id { get; set; }
        public string WebhookUrl { get; set; }
    }
}
