﻿namespace ZedAxisOnline.BusinessObjects.QuickBook
{
    public class QuickbookImportSummaryEntity
    {
        public int MappingId { get; set; }
        public int UserId { get; set; }
        public bool IsImported { get; set; }
        public string TransactionId { get; set; }
        public string ImportType { get; set; }
        public bool IsShow { get; set; }
        public string Status { get; set; }
        public string Error { get; set; }
        public string Message { get; set; }
        public string ViewUrl { get; set; }
    }

    public class QuickbookImportSummaryResponse
    {
        public int Id { get; set; }
        public string ImportType { get; set; }
        public bool IsImported { get; set; }
        public string TransactionId { get; set; }
        public string QuickbookTransactionType { get; set; }
        public string ViewUrl { get; set; }
        public string Status { get; set; }
        public string Error { get; set; }
    }


}
