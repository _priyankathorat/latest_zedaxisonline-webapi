﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ZedAxisOnline.BusinessObjects.QuickBook
{
   
    public class IdNameEntity
    {
        public string Id { get; set; }
        public string Name { get; set; }
    }


    public class AccountSettingEntity
    {
        public List<string> ItemType { get; set; }

        public List<IdNameEntity> TaxEntities { get; set; }
        public List<IdNameEntity> COGSAccountEntities { get; set; }
        public List<IdNameEntity> AssetAccountEntities { get; set; }
        public List<IdNameEntity> IncomeAccountEntities { get; set; }

        public SaveSettingEntity ExistingSettings { get; set; }
    }

    public class SaveSettingEntity
    {
        public int UserId { get; set; }

        public string Type { get; set; }

        public string TaxCode { get; set; }
        public string TaxCodeId { get; set; }

        public string IncomeAccount { get; set; }
        public string IncomeAccountId { get; set; }

        public string COGSAccount { get; set; }
        public string COGSAccountId { get; set; }

        public string AssetAccount { get; set; }
        public string AssetAccountId { get; set; }
    }







}
