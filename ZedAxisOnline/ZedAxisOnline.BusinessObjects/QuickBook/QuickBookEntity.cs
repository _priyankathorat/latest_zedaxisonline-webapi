﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ZedAxisOnline.BusinessObjects.QuickBook
{
    public class QuickBookEntity
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public string AccessToken { get; set; }
        public string RefreshToken { get; set; }
        public string CountryVersion { get; set; }
        public string CompanyName { get; set; }
        public string RealmId { get; set; }
        
        public bool IsActive { get; set; }
        public bool IsCurrentConnection { get; set; }
    }

    public class QuickbookCodeEntity
    {
        public int userId { get; set; }
        public string code { get; set; }
        public string state { get; set; }
        public string realmId { get; set; }
    }
    public class QuickbookConnectionDetails
    {
        public int Id { get; set; }
        public string CompanyName { get; set; }
        public bool IsCurrentConnection { get; set; }
    }

    public class QboLoginDetailsEntity
    {
        public int Id { get; set; }
        public string RealmId { get; set; }
        public string AccessToken { get; set; }
        public string CountryVersion { get; set; }
        public string RefreshToken { get; set; }
    }

    public class QuickbooksCompanyDetailsEntity
    {
        public string Country { get; set; }

        public string Name { get; set; }

    }

    


}
