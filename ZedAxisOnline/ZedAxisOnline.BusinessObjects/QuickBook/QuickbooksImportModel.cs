using System;

namespace ZedAxisOnline.BusinessObjects.QuickBook
{
    public class QuickbooksImportModel
    {
        public int MappingId { get; set; }

        public int UserId { get; set; }

    }

    public class QuickbooksCompanyDetailsDto
    {
        public string Country { get; set; }

        public string Name { get; set; }

    }
}