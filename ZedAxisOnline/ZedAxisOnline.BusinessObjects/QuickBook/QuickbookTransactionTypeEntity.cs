﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ZedAxisOnline.BusinessObjects.QuickBook
{
    public class QuickbookTransactionTypeEntity
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
