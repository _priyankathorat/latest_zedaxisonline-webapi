﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ZedAxisOnline.BusinessObjects.QuickBook
{
    public class DataForMapppingEntity
    {
        public int Id { get; set; }
        public string QBOTransactiontype { get; set; }
        public string QBOTransColumnName { get; set; }
        public int ConnectionTransColumnId { get; set; }
        public int IsMapped { get; set; }
        public string ConstantValue { get; set; }
        public string SelectedItem { get; set; }
        public string SelectedFunction { get; set; }
        public int MappingFunctionId { get; set; }
        public string SampleData1 { get; set; }
        public string SampleData2 { get; set; }
    }

    public class CheckMappingNameEntity
    {
        public int userId { get; set; }
        public string MappingName { get; set; }
    }

    public class SaveMappingEntity
    {
        public string Name { get; set; }
        public int UserId { get; set; }
        public int ConnectionTransactionTypeId { get; set; }
        public int QuickbookTransactionTypeId { get; set; }
       // public int ConnectionTypeId { get; set; }
        public List<MappedFieldEntity> MappedField { get; set; }
    }

    public class MappedFieldEntity
    {
        public int QBOTransColumnId { get; set; }
        public int? ConnectionTransColumnId { get; set; }
        public string ConstantValue { get; set; }
        public int? SelectedFunctionId { get; set; }
    }



    }
