﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ZedAxisOnline.BusinessObjects.QuickBook
{

    public class FunctionDetailsEntity
    {
        public int Id { get; set; }
        public int FunctionId { get; set; }
        public string Name { get; set; }
        public string SearchFor { get; set; }
        public string ReplaceWith { get; set; }
    }

    public class MappingFunctionListEntity
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }

    public class MappingFunctionDetailsEntity
    {
        public int Id { get; set; }
        public int FunctionId { get; set; }
        public string Name { get; set; }
        public string SearchFor { get; set; }
        public string ReplaceWith { get; set; }
        public int UserId { get; set; }
    }
}
