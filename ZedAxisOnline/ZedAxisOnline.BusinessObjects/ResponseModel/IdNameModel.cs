﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ZedAxisOnline.BusinessObjects.ResponseModel
{
    public class IdNameModel
    {
        public string Id { get; set; }
        public string Name { get; set; }
    }
}
