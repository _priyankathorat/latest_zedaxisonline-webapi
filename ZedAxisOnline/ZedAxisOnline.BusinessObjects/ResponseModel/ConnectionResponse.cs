﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ZedAxisOnline.BusinessObjects.ResponseModel
{
    public class ConnectionResponse
    {
        public int ConnectionTypeId { get; set; }
        public string ConnectionName { get; set; }
        public string ConnectionType { get; set; }
        public int Id { get; set; }
    }
}
