﻿namespace ZedAxisOnline.BusinessObjects.ResponseModel
{
    public class UserResponse
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string PhotoUrl { get; set; }
        public string Role { get; set; }
        
    }
}
