﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ZedAxisOnline.BusinessObjects.ResponseModel
{
    public class ZohoOrganizationResponse
    {

        public ZohoOrganizationResponse()
        {
            this.Organizations = new List<IdNameModel>();
        }

        public bool IsValid { get; set; }
        public string Message { get; set; }
        public List<IdNameModel> Organizations { get; set; }
    }
}
