﻿using System;
using System.Collections.Generic;
using System.Text;
using ZedAxisOnline.BusinessObjects.ResponseModel;
using ZedAxisOnline.BusinessObjects.User;

namespace ZedAxisOnline.BusinessObjects.SignIn
{
    public class SignInResponse
    {
        public string Token { get; set; }
        public UserResponse User { get; set; }
    }
}
