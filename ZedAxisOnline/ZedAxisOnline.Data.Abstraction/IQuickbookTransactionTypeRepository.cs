﻿using System.Collections.Generic;
using System.Threading.Tasks;
using ZedAxisOnline.BusinessObjects.QuickBook;

namespace ZedAxisOnline.Data.Abstraction
{
    public interface IQuickbookTransactionTypeRepository
    {
        Task<IList<QuickbookTransactionTypeEntity>> GetAllAsync();
    }
}
