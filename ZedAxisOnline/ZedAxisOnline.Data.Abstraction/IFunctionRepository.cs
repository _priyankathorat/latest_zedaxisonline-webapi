﻿using System.Collections.Generic;
using System.Threading.Tasks;
using ZedAxisOnline.BusinessObjects.QuickBook;

namespace ZedAxisOnline.Data.Abstraction
{
    public interface IFunctionRepository
    {

        Task<List<FunctionDetailsEntity>> GetMappingFunctionData(int MappingFunctionId);
        Task<List<MappingFunctionListEntity>> GetMappingFunctionListForUser(int userId);
        Task<MappingFunctionDetailsEntity> AddFunctionDetails(MappingFunctionDetailsEntity MappingFunction);
        Task<int> UpdateFunctionDetails(MappingFunctionDetailsEntity MappingFunction);
        Task<int> AddNewFunctionDetailsToFunction(MappingFunctionDetailsEntity MappingFunction);
        Task<bool> DeleteFunctionDetails(int id);
    }
}