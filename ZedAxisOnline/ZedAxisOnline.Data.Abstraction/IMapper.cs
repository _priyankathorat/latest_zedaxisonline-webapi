﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ZedAxisOnline.Data.Abstraction
{
    public interface IMapper<T1, T2>
    {
        IList<T2> ToBusinessObjectList(IList<T1> list);
        T2 ToBusinessObject(T1 obj);
        IList<T1> ToDataObjectList(IList<T2> list);
        T1 ToDataObject(T2 obj);
    }
}
