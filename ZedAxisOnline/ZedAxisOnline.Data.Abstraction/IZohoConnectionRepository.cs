﻿using System.Collections.Generic;
using System.Threading.Tasks;
using ZedAxisOnline.BusinessObjects.Connection;

namespace ZedAxisOnline.Data.Abstraction
{
    public interface IZohoConnectionRepository
    {
        Task<IList<ZohoConnectionEntity>> GetZohoConnectionsAsync(int userId);
        Task<bool> IsZohoConnectionExists(string authToken, int userId);
        Task AddZohoConnectionAsync(ZohoConnectionEntity objectToSave);
        Task UpdateWebhookUrlAsync(int id, string webhookUrl, int userId);
        Task<ZohoConnectionDetailsEntity> FindById(int id);
        Task<int> AddZohoInvoiceAsync(ZohoInvoiceEntity objectToSave);
        Task<int> AddZohoInvoiceItemAsync(ZohoInvoiceItemEntity objectToSave);

        Task<IList<ZohoInvoiceEntity>> FindInvoiceByMappingId(int mappingId);

    }
}
