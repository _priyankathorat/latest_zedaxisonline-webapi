﻿using System.Threading.Tasks;
using ZedAxisOnline.BusinessObjects.QuickBook;

namespace ZedAxisOnline.Data.Abstraction
{
    public interface IQuickbookImportSummaryRepository
    {
        Task<QuickbookImportSummaryEntity> CreateQuickBookImportSummaryAsync(QuickbookImportSummaryEntity quickBookEntity);
        Task<QuickbookImportSummaryEntity> GetQuickbookImportSummaryEntity(int Id);
    }
}
