﻿using System.Collections.Generic;
using System.Threading.Tasks;
using ZedAxisOnline.BusinessObjects.QuickBook;

namespace ZedAxisOnline.Data.Abstraction
{
    public interface IQuickbookRepository
    {
        Task<QuickBookEntity> GetUserQuickbookConnections(int UserId);
        Task<QuickBookEntity> GetUserQuickbookCompany(int UserId);
        Task<QuickBookEntity> GetUserQuickbookConnectionsById(int Id);
        Task<QuickBookEntity> GetQuickBookEntity(int Id);
        Task<QuickBookEntity> CreateQboConnectionAsync(QuickBookEntity quickBookEntity);
        Task<QuickBookEntity> ValidateAndCreateQboConnectionAsync(QuickBookEntity quickBookEntity);
        Task<IEnumerable<QuickbookImportSummaryResponse>> GetQboImportSummaryRecordsAsync(int userId, int mappingId);
        Task<QuickBookEntity> UpdateQuickbookConnections(QuickBookEntity quickBookEntity);
        Task<QuickbookImportSummaryResponse> GetQboImportSummaryRecordById(int userId, int importSummaryId);

        Task<bool> SaveItemSettings(SaveSettingEntity saveSetting);
        Task<SaveSettingEntity> GetItemSettings(int userId);
        Task<QuickBookEntity> GetAvaiableQuickbookConnections();
    }
}
