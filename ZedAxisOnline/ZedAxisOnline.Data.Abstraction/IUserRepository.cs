﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using ZedAxisOnline.BusinessObjects.User;

namespace ZedAxisOnline.Data.Abstraction
{
    public interface IUserRepository
    {
        Task<UserEntity> GetUserAsync(string Email);
        Task<UserEntity> GetUserByQboSubAsync(string QboSub);
        Task<UserEntity> CreateUserAsync(UserEntity userToRegister);
        Task<UserEntity> GetUserAsync(int userId);
        Task<UserEntity> UpdateUserAsync(UserEntity user, int userId);
    }
}
