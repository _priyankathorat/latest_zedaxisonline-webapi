﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using ZedAxisOnline.BusinessObjects.Connection;
using ZedAxisOnline.BusinessObjects.User;

namespace ZedAxisOnline.Data.Abstraction
{
    public interface IAmazonConnectionRepository
    {
        Task SaveOrUpdateAmazonConnection(AmazonConnectionEntity entity);
        Task<IList<AmazonConnectionEntity>> GetAmazonConnections(int userId);
    }
}
