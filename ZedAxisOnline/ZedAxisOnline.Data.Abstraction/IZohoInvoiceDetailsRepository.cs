﻿using System.Collections.Generic;
using System.Threading.Tasks;
using ZedAxisOnline.BusinessObjects.Connection;
using ZedAxisOnline.Data.EF.Models;

namespace ZedAxisOnline.Data.Abstraction
{
    public interface IZohoInvoiceDetailsRepository
    {
        Task<List<ZohoInvoice>> GetZohoInvoiceDetails(int MappingId, bool isImported = false);
        // Task GetZohoInvoiceDetails(ZohoConnectionEntity objectToSave);
        Task<List<ZohoInvoiceEntity>> GetZohoInoviceListAsync(int mappingId);

       
        // Task<List<ZohoInvoice>> FindByMappingId(int mappingId);
        //Task<ZohoInvoice> FindByInvoiceIdAsync(string invoice_id);
    }
}
