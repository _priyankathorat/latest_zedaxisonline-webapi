﻿using System.Collections.Generic;
using System.Threading.Tasks;
using ZedAxisOnline.BusinessObjects.Connection;
using ZedAxisOnline.BusinessObjects.QuickBook;
using ZedAxisOnline.Data.EF.Models;

namespace ZedAxisOnline.Data.Abstraction
{
    public interface IMappingRepository
    {
        Task<List<DataForMapppingEntity>> GetDataForMappping(int qboTransactionTypeId);
        Task<bool> IsMappingNameValid(string MappingName, int userId);
        Task<int> SaveMapping(SaveMappingEntity saveMappingEntity);
        Task<List<ConnectionTransactionType>> GetConnectionTransactionType(int conTypeId);
        Task<List<QuickbookTransactionColumn>> GetQuickbookTransactionColumn(int qboTransTypeId);

        Task<Mapping> FindMappingByIdAsync(int  mappingId);
        Task<IList<ConnectionTransactionColumnEntity>> GetConnectionTransactionColumnsAsync(int connectionTypeId);
    }
}
