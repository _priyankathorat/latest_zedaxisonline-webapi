namespace ZedAxisOnline.Data.EF.Models
{
    public class ZohoInvoiceItem
    {
        public int Id { get; set; }
        public string item_id { get; set; }
        public string name { get; set; }
        public string description { get; set; }
        public string price { get; set; }
        public string discount_amount { get; set; }
        public string quantity { get; set; }
        public string item_total { get; set; }

        public int ZohoInvoiceId { get; set; }
        public ZohoInvoice ZohoInvoice { get; set; }
    }
}