﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ZedAxisOnline.Data.EF.Models
{
    public class AmazonConnection
    {
        public int Id { get; set; }
        public string AccessToken { get; set; }
        public string AmazonUserId { get; set; }
        public DateTime ExpireOn { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime ModifiedOn { get; set; }

        public int UserId { get; set; }
        public User User { get; set; }
    }
}
