using System.Collections.Generic;

namespace ZedAxisOnline.Data.EF.Models
{
    public class ConnectionTransactionColumn
    {
        public int Id { get; set; }
        public int SequenceNumber { get; set; }

        public string Name { get; set; }
        public int ConnectionTransactionTypeId { get; set; }
        public ConnectionTransactionType ConnectionTransactionType { get; set; }
        public ICollection<QuickbookSampleData> QuickBookSampleData { get; set; }
        public ICollection<MappingDetail> MappingDetails { get; set; }

    }
}