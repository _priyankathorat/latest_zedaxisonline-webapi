using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ZedAxisOnline.Data.EF.Models
{
    public class UserQuickbookConnection : BaseOAuthEntity
    {
        public override int Id { get; set; }
        public override string AccessToken { get; set; }
        public override string RefreshToken { get; set; }
        public bool IsActive { get; set; }
        public string CompanyName { get; set; }
        public bool IsCurrentCompany { get; set; }
        public string RealmId { get; set; }
        public DateTime ExpireOn { get; set; }
        public DateTime CreatedOn { get; set; }

        public int UserId { get; set; }
        public User User { get; set; }
        public string CountryVersion { get; set; }
    }
}