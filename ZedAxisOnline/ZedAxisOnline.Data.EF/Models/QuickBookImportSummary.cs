﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ZedAxisOnline.Data.EF.Models
{
  public  class QuickbookImportSummary
    {
        public int Id { get; set; }
        public string ImportType { get; set; }
        public bool IsImported { get; set; }
        public bool IsShow { get; set; }
        public string TransactionId { get; set; }
        public string ViewUrl { get; set; }
        public string Status { get; set; }
        public string Error { get; set; }
        public string Message { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime UpdatedDate { get; set; }

        public int UserId { get; set; }
        public User User { get; set; }


        public int MappingId { get; set; }
        public Mapping Mapping { get; set; }

    }
}
