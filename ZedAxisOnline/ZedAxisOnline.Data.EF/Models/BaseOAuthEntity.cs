﻿namespace ZedAxisOnline.Data.EF.Models
{
    public class BaseOAuthEntity
    {
        public virtual string RefreshToken { get; set; }
        public virtual string AccessToken { get; set; }
        public virtual int Id { get; set; }
    }
}