using System.Collections.Generic;

namespace ZedAxisOnline.Data.EF.Models
{
    public class ConnectionTransactionType
    {
        public int Id { get; set; }
        public string TransType { get; set; }
        public string EventName { get; set; }

        public int ConnectionTypeId { get; set; }
        public ConnectionType ConnectionType { get; set; }

        public ICollection<ConnectionTransactionColumn> ConnectionTransactionColumns { get; set; }
        public ICollection<Mapping> Mappings { get; set; }

    }
}