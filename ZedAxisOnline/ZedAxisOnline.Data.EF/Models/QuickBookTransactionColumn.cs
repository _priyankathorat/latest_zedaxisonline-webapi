using System.Collections.Generic;

namespace ZedAxisOnline.Data.EF.Models
{
    public class QuickbookTransactionColumn
    {
        public int Id { get; set; }
        public int SequenceNumber { get; set; }
        public string Name { get; set; }
        public int QuickbookTransactionTypeId { get; set; }
        public QuickbookTransactionType QuickbookTransactionType { get; set; }

        public ICollection<MappingDetail> MappingDetails { get; set; }



    }
}