using System.Collections.Generic;

namespace ZedAxisOnline.Data.EF.Models
{
    public class ConnectionType
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public  ICollection<ConnectionTransactionType> ConnectionTransactionTypes { get; set; }   

    }
}