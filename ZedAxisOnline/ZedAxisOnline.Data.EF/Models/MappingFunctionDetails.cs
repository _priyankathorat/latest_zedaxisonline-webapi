namespace ZedAxisOnline.Data.EF.Models
{
    public class MappingFunctionDetail
    {

        public int Id { get; set; }
        public string SearchFor { get; set; }
        public string ReplaceWith { get; set; }
        public string Expression { get; set; }

        public int MappingFunctionId { get; set; }
        public MappingFunction MappingFunction { get; set; }
        

    }
}