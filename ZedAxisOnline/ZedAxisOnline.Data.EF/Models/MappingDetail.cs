using System.Collections.Generic;

namespace ZedAxisOnline.Data.EF.Models
{
    public class MappingDetail
    {
        public int Id { get; set; }
        public string ConstantValue { get; set; }
        public int MappingId { get; set; }
        public Mapping Mapping { get; set; }
        public int QuickbookTransactionColumnId { get; set; }
        public QuickbookTransactionColumn QuickbookTransactionColumn { get; set; }
        public int? ConnectionTransactionColumnId { get; set; }
        public ConnectionTransactionColumn ConnectionTransactionColumn { get; set; }
        public int? MappingFunctionId { get; set; }
        public MappingFunction MappingFunction { get; set; }
    }
}