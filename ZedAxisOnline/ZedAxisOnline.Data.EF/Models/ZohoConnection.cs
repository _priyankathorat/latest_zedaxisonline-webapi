﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ZedAxisOnline.Data.EF.Models
{
    public class ZohoConnection
    {
        public int Id { get; set; }
        public string AuthToken { get; set; }
        public string OrganizationId { get; set; }
        public string OrganizationName { get; set; }
        public string ConnectionName { get; set; }
        public string WebhookUrl { get; set; }
        public DateTime ModifiedOn { get; set; }
        public DateTime CreatedOn { get; set; }

        public int UserId { get; set; }
        public User User { get; set; }
    }
}
