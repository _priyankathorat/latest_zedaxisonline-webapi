using System;
using System.Collections.Generic;

namespace ZedAxisOnline.Data.EF.Models
{
    public class QuickbookAccountSetting
    {
        public int Id { get; set; }
        public string Type { get; set; }

        public string TaxCode { get; set; }
        public string TaxCodeId { get; set; }

        public string IncomeAccount { get; set; }
        public string IncomeAccountId { get; set; }

        public string COGSAccount { get; set; }
        public string COGSAccountId { get; set; }

        public string AssetAccount { get; set; }
        public string AssetAccountId { get; set; }

        public DateTime CreateOn { get; set; }

        public int UserId { get; set; }
        public User User { get; set; }

    }
}