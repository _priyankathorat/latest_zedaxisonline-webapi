using System;
using System.Collections.Generic;

namespace ZedAxisOnline.Data.EF.Models
{
    public class ZohoInvoice
    {
        public int Id { get; set; }
        public bool IsImported { get; set; }
        public string invoice_id { get; set; }
        public string customer_name { get; set; }
        public string email { get; set; }
        public DateTime invoice_date { get; set; }
        public string currency_code { get; set; }
        public double total { get; set; }
        public string status { get; set; }
        public string billing_address_city { get; set; }
        public string billing_address_state { get; set; }
        public string billing_address_zip { get; set; }
        public string billing_address_country { get; set; }
        public string shipping_address_street { get; set; }
        public string shipping_address_city { get; set; }
        public string shipping_address_state { get; set; }
        public string shipping_address_zip { get; set; }
        public string shipping_address_country { get; set; }

        public int MappingId { get; set; }
        public Mapping Mapping { get; set; }

        public ICollection<ZohoInvoiceItem> ZohoInvoiceItems { get; set; }

       
    }
}