using System.Collections.Generic;

namespace ZedAxisOnline.Data.EF.Models
{
    public class QuickbookTransactionType
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public ICollection<QuickbookTransactionColumn> QuickbookTransactionColumns { get; set; }
        public ICollection<Mapping> Mappings { get; set; }

    }
}