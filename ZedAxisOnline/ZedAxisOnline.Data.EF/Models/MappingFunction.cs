using System;
using System.Collections.Generic;

namespace ZedAxisOnline.Data.EF.Models
{
    public class MappingFunction
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime ModifiedOn { get; set; }

        public int UserId { get; set; }
        public User User { get; set; }

        public ICollection<MappingFunctionDetail> MappingFunctionDetails { get; set; }
        public ICollection<MappingDetail> MappingDetails { get; set; }

    }
}