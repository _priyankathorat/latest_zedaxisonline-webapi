using System.Collections.Generic;

namespace ZedAxisOnline.Data.EF.Models
{
    public class Role
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public ICollection<User> Users { get; set; }
    }
}