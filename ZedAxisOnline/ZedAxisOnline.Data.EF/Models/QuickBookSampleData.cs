using System.Collections.Generic;

namespace ZedAxisOnline.Data.EF.Models
{
    public class QuickbookSampleData
    { 
        public int Id { get; set; }
        public string SampleData1 { get; set; }
        public string SampleData2 { get; set; }

        public int ConnectionTransactionColumnId { get; set; }
        public ConnectionTransactionColumn ConnectionTransactionColumn { get; set; }

        public int UserId { get; set; }
        public User User { get; set; }



    }
}