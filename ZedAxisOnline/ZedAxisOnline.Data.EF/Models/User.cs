using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace ZedAxisOnline.Data.EF.Models
{
    public class User
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public byte[] PasswordHash { get; set; }
        public byte[] PasswordSalt { get; set; }
        public bool IsActive { get; set; }
        public string Photo { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime ModifiedOn { get; set; }
        public DateTime LastActive { get; set; }
        public string QuickbookSub { get; set; }

        public int RoleId { get; set; }
        public Role Role { get; set; }

        public ICollection<ZohoConnection> ZohoConnections { get; set; }
        public ICollection<AmazonConnection> AmazonConnections { get; set; }

        
        public ICollection<QuickbookSampleData> QuickBookSampleData { get; set; }
        public ICollection<UserQuickbookConnection> UserQuickbookConnections { get; set; }
        public ICollection<Mapping> Mappings { get; set; }
        public ICollection<MappingFunction> MappingFunctions { get; set; }
        public ICollection<QuickbookImportSummary> QuickBookImportSummarys { get; set; }

        public ICollection<QuickbookAccountSetting> QuickbookAccountSettings { get; set; }
    }



}