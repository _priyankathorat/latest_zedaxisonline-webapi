using System;
using System.Collections.Generic;

namespace ZedAxisOnline.Data.EF.Models
{
    public class Mapping
    {
        public int Id { get; set; }
        public string Name { get; set; }
       
        public DateTime CreatedOn { get; set; }
        public DateTime ModifiedOn { get; set; }

        public int UserId { get; set; }
        public User User { get; set; }

        
        public int ConnectionTransactionTypeId { get; set; }
        public ConnectionTransactionType ConnectionTransactionType { get; set; }

        public int QuickbookTransactionTypeId { get; set; }
        public QuickbookTransactionType QuickbookTransactionType { get; set; }
        public ICollection<MappingDetail> MappingDetails { get; set; }
        public ICollection<ZohoInvoice> ZohoInvoice { get; set; }
        public ICollection<QuickbookImportSummary> QuickBookImportSummarys { get; set; }
    }
}