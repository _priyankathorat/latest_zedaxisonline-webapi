﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;

namespace ZedAxisOnline.Data.EF
{
    /// <summary>
    /// This class is used when we seperate our project based on the Layers
    /// Note: If you are adding migration from here then dont forget to make it as "start up project"
    /// </summary>
    public class ApplicationDbContextFactory : IDesignTimeDbContextFactory<ApplicationDbContext>
    {
        public ApplicationDbContext CreateDbContext(string[] args)
        {
            var builder = new DbContextOptionsBuilder<ApplicationDbContext>();
            //    builder.UseSqlServer(@"Server=.\SQLEXPRESS;Database=ZedAxisOnlineImprovedDB;User id=sa;Password=Reset1234;"
            //builder.UseSqlServer(@"Server=WELCOME-PC;Database=ZedAxisOnlineImprovedDB;User id=sa;Password=Reset1234;"
            builder.UseSqlServer(@"Server=WIN-C3TUPMJK7MO;Database=ZedAxisOnlineImprovedDB;User id=OBAdmin;Password=uRkRpuT2P8;"
           ); 
            return new ApplicationDbContext(builder.Options);
        }
    }
}
