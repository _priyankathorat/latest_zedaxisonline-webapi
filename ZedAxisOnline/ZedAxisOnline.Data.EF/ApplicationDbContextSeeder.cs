﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ZedAxisOnline.Data.EF.Models;

namespace ZedAxisOnline.Data.EF
{
    public class ApplicationDbContextSeeder : IApplicationDbContextSeeder
    {
        private readonly ApplicationDbContext applicationDbContext;

        public ApplicationDbContextSeeder(ApplicationDbContext applicationDbContext)
        {
            this.applicationDbContext = applicationDbContext;
        }
        public async Task<bool> SeedAmazonColumnDetails()
        {
          int transTypeId=  applicationDbContext.ConnectionTransactionType.Where(x => x.TransType == "Settlements").FirstOrDefault().Id;

            var ConnectionTransactionColumns = new List<ConnectionTransactionColumn>()
                    {
                        new ConnectionTransactionColumn()
                        {
                            Name="AmazonOrderId",
                            SequenceNumber=1,
                        },
                          new ConnectionTransactionColumn()
                        {
                            Name="SellerOrderId",
                            SequenceNumber=2,
                        },
                           new ConnectionTransactionColumn()
                        {
                            Name="PurchaseDate",
                            SequenceNumber=3,
                        },
                              new ConnectionTransactionColumn()
                        {
                            Name="LastUpdateDate",
                            SequenceNumber=4,
                        },
                        new ConnectionTransactionColumn()
                        {
                            Name="OrderStatus",
                            SequenceNumber=5,
                        },
                        new ConnectionTransactionColumn()
                        {
                            Name="FulfillmentChannel",
                            SequenceNumber=6,
                        },
                        new ConnectionTransactionColumn()
                        {
                            Name="SalesChannel",
                            SequenceNumber=7,
                        },
                        new ConnectionTransactionColumn()
                        {
                            Name="OrderChannel",
                            SequenceNumber=8,
                        },
                        new ConnectionTransactionColumn()
                        {
                            Name="ShipServiceLevel",
                            SequenceNumber=9,
                        },
                        new ConnectionTransactionColumn()
                        {
                            Name="ShippingAddress",
                            SequenceNumber=10,
                        },
                         new ConnectionTransactionColumn()
                        {
                            Name="OrderTotal",
                            SequenceNumber=11,
                        },
                          new ConnectionTransactionColumn()
                        {
                            Name="NumberOfItemsShipped",
                            SequenceNumber=12,
                        },
                         new ConnectionTransactionColumn()
                        {
                            Name="NumberOfItemsUnshipped",
                            SequenceNumber=13,
                        },
                        new ConnectionTransactionColumn()
                        {
                            Name="PaymentExecutionDetail",
                            SequenceNumber=14,
                        },
                        new ConnectionTransactionColumn()
                        {
                            Name="PaymentMethod",
                            SequenceNumber=15,
                        },
                        new ConnectionTransactionColumn()
                        {
                            Name="PaymentMethodDetails",
                            SequenceNumber=16,
                        },
                         new ConnectionTransactionColumn()
                        {
                            Name="IsReplacementOrder",
                            SequenceNumber=17,
                        },
                        new ConnectionTransactionColumn()
                        {
                            Name="ReplacedOrderId",
                            SequenceNumber=18,
                        },
                         new ConnectionTransactionColumn()
                        {
                            Name="MarketplaceId",
                            SequenceNumber=19,
                        },
                         new ConnectionTransactionColumn()
                        {
                            Name="BuyerEmail",
                            SequenceNumber=20,
                        },
                            new ConnectionTransactionColumn()
                        {
                            Name="BuyerName",
                            SequenceNumber=21,
                        },
                               new ConnectionTransactionColumn()
                        {
                            Name="BuyerCounty",
                            SequenceNumber=22,
                        },
                                   new ConnectionTransactionColumn()
                        {
                            Name="BuyerTaxInfo",
                            SequenceNumber=23,
                        },
                                       new ConnectionTransactionColumn()
                        {
                            Name="ShipmentServiceLevelCategory",
                            SequenceNumber=24,
                        },
                               new ConnectionTransactionColumn()
                        {
                            Name="ShippedByAmazonTFM",
                            SequenceNumber=25,
                        },
                                   new ConnectionTransactionColumn()
                        {
                            Name="TFMShipmentStatus",
                            SequenceNumber=26,
                        },
                                       new ConnectionTransactionColumn()
                        {
                            Name="EasyShipShipmentStatus",
                            SequenceNumber=27,
                        },
                               new ConnectionTransactionColumn()
                        {
                            Name="OrderType",
                            SequenceNumber=28,
                        },
                                   new ConnectionTransactionColumn()
                        {
                            Name="EarliestDeliveryDate",
                            SequenceNumber=29,
                        },
                                       new ConnectionTransactionColumn()
                        {
                            Name="LatestDeliveryDate",
                            SequenceNumber=30,
                        },
                               new ConnectionTransactionColumn()
                        {
                            Name="IsBusinessOrder",
                            SequenceNumber=31,
                        },
                                   new ConnectionTransactionColumn()
                        {
                            Name="PurchaseOrderNumber",
                            SequenceNumber=32,
                        },
                                       new ConnectionTransactionColumn()
                        {
                            Name="IsPrime",
                            SequenceNumber=33,
                        },
                               new ConnectionTransactionColumn()
                        {
                            Name="IsPremiumOrder",
                            SequenceNumber=34,
                        },
                                   new ConnectionTransactionColumn()
                        {
                            Name="PromiseResponseDueDate",
                            SequenceNumber=35,
                        },
                                       new ConnectionTransactionColumn()
                        {
                            Name="IsEstimatedShipDateSet",
                            SequenceNumber=36,
                        }
                    };
            foreach (var item in ConnectionTransactionColumns)
            {
                item.ConnectionTransactionTypeId = transTypeId;
                applicationDbContext.ConnectionTransactionColumn.Add(item);
               await applicationDbContext.SaveChangesAsync();

            }

            return true;

            }
            public async Task<bool> SeedZohoColumnDetails()
        {
            ConnectionType connectionType = new ConnectionType()
            {
                Name = "Zoho",
                ConnectionTransactionTypes = new List<ConnectionTransactionType>()
            {
                new ConnectionTransactionType()
                {
                    TransType = "Invoice",

                    ConnectionTransactionColumns =new List<ConnectionTransactionColumn>()
                    {
                        new ConnectionTransactionColumn()
                        {
                            Name="invoice_id",
                            SequenceNumber=1,
                        },
                          new ConnectionTransactionColumn()
                        {
                            Name="status",
                            SequenceNumber=2,
                        },
                           new ConnectionTransactionColumn()
                        {
                            Name="invoice_date",
                            SequenceNumber=3,
                        },
                              new ConnectionTransactionColumn()
                        {
                            Name="customer_name",
                            SequenceNumber=4,
                        },
                        new ConnectionTransactionColumn()
                        {
                            Name="email",
                            SequenceNumber=5,
                        },
                        new ConnectionTransactionColumn()
                        {
                            Name="invoice_items_item_id",
                            SequenceNumber=6,
                        },
                        new ConnectionTransactionColumn()
                        {
                            Name="invoice_items_name",
                            SequenceNumber=7,
                        },
                        new ConnectionTransactionColumn()
                        {
                            Name="invoice_items_description",
                            SequenceNumber=8,
                        },
                        new ConnectionTransactionColumn()
                        {
                            Name="invoice_items_price",
                            SequenceNumber=9,
                        },
                        new ConnectionTransactionColumn()
                        {
                            Name="invoice_items_quantity",
                            SequenceNumber=10,
                        },
                         new ConnectionTransactionColumn()
                        {
                            Name="invoice_items_total",
                            SequenceNumber=11,
                        },
                          new ConnectionTransactionColumn()
                        {
                            Name="invoice_items_discount_amount",
                            SequenceNumber=12,
                        },
                         new ConnectionTransactionColumn()
                        {
                            Name="billing_address_city",
                            SequenceNumber=13,
                        },
                        new ConnectionTransactionColumn()
                        {
                            Name="billing_address_state",
                            SequenceNumber=14,
                        },
                        new ConnectionTransactionColumn()
                        {
                            Name="billing_address_zip",
                            SequenceNumber=15,
                        },
                        new ConnectionTransactionColumn()
                        {
                            Name="billing_address_country",
                            SequenceNumber=16,
                        },
                         new ConnectionTransactionColumn()
                        {
                            Name="shipping_address_street",
                            SequenceNumber=17,
                        },
                        new ConnectionTransactionColumn()
                        {
                            Name="shipping_address_state",
                            SequenceNumber=18,
                        },
                         new ConnectionTransactionColumn()
                        {
                            Name="shipping_address_zip",
                            SequenceNumber=19,
                        },
                         new ConnectionTransactionColumn()
                        {
                            Name="shipping_address_country",
                            SequenceNumber=20,
                        },
                            new ConnectionTransactionColumn()
                        {
                            Name="total",
                            SequenceNumber=21,
                        },
                               new ConnectionTransactionColumn()
                        {
                            Name="currency_code",
                            SequenceNumber=22,
                        }
                    }
                }
            }
            };
            try
            {

            
            await applicationDbContext.ConnectionType.AddAsync(connectionType);
            await applicationDbContext.SaveChangesAsync();
            }
            catch (System.Exception ex)
            {

                var exd = ex.Message;
            }
            return true;
        }


    }
}
