﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using ZedAxisOnline.Data.EF.Models;

namespace ZedAxisOnline.Data.EF.Configuration
{
    internal class ConnectionTransactionColumnConfiguration : IEntityTypeConfiguration<ConnectionTransactionColumn>
    {
        public void Configure(EntityTypeBuilder<ConnectionTransactionColumn> builder)
        {
            builder.HasKey(x => x.Id);
            builder.Property(x => x.Id).UseSqlServerIdentityColumn();

            builder.HasOne(x => x.ConnectionTransactionType)
                .WithMany(x => x.ConnectionTransactionColumns)
                .HasForeignKey(x => x.ConnectionTransactionTypeId)
                .OnDelete(DeleteBehavior.Cascade);
        }
    }
}

