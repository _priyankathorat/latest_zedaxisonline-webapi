﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using ZedAxisOnline.Data.EF.Models;

namespace ZedAxisOnline.Data.EF.Configuration
{
    internal class QuickbookTransactionTypeConfiguration : IEntityTypeConfiguration<QuickbookTransactionType>
    {
        public void Configure(EntityTypeBuilder<QuickbookTransactionType> builder)
        {
            builder.HasKey(x => x.Id);
            builder.Property(x => x.Id).UseSqlServerIdentityColumn();
        }
    }
}

