﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using ZedAxisOnline.Data.EF.Models;

namespace ZedAxisOnline.Data.EF.Configuration
{
    internal class MappingFunctionDetailConfiguration : IEntityTypeConfiguration<MappingFunctionDetail>
    {
        public void Configure(EntityTypeBuilder<MappingFunctionDetail> builder)
        {
            builder.HasKey(x => x.Id);
            builder.Property(x => x.Id).UseSqlServerIdentityColumn();

            builder.HasOne(x => x.MappingFunction)
                .WithMany(x => x.MappingFunctionDetails)
                .HasForeignKey(x => x.MappingFunctionId)
                .OnDelete(DeleteBehavior.Cascade);
        }
    }
}

