﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using ZedAxisOnline.Data.EF.Models;

namespace ZedAxisOnline.Data.EF.Configuration
{
    internal class AmazonConnectionConfiguration : IEntityTypeConfiguration<AmazonConnection>
    {
        public void Configure(EntityTypeBuilder<AmazonConnection> builder)
        {
            builder.HasKey(x => x.Id);
            builder.Property(x => x.Id).UseSqlServerIdentityColumn();

            builder.Property(x => x.AccessToken).IsRequired();
            builder.HasOne(x => x.User).WithMany(x => x.AmazonConnections).HasForeignKey(x => x.UserId);
        }
    }
}

