﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using ZedAxisOnline.Data.EF.Models;

namespace ZedAxisOnline.Data.EF.Configuration
{
    internal class MappingConfiguration : IEntityTypeConfiguration<Mapping>
    {
        public void Configure(EntityTypeBuilder<Mapping> builder)
        {
            builder.HasKey(x => x.Id);
            builder.Property(x => x.Id).UseSqlServerIdentityColumn();

            builder.HasOne(x => x.User)
                .WithMany(x => x.Mappings)
                .HasForeignKey(x => x.UserId)
                .OnDelete(DeleteBehavior.Cascade);

            builder.HasOne(x => x.ConnectionTransactionType)
               .WithMany(x => x.Mappings)
               .HasForeignKey(x => x.ConnectionTransactionTypeId)
               .OnDelete(DeleteBehavior.Cascade);

            builder.HasOne(x => x.QuickbookTransactionType)
               .WithMany(x => x.Mappings)
               .HasForeignKey(x => x.QuickbookTransactionTypeId)
               .OnDelete(DeleteBehavior.Cascade);
        }
    }
}

