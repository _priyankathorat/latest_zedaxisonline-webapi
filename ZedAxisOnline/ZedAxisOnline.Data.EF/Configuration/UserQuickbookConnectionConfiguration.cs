﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using ZedAxisOnline.Data.EF.Models;

namespace ZedAxisOnline.Data.EF.Configuration
{
    internal class UserQuickbookConnectionConfiguration : IEntityTypeConfiguration<UserQuickbookConnection>
    {
        public void Configure(EntityTypeBuilder<UserQuickbookConnection> builder)
        {
            builder.HasKey(x => x.Id);
            builder.Property(x => x.Id).UseSqlServerIdentityColumn();
            builder.HasOne(x => x.User)
                .WithMany(x => x.UserQuickbookConnections)
                .HasForeignKey(x => x.UserId)
                .OnDelete(DeleteBehavior.Cascade);
        }
    }
}

