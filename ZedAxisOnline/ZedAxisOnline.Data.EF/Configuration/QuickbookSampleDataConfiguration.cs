﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using ZedAxisOnline.Data.EF.Models;

namespace ZedAxisOnline.Data.EF.Configuration
{
    internal class QuickbookSampleDataConfiguration : IEntityTypeConfiguration<QuickbookSampleData>
    {
        public void Configure(EntityTypeBuilder<QuickbookSampleData> builder)
        {
            builder.HasKey(x => x.Id);
            builder.Property(x => x.Id).UseSqlServerIdentityColumn();

            builder.HasOne(x => x.ConnectionTransactionColumn)
                .WithMany(x => x.QuickBookSampleData)
                .HasForeignKey(x => x.ConnectionTransactionColumnId)
                .OnDelete(DeleteBehavior.Cascade);

            builder.HasOne(x => x.User)
                .WithMany(x => x.QuickBookSampleData)
                .HasForeignKey(x => x.UserId)
                .OnDelete(DeleteBehavior.Cascade);

        }
    }
}

