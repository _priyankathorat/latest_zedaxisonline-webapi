﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using ZedAxisOnline.Data.EF.Models;

namespace ZedAxisOnline.Data.EF.Configuration
{
    internal class QuickbookAccountSettingConfiguration : IEntityTypeConfiguration<QuickbookAccountSetting>
    {
        public void Configure(EntityTypeBuilder<QuickbookAccountSetting> builder)
        {
            builder.HasKey(x => x.Id);
            builder.Property(x => x.Id).UseSqlServerIdentityColumn();

            builder.HasOne(x => x.User)
                .WithMany(x => x.QuickbookAccountSettings)
                .HasForeignKey(x => x.UserId)
                .OnDelete(DeleteBehavior.Cascade);
        }
    }
}

