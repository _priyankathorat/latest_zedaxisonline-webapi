﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using ZedAxisOnline.Data.EF.Models;

namespace ZedAxisOnline.Data.EF.Configuration
{
    internal class ZohoConnectionConfiguration : IEntityTypeConfiguration<ZohoConnection>
    {
        public void Configure(EntityTypeBuilder<ZohoConnection> builder)
        {
            builder.HasKey(x => x.Id);
            builder.Property(x => x.Id).UseSqlServerIdentityColumn();
            builder.Property(x => x.AuthToken).IsRequired();
            builder.HasOne(x => x.User).WithMany(x => x.ZohoConnections).HasForeignKey(x => x.UserId);
        }
    }
}

