﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using ZedAxisOnline.Data.EF.Models;

namespace ZedAxisOnline.Data.EF.Configuration
{
    internal class QuickbookImportSummaryConfiguration : IEntityTypeConfiguration<QuickbookImportSummary>
    {
        public void Configure(EntityTypeBuilder<QuickbookImportSummary> builder)
        {
            builder.HasKey(x => x.Id);
            builder.Property(x => x.Id).UseSqlServerIdentityColumn();

            builder.HasOne(x => x.User)
                .WithMany(x => x.QuickBookImportSummarys)
                .HasForeignKey(x => x.UserId)
                .OnDelete(DeleteBehavior.Restrict);

            builder.HasOne(x => x.Mapping)
                .WithMany(x => x.QuickBookImportSummarys)
                .HasForeignKey(x => x.MappingId)
                .OnDelete(DeleteBehavior.Cascade);

        }
    }
}

