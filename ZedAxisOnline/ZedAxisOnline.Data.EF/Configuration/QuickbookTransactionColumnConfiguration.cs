﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using ZedAxisOnline.Data.EF.Models;

namespace ZedAxisOnline.Data.EF.Configuration
{
    internal class QuickbookTransactionColumnConfiguration : IEntityTypeConfiguration<QuickbookTransactionColumn>
    {
        public void Configure(EntityTypeBuilder<QuickbookTransactionColumn> builder)
        {
            builder.HasKey(x => x.Id);
            builder.Property(x => x.Id).UseSqlServerIdentityColumn();

            builder.HasOne(x => x.QuickbookTransactionType)
                .WithMany(x => x.QuickbookTransactionColumns)
                .HasForeignKey(x => x.QuickbookTransactionTypeId)
                .OnDelete(DeleteBehavior.Cascade);
        }
    }
}

