﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using ZedAxisOnline.Data.EF.Models;

namespace ZedAxisOnline.Data.EF.Configuration
{
    internal class ZohoInvoiceConfiguration : IEntityTypeConfiguration<ZohoInvoice>
    {
        public void Configure(EntityTypeBuilder<ZohoInvoice> builder)
        {
            builder.HasKey(x => x.Id);
            builder.Property(x => x.Id).UseSqlServerIdentityColumn();

            builder.HasOne(x => x.Mapping).WithMany(x => x.ZohoInvoice).HasForeignKey(x => x.MappingId);
        }
    }
}

