﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using ZedAxisOnline.Data.EF.Models;

namespace ZedAxisOnline.Data.EF.Configuration
{
    internal class MappingDetailConfiguration : IEntityTypeConfiguration<MappingDetail>
    {
        public void Configure(EntityTypeBuilder<MappingDetail> builder)
        {
            builder.HasKey(x => x.Id);
            builder.Property(x => x.Id).UseSqlServerIdentityColumn();

            builder.HasOne(x => x.Mapping)
                .WithMany(x => x.MappingDetails)
                .HasForeignKey(x => x.MappingId)
                .OnDelete(DeleteBehavior.Restrict);

            builder.HasOne(x => x.MappingFunction)
                .WithMany(x => x.MappingDetails)
                .HasForeignKey(x => x.MappingFunctionId)
                .OnDelete(DeleteBehavior.SetNull);

            builder.HasOne(x => x.QuickbookTransactionColumn)
                .WithMany(x => x.MappingDetails)
                .HasForeignKey(x => x.QuickbookTransactionColumnId)
                .OnDelete(DeleteBehavior.Restrict);

            builder.HasOne(x => x.ConnectionTransactionColumn)
               .WithMany(x => x.MappingDetails)
               .HasForeignKey(x => x.ConnectionTransactionColumnId)
               .OnDelete(DeleteBehavior.SetNull);


        }
    }
}

