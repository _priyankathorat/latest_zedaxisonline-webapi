﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using ZedAxisOnline.Data.EF.Models;

namespace ZedAxisOnline.Data.EF.Configuration
{
    internal class ZohoInvoiceItemConfiguration : IEntityTypeConfiguration<ZohoInvoiceItem>
    {
        public void Configure(EntityTypeBuilder<ZohoInvoiceItem> builder)
        {
            builder.HasKey(x => x.Id);
            builder.Property(x => x.Id).UseSqlServerIdentityColumn();

            builder.HasOne(x => x.ZohoInvoice).WithMany(x => x.ZohoInvoiceItems).HasForeignKey(x => x.ZohoInvoiceId);
        }
    }
}

