﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using ZedAxisOnline.Data.EF.Models;

namespace ZedAxisOnline.Data.EF.Configuration
{
    internal class ConnectionTransactionTypeConfiguration : IEntityTypeConfiguration<ConnectionTransactionType>
    {
        public void Configure(EntityTypeBuilder<ConnectionTransactionType> builder)
        {
            builder.HasKey(x => x.Id);
            builder.Property(x => x.Id).UseSqlServerIdentityColumn();

            builder.HasOne(x => x.ConnectionType)
                .WithMany(x => x.ConnectionTransactionTypes)
                .HasForeignKey(x => x.ConnectionTypeId)
                .OnDelete(DeleteBehavior.Cascade);
        }
    }
}

