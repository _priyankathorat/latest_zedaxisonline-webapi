﻿using ZedAxisOnline.Data.EF.Configuration;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using ZedAxisOnline.Data.EF.Models;

namespace ZedAxisOnline.Data.EF
{
    public class ApplicationDbContext:DbContext
    {

        public DbSet<Role> Role { get; set; }
        public DbSet<User> User { get; set; }
        public DbSet<ZohoConnection> ZohoConnection { get; set; }
        public DbSet<ConnectionType> ConnectionType { get; set; }
        public DbSet<ConnectionTransactionType> ConnectionTransactionType { get; set; }
        public DbSet<ConnectionTransactionColumn> ConnectionTransactionColumn { get; set; }
        public DbSet<QuickbookSampleData> QuickbookSampleData { get; set; }
        public DbSet<QuickbookTransactionType> QuickbookTransactionType { get; set; }
        public DbSet<QuickbookTransactionColumn> QuickbookTransactionColumn { get; set; }

        public DbSet<UserQuickbookConnection> UserQuickbookConnection { get; set; }
        public DbSet<Mapping> Mapping { get; set; }
        public DbSet<MappingFunction> MappingFunction { get; set; }
        public DbSet<MappingFunctionDetail> MappingFunctionDetail { get; set; }

        public DbSet<MappingDetail> MappingDetail { get; set; }

        public DbSet<AmazonConnection> AmazonConnection { get; set; }

        public DbSet<ZohoInvoice> ZohoInvoice { get; set; }
        public DbSet<ZohoInvoiceItem> ZohoInvoiceItem { get; set; }

        public DbSet<QuickbookImportSummary> QuickbookImportSummary { get; set; }

        public DbSet<QuickbookAccountSetting> QuickbookAccountSetting { get; set; }
       


        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new UserConfiguration());
            modelBuilder.ApplyConfiguration(new RoleConfiguration());
            modelBuilder.ApplyConfiguration(new ZohoConnectionConfiguration()); 

            modelBuilder.ApplyConfiguration(new ConnectionTypeConfiguration()); 
            modelBuilder.ApplyConfiguration(new ConnectionTransactionTypeConfiguration()); 
            modelBuilder.ApplyConfiguration(new ConnectionTransactionColumnConfiguration()); 
            modelBuilder.ApplyConfiguration(new QuickbookSampleDataConfiguration());

            modelBuilder.ApplyConfiguration(new QuickbookTransactionTypeConfiguration()); 
            modelBuilder.ApplyConfiguration(new QuickbookTransactionColumnConfiguration()); 
            
            modelBuilder.ApplyConfiguration(new UserQuickbookConnectionConfiguration());

            modelBuilder.ApplyConfiguration(new MappingConfiguration());
            modelBuilder.ApplyConfiguration(new MappingFunctionConfiguration());
            modelBuilder.ApplyConfiguration(new MappingFunctionDetailConfiguration());

            modelBuilder.ApplyConfiguration(new MappingDetailConfiguration());

            modelBuilder.ApplyConfiguration(new AmazonConnectionConfiguration());

            modelBuilder.ApplyConfiguration(new ZohoInvoiceConfiguration());
            modelBuilder.ApplyConfiguration(new ZohoInvoiceItemConfiguration());

            modelBuilder.ApplyConfiguration(new QuickbookImportSummaryConfiguration());

            modelBuilder.ApplyConfiguration(new QuickbookAccountSettingConfiguration());
        }
    }
}
