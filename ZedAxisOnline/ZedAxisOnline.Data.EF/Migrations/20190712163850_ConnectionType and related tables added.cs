﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ZedAxisOnline.Data.EF.Migrations
{
    public partial class ConnectionTypeandrelatedtablesadded : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "QuickbookLoginInformation");

            migrationBuilder.AddColumn<string>(
                name: "QuickbookSub",
                table: "User",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "ConnectionType",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ConnectionType", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ConnectionTransactionType",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    TransType = table.Column<string>(nullable: true),
                    EventName = table.Column<string>(nullable: true),
                    ConnectionTypeId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ConnectionTransactionType", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ConnectionTransactionType_ConnectionType_ConnectionTypeId",
                        column: x => x.ConnectionTypeId,
                        principalTable: "ConnectionType",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ConnectionTransactionColumn",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: true),
                    ConnectionTransactionTypeId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ConnectionTransactionColumn", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ConnectionTransactionColumn_ConnectionTransactionType_ConnectionTransactionTypeId",
                        column: x => x.ConnectionTransactionTypeId,
                        principalTable: "ConnectionTransactionType",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "QuickbookSampleData",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    SampleData1 = table.Column<string>(nullable: true),
                    SampleData2 = table.Column<string>(nullable: true),
                    ConnectionTransactionColumnId = table.Column<int>(nullable: false),
                    UserId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_QuickbookSampleData", x => x.Id);
                    table.ForeignKey(
                        name: "FK_QuickbookSampleData_ConnectionTransactionColumn_ConnectionTransactionColumnId",
                        column: x => x.ConnectionTransactionColumnId,
                        principalTable: "ConnectionTransactionColumn",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_QuickbookSampleData_User_UserId",
                        column: x => x.UserId,
                        principalTable: "User",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_ConnectionTransactionColumn_ConnectionTransactionTypeId",
                table: "ConnectionTransactionColumn",
                column: "ConnectionTransactionTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_ConnectionTransactionType_ConnectionTypeId",
                table: "ConnectionTransactionType",
                column: "ConnectionTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_QuickbookSampleData_ConnectionTransactionColumnId",
                table: "QuickbookSampleData",
                column: "ConnectionTransactionColumnId");

            migrationBuilder.CreateIndex(
                name: "IX_QuickbookSampleData_UserId",
                table: "QuickbookSampleData",
                column: "UserId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "QuickbookSampleData");

            migrationBuilder.DropTable(
                name: "ConnectionTransactionColumn");

            migrationBuilder.DropTable(
                name: "ConnectionTransactionType");

            migrationBuilder.DropTable(
                name: "ConnectionType");

            migrationBuilder.DropColumn(
                name: "QuickbookSub",
                table: "User");

            migrationBuilder.CreateTable(
                name: "QuickbookLoginInformation",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    AccessToken = table.Column<string>(nullable: true),
                    ExpireOn = table.Column<DateTime>(nullable: false),
                    RealmId = table.Column<string>(nullable: true),
                    RefreshToken = table.Column<string>(nullable: true),
                    Sub = table.Column<string>(maxLength: 100, nullable: true),
                    UserId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_QuickbookLoginInformation", x => x.Id);
                    table.ForeignKey(
                        name: "FK_QuickbookLoginInformation_User_UserId",
                        column: x => x.UserId,
                        principalTable: "User",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_QuickbookLoginInformation_UserId",
                table: "QuickbookLoginInformation",
                column: "UserId");
        }
    }
}
