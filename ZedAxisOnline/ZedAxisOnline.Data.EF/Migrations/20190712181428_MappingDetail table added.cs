﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ZedAxisOnline.Data.EF.Migrations
{
    public partial class MappingDetailtableadded : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "MappingDetail",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ConstantValue = table.Column<string>(nullable: true),
                    MappingId = table.Column<int>(nullable: false),
                    QuickbookTransactionColumnId = table.Column<int>(nullable: false),
                    ConnectionTransactionColumnId = table.Column<int>(nullable: true),
                    MappingFunctionId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MappingDetail", x => x.Id);
                    table.ForeignKey(
                        name: "FK_MappingDetail_ConnectionTransactionColumn_ConnectionTransactionColumnId",
                        column: x => x.ConnectionTransactionColumnId,
                        principalTable: "ConnectionTransactionColumn",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.SetNull);
                    table.ForeignKey(
                        name: "FK_MappingDetail_MappingFunction_MappingFunctionId",
                        column: x => x.MappingFunctionId,
                        principalTable: "MappingFunction",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.SetNull);
                    table.ForeignKey(
                        name: "FK_MappingDetail_Mapping_MappingId",
                        column: x => x.MappingId,
                        principalTable: "Mapping",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_MappingDetail_QuickbookTransactionColumn_QuickbookTransactionColumnId",
                        column: x => x.QuickbookTransactionColumnId,
                        principalTable: "QuickbookTransactionColumn",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_MappingDetail_ConnectionTransactionColumnId",
                table: "MappingDetail",
                column: "ConnectionTransactionColumnId");

            migrationBuilder.CreateIndex(
                name: "IX_MappingDetail_MappingFunctionId",
                table: "MappingDetail",
                column: "MappingFunctionId");

            migrationBuilder.CreateIndex(
                name: "IX_MappingDetail_MappingId",
                table: "MappingDetail",
                column: "MappingId");

            migrationBuilder.CreateIndex(
                name: "IX_MappingDetail_QuickbookTransactionColumnId",
                table: "MappingDetail",
                column: "QuickbookTransactionColumnId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "MappingDetail");
        }
    }
}
