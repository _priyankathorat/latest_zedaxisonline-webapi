﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ZedAxisOnline.Data.EF.Migrations
{
    public partial class zohoinvoicerelatedtableadded : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "ZohoInvoiceDetail",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    IsImported = table.Column<bool>(nullable: false),
                    invoice_id = table.Column<string>(nullable: true),
                    customer_name = table.Column<string>(nullable: true),
                    email = table.Column<string>(nullable: true),
                    invoice_date = table.Column<DateTime>(nullable: false),
                    currency_code = table.Column<string>(nullable: true),
                    total = table.Column<double>(nullable: false),
                    status = table.Column<string>(nullable: true),
                    billing_address_city = table.Column<string>(nullable: true),
                    billing_address_state = table.Column<string>(nullable: true),
                    billing_address_zip = table.Column<string>(nullable: true),
                    billing_address_country = table.Column<string>(nullable: true),
                    shipping_address_street = table.Column<string>(nullable: true),
                    shipping_address_city = table.Column<string>(nullable: true),
                    shipping_address_state = table.Column<string>(nullable: true),
                    shipping_address_zip = table.Column<string>(nullable: true),
                    shipping_address_country = table.Column<string>(nullable: true),
                    MappingsId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ZohoInvoiceDetail", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ZohoInvoiceDetail_Mapping_MappingsId",
                        column: x => x.MappingsId,
                        principalTable: "Mapping",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ZohoInvoiceItem",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    item_id = table.Column<string>(nullable: true),
                    name = table.Column<string>(nullable: true),
                    description = table.Column<string>(nullable: true),
                    price = table.Column<string>(nullable: true),
                    discount_amount = table.Column<string>(nullable: true),
                    quantity = table.Column<string>(nullable: true),
                    item_total = table.Column<string>(nullable: true),
                    ZohoInvoiceDetailId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ZohoInvoiceItem", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ZohoInvoiceItem_ZohoInvoiceDetail_ZohoInvoiceDetailId",
                        column: x => x.ZohoInvoiceDetailId,
                        principalTable: "ZohoInvoiceDetail",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_ZohoInvoiceDetail_MappingsId",
                table: "ZohoInvoiceDetail",
                column: "MappingsId");

            migrationBuilder.CreateIndex(
                name: "IX_ZohoInvoiceItem_ZohoInvoiceDetailId",
                table: "ZohoInvoiceItem",
                column: "ZohoInvoiceDetailId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ZohoInvoiceItem");

            migrationBuilder.DropTable(
                name: "ZohoInvoiceDetail");
        }
    }
}
