﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ZedAxisOnline.Data.EF.Migrations
{
    public partial class MappingMappingFunctionanditsdetailstablesadded : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Mapping",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: true),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    ModifiedOn = table.Column<DateTime>(nullable: false),
                    UserId = table.Column<int>(nullable: false),
                    ConnectionTransactionTypeId = table.Column<int>(nullable: false),
                    QuickbookTransactionTypeId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Mapping", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Mapping_ConnectionTransactionType_ConnectionTransactionTypeId",
                        column: x => x.ConnectionTransactionTypeId,
                        principalTable: "ConnectionTransactionType",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Mapping_QuickbookTransactionType_QuickbookTransactionTypeId",
                        column: x => x.QuickbookTransactionTypeId,
                        principalTable: "QuickbookTransactionType",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Mapping_User_UserId",
                        column: x => x.UserId,
                        principalTable: "User",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "MappingFunction",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: true),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    ModifiedOn = table.Column<DateTime>(nullable: false),
                    UserId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MappingFunction", x => x.Id);
                    table.ForeignKey(
                        name: "FK_MappingFunction_User_UserId",
                        column: x => x.UserId,
                        principalTable: "User",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "MappingFunctionDetail",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    SearchFor = table.Column<string>(nullable: true),
                    ReplaceWith = table.Column<string>(nullable: true),
                    Expression = table.Column<string>(nullable: true),
                    MappingFunctionId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MappingFunctionDetail", x => x.Id);
                    table.ForeignKey(
                        name: "FK_MappingFunctionDetail_MappingFunction_MappingFunctionId",
                        column: x => x.MappingFunctionId,
                        principalTable: "MappingFunction",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Mapping_ConnectionTransactionTypeId",
                table: "Mapping",
                column: "ConnectionTransactionTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_Mapping_QuickbookTransactionTypeId",
                table: "Mapping",
                column: "QuickbookTransactionTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_Mapping_UserId",
                table: "Mapping",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_MappingFunction_UserId",
                table: "MappingFunction",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_MappingFunctionDetail_MappingFunctionId",
                table: "MappingFunctionDetail",
                column: "MappingFunctionId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Mapping");

            migrationBuilder.DropTable(
                name: "MappingFunctionDetail");

            migrationBuilder.DropTable(
                name: "MappingFunction");
        }
    }
}
