﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ZedAxisOnline.Data.EF.Migrations
{
    public partial class ZohoInvoiceDetailsrenamedtoZohoInvoice : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ZohoInvoiceItem_ZohoInvoiceDetail_ZohoInvoiceDetailId",
                table: "ZohoInvoiceItem");

            migrationBuilder.DropTable(
                name: "ZohoInvoiceDetail");

            migrationBuilder.RenameColumn(
                name: "ZohoInvoiceDetailId",
                table: "ZohoInvoiceItem",
                newName: "ZohoInvoiceId");

            migrationBuilder.RenameIndex(
                name: "IX_ZohoInvoiceItem_ZohoInvoiceDetailId",
                table: "ZohoInvoiceItem",
                newName: "IX_ZohoInvoiceItem_ZohoInvoiceId");

            migrationBuilder.CreateTable(
                name: "ZohoInvoice",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    IsImported = table.Column<bool>(nullable: false),
                    invoice_id = table.Column<string>(nullable: true),
                    customer_name = table.Column<string>(nullable: true),
                    email = table.Column<string>(nullable: true),
                    invoice_date = table.Column<DateTime>(nullable: false),
                    currency_code = table.Column<string>(nullable: true),
                    total = table.Column<double>(nullable: false),
                    status = table.Column<string>(nullable: true),
                    billing_address_city = table.Column<string>(nullable: true),
                    billing_address_state = table.Column<string>(nullable: true),
                    billing_address_zip = table.Column<string>(nullable: true),
                    billing_address_country = table.Column<string>(nullable: true),
                    shipping_address_street = table.Column<string>(nullable: true),
                    shipping_address_city = table.Column<string>(nullable: true),
                    shipping_address_state = table.Column<string>(nullable: true),
                    shipping_address_zip = table.Column<string>(nullable: true),
                    shipping_address_country = table.Column<string>(nullable: true),
                    MappingsId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ZohoInvoice", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ZohoInvoice_Mapping_MappingsId",
                        column: x => x.MappingsId,
                        principalTable: "Mapping",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_ZohoInvoice_MappingsId",
                table: "ZohoInvoice",
                column: "MappingsId");

            migrationBuilder.AddForeignKey(
                name: "FK_ZohoInvoiceItem_ZohoInvoice_ZohoInvoiceId",
                table: "ZohoInvoiceItem",
                column: "ZohoInvoiceId",
                principalTable: "ZohoInvoice",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ZohoInvoiceItem_ZohoInvoice_ZohoInvoiceId",
                table: "ZohoInvoiceItem");

            migrationBuilder.DropTable(
                name: "ZohoInvoice");

            migrationBuilder.RenameColumn(
                name: "ZohoInvoiceId",
                table: "ZohoInvoiceItem",
                newName: "ZohoInvoiceDetailId");

            migrationBuilder.RenameIndex(
                name: "IX_ZohoInvoiceItem_ZohoInvoiceId",
                table: "ZohoInvoiceItem",
                newName: "IX_ZohoInvoiceItem_ZohoInvoiceDetailId");

            migrationBuilder.CreateTable(
                name: "ZohoInvoiceDetail",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    IsImported = table.Column<bool>(nullable: false),
                    MappingsId = table.Column<int>(nullable: false),
                    billing_address_city = table.Column<string>(nullable: true),
                    billing_address_country = table.Column<string>(nullable: true),
                    billing_address_state = table.Column<string>(nullable: true),
                    billing_address_zip = table.Column<string>(nullable: true),
                    currency_code = table.Column<string>(nullable: true),
                    customer_name = table.Column<string>(nullable: true),
                    email = table.Column<string>(nullable: true),
                    invoice_date = table.Column<DateTime>(nullable: false),
                    invoice_id = table.Column<string>(nullable: true),
                    shipping_address_city = table.Column<string>(nullable: true),
                    shipping_address_country = table.Column<string>(nullable: true),
                    shipping_address_state = table.Column<string>(nullable: true),
                    shipping_address_street = table.Column<string>(nullable: true),
                    shipping_address_zip = table.Column<string>(nullable: true),
                    status = table.Column<string>(nullable: true),
                    total = table.Column<double>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ZohoInvoiceDetail", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ZohoInvoiceDetail_Mapping_MappingsId",
                        column: x => x.MappingsId,
                        principalTable: "Mapping",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_ZohoInvoiceDetail_MappingsId",
                table: "ZohoInvoiceDetail",
                column: "MappingsId");

            migrationBuilder.AddForeignKey(
                name: "FK_ZohoInvoiceItem_ZohoInvoiceDetail_ZohoInvoiceDetailId",
                table: "ZohoInvoiceItem",
                column: "ZohoInvoiceDetailId",
                principalTable: "ZohoInvoiceDetail",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
