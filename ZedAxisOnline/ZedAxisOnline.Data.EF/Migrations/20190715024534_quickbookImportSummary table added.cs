﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ZedAxisOnline.Data.EF.Migrations
{
    public partial class quickbookImportSummarytableadded : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "QuickbookImportSummary",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ImportType = table.Column<string>(nullable: true),
                    IsImported = table.Column<bool>(nullable: false),
                    IsShow = table.Column<bool>(nullable: false),
                    TransactionId = table.Column<string>(nullable: true),
                    ViewUrl = table.Column<string>(nullable: true),
                    Status = table.Column<string>(nullable: true),
                    Error = table.Column<string>(nullable: true),
                    Message = table.Column<string>(nullable: true),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    UpdatedDate = table.Column<DateTime>(nullable: false),
                    UserId = table.Column<int>(nullable: false),
                    MappingsId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_QuickbookImportSummary", x => x.Id);
                    table.ForeignKey(
                        name: "FK_QuickbookImportSummary_Mapping_MappingsId",
                        column: x => x.MappingsId,
                        principalTable: "Mapping",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_QuickbookImportSummary_User_UserId",
                        column: x => x.UserId,
                        principalTable: "User",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_QuickbookImportSummary_MappingsId",
                table: "QuickbookImportSummary",
                column: "MappingsId");

            migrationBuilder.CreateIndex(
                name: "IX_QuickbookImportSummary_UserId",
                table: "QuickbookImportSummary",
                column: "UserId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "QuickbookImportSummary");
        }
    }
}
