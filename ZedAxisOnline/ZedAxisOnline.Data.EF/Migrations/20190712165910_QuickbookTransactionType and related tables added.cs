﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ZedAxisOnline.Data.EF.Migrations
{
    public partial class QuickbookTransactionTypeandrelatedtablesadded : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "QuickbookTransactionType",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_QuickbookTransactionType", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "QuickbookTransactionColumn",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: true),
                    QuickbookTransactionTypeId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_QuickbookTransactionColumn", x => x.Id);
                    table.ForeignKey(
                        name: "FK_QuickbookTransactionColumn_QuickbookTransactionType_QuickbookTransactionTypeId",
                        column: x => x.QuickbookTransactionTypeId,
                        principalTable: "QuickbookTransactionType",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_QuickbookTransactionColumn_QuickbookTransactionTypeId",
                table: "QuickbookTransactionColumn",
                column: "QuickbookTransactionTypeId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "QuickbookTransactionColumn");

            migrationBuilder.DropTable(
                name: "QuickbookTransactionType");
        }
    }
}
