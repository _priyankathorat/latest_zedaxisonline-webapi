﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ZedAxisOnline.Data.EF.Migrations
{
    public partial class ZohoConnectiontableupdated : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "AccessToken",
                table: "ZohoConnection");

            migrationBuilder.RenameColumn(
                name: "TokenType",
                table: "ZohoConnection",
                newName: "WebhookUrl");

            migrationBuilder.RenameColumn(
                name: "RefreshToken",
                table: "ZohoConnection",
                newName: "AuthToken");

            migrationBuilder.RenameColumn(
                name: "ExpireInSec",
                table: "ZohoConnection",
                newName: "OrganizationName");

            migrationBuilder.AddColumn<DateTime>(
                name: "ModifiedOn",
                table: "ZohoConnection",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "OrganizationId",
                table: "ZohoConnection",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ModifiedOn",
                table: "ZohoConnection");

            migrationBuilder.DropColumn(
                name: "OrganizationId",
                table: "ZohoConnection");

            migrationBuilder.RenameColumn(
                name: "WebhookUrl",
                table: "ZohoConnection",
                newName: "TokenType");

            migrationBuilder.RenameColumn(
                name: "OrganizationName",
                table: "ZohoConnection",
                newName: "ExpireInSec");

            migrationBuilder.RenameColumn(
                name: "AuthToken",
                table: "ZohoConnection",
                newName: "RefreshToken");

            migrationBuilder.AddColumn<string>(
                name: "AccessToken",
                table: "ZohoConnection",
                nullable: false,
                defaultValue: "");
        }
    }
}
