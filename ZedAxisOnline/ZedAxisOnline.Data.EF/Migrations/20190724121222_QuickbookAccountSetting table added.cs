﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ZedAxisOnline.Data.EF.Migrations
{
    public partial class QuickbookAccountSettingtableadded : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "QuickbookAccountSetting",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Type = table.Column<string>(nullable: true),
                    TaxCode = table.Column<string>(nullable: true),
                    TaxCodeId = table.Column<string>(nullable: true),
                    IncomeAccount = table.Column<string>(nullable: true),
                    IncomeAccountId = table.Column<string>(nullable: true),
                    COGSAccount = table.Column<string>(nullable: true),
                    COGSAccountId = table.Column<string>(nullable: true),
                    AssetAccount = table.Column<string>(nullable: true),
                    AssetAccountId = table.Column<string>(nullable: true),
                    CreateOn = table.Column<DateTime>(nullable: false),
                    UserId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_QuickbookAccountSetting", x => x.Id);
                    table.ForeignKey(
                        name: "FK_QuickbookAccountSetting_User_UserId",
                        column: x => x.UserId,
                        principalTable: "User",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_QuickbookAccountSetting_UserId",
                table: "QuickbookAccountSetting",
                column: "UserId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "QuickbookAccountSetting");
        }
    }
}
