﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace ZedAxisOnline.Data.EF.Migrations
{
    public partial class Updatedusertable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_QuickbookImportSummary_Mapping_MappingsId",
                table: "QuickbookImportSummary");

            migrationBuilder.DropForeignKey(
                name: "FK_ZohoInvoice_Mapping_MappingsId",
                table: "ZohoInvoice");

            migrationBuilder.RenameColumn(
                name: "MappingsId",
                table: "ZohoInvoice",
                newName: "MappingId");

            migrationBuilder.RenameIndex(
                name: "IX_ZohoInvoice_MappingsId",
                table: "ZohoInvoice",
                newName: "IX_ZohoInvoice_MappingId");

            migrationBuilder.RenameColumn(
                name: "MappingsId",
                table: "QuickbookImportSummary",
                newName: "MappingId");

            migrationBuilder.RenameIndex(
                name: "IX_QuickbookImportSummary_MappingsId",
                table: "QuickbookImportSummary",
                newName: "IX_QuickbookImportSummary_MappingId");

            migrationBuilder.AddColumn<string>(
                name: "QboSub",
                table: "User",
                nullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_QuickbookImportSummary_Mapping_MappingId",
                table: "QuickbookImportSummary",
                column: "MappingId",
                principalTable: "Mapping",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_ZohoInvoice_Mapping_MappingId",
                table: "ZohoInvoice",
                column: "MappingId",
                principalTable: "Mapping",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_QuickbookImportSummary_Mapping_MappingId",
                table: "QuickbookImportSummary");

            migrationBuilder.DropForeignKey(
                name: "FK_ZohoInvoice_Mapping_MappingId",
                table: "ZohoInvoice");

            migrationBuilder.DropColumn(
                name: "QboSub",
                table: "User");

            migrationBuilder.RenameColumn(
                name: "MappingId",
                table: "ZohoInvoice",
                newName: "MappingsId");

            migrationBuilder.RenameIndex(
                name: "IX_ZohoInvoice_MappingId",
                table: "ZohoInvoice",
                newName: "IX_ZohoInvoice_MappingsId");

            migrationBuilder.RenameColumn(
                name: "MappingId",
                table: "QuickbookImportSummary",
                newName: "MappingsId");

            migrationBuilder.RenameIndex(
                name: "IX_QuickbookImportSummary_MappingId",
                table: "QuickbookImportSummary",
                newName: "IX_QuickbookImportSummary_MappingsId");

            migrationBuilder.AddForeignKey(
                name: "FK_QuickbookImportSummary_Mapping_MappingsId",
                table: "QuickbookImportSummary",
                column: "MappingsId",
                principalTable: "Mapping",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_ZohoInvoice_Mapping_MappingsId",
                table: "ZohoInvoice",
                column: "MappingsId",
                principalTable: "Mapping",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
