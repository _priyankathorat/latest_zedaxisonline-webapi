﻿using System.Threading.Tasks;

namespace ZedAxisOnline.Data.EF
{
    public interface IApplicationDbContextSeeder
    {
        Task<bool> SeedZohoColumnDetails();
    }
}