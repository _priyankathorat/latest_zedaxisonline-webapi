﻿using System;
using ZedAxisOnline.Data.Abstraction;
using System.Threading.Tasks;
using ZedAxisOnline.BusinessObjects.RequestModels;
using ZedAxisOnline.BusinessObjects.User;
using ZedAxisOnline.BusinessObjects.ResponseModel;
using System.IO;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Options;
using ZedAxisOnline.Configuration.AppSettingConfiguration;

namespace ZedAxisOnline.BusinessLogic
{
    public class UserManager : IUserManager
    {
        readonly IUserRepository _userRepository;
        private readonly IHostingEnvironment _hostingEnvironment;
        private readonly AppSettings _appSettings;

        public UserManager(IUserRepository userRepository, IHostingEnvironment hostingEnvironment, IOptions<AppSettings> appSettings)
        {
            _userRepository = userRepository;
            _hostingEnvironment = hostingEnvironment;
            _appSettings = appSettings.Value;
        }

        public async Task<UserResponse> GetUserProfileAsync(int userId)
        {
            var user = await _userRepository.GetUserAsync(userId);
            return CerateUserResponse(user);
        }

        public async Task<UserResponse> UpdateUserInformation(ProfileUpdateRequest model, int userId)
        {
            UserEntity user = new UserEntity
            {
                FirstName = model.FirstName,
                LastName = model.LastName,
                Role = model.Role
            };
            #region File save Logic
            var file = model.PhotoUpload;
            if(file !=null && file.Length > 0)
            {
                var name = Guid.NewGuid().ToString("N");

                var extension = Path.GetExtension(file.FileName);
                var rootpath = _hostingEnvironment.ContentRootPath;
                var uploads = Path.Combine(rootpath, "wwwroot\\profilePhotos");
                if (!Directory.Exists(uploads))
                {
                    Directory.CreateDirectory(uploads);
                }
                var filepath = Path.Combine(uploads, name + extension);
                using (var fileStream = new FileStream(filepath, FileMode.Create))
                {
                    await file.CopyToAsync(fileStream);
                    user.Photo = name + extension;
                }
            }
            #endregion
            var responseResult = await _userRepository.UpdateUserAsync(user, userId);
            return CerateUserResponse(responseResult);
        }

        #region Private Methods

        private UserResponse CerateUserResponse(UserEntity user)
        {
            return new UserResponse()
            {
                Email = user.Email,
                FirstName = user.FirstName,
                LastName = user.LastName,
                PhotoUrl = user.Photo != null ? _appSettings.BaseUrl +"profilePhotos/" + user.Photo : user.Photo,
                Role = user.Role
            };
        }
        #endregion
    }
}
