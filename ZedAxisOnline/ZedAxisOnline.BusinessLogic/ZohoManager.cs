﻿using System;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using ZedAxisOnline.Configuration.AppSettingConfiguration;
using System.Net.Http;
using Newtonsoft.Json;
using ZedAxisOnline.BusinessObjects.User;
using ZedAxisOnline.Data.Abstraction;
using ZedAxisOnline.BusinessObjects.Connection;
using ZedAxisOnline.BusinessObjects.ResponseModel;
using ZedAxisOnline.Configuration;
using ZedAxisOnline.BusinessObjects.RequestModels;
using System.Collections.Generic;

namespace ZedAxisOnline.BusinessLogic
{
    public class ZohoManager : IZohoManager
    {
        private readonly ThirdPartyConfigurations _thirdPartyConfiguration;
        private readonly IZohoConnectionRepository _zohoConnectionRepository;

        public ZohoManager(IOptions<ThirdPartyConfigurations> thirdPartyConfiguration, IZohoConnectionRepository zohoConnectionRepository)
        {
            _thirdPartyConfiguration = thirdPartyConfiguration.Value;
            _zohoConnectionRepository = zohoConnectionRepository;
        }

        public async Task<ZohoOrganizationResponse> OrgnizationDetailsAsync(string authToken, int userId)
        {
            ZohoOrganizationResponse responseModel = new ZohoOrganizationResponse();
            try
            {
                using (var httpClient = new HttpClient())
                {
                    httpClient.DefaultRequestHeaders.Add("Authorization", "Zoho-authtoken " + authToken);
                    var response = await httpClient.GetAsync(_thirdPartyConfiguration.Zoho.SubscriptionOrganizationUrl);
                    var rawResponseContent = await response.Content.ReadAsStringAsync();
                    var organisationList = JsonConvert.DeserializeObject<ZohoAuthResponseModel>(rawResponseContent);
                    if (organisationList.organizations == null)
                    {
                        responseModel.Message = MessagesConstant.InvalidUserTokenMessage;
                        responseModel.IsValid = false;
                        return responseModel;
                    }
                    if (organisationList.organizations.Length == 0)
                    {
                        responseModel.IsValid = false;
                        responseModel.Message = MessagesConstant.NoOrganizationAvailableMessage;
                        return responseModel;

                    }
                    bool isZohoConnectionExists = await _zohoConnectionRepository.IsZohoConnectionExists(authToken, userId);
                    if (isZohoConnectionExists)
                    {
                        responseModel.IsValid = false;
                        responseModel.Message = MessagesConstant.ZohoConnectionAlreadyExistMessage;
                        return responseModel;
                    }
                    else
                    {
                        foreach (var item in organisationList.organizations)
                        {
                            IdNameModel orgnisationDetails = new IdNameModel();
                            orgnisationDetails.Id = item.organization_id;
                            orgnisationDetails.Name = item.name;
                            responseModel.Organizations.Add(orgnisationDetails);
                        }
                    }
                    responseModel.IsValid = true;
                }
            }
            catch (Exception e)
            {
                responseModel.Message = e.Message;
            }
            return responseModel;
        }

        public async Task SaveConnectionAsync(ZohoSaveConnectionRequest saveConnection, int userId)
        {
            ZohoConnectionEntity objectToSave = new ZohoConnectionEntity
            {
                AuthToken = saveConnection.AuthToken,
                ConnectionName = saveConnection.ConnectionName,
                CreatedOn = DateTime.Now,
                ModifiedOn = DateTime.Now,
                OrganizationId = saveConnection.OrganizationId,
                OrganizationName = saveConnection.OrganizationName,
                UserId = userId
            };

            await _zohoConnectionRepository.AddZohoConnectionAsync(objectToSave);
        }

        public async Task SaveWebhookAsync(ZohoSaveWebhookRequest saveWebhookModel, int userId)
        {
            await _zohoConnectionRepository.UpdateWebhookUrlAsync(saveWebhookModel.Id, saveWebhookModel.WebhookUrl, userId);
        }

        public async Task GetInvoiceListFromZohoAPI(int connectionId, int mappingId)
        {
            var savedConnectionInfo =await _zohoConnectionRepository.FindById(connectionId);
            string url = _thirdPartyConfiguration.Zoho.InvoiceListUrl;
            using (var httpClient = new HttpClient())
            {
                try
                {
                    httpClient.DefaultRequestHeaders.Add("Authorization", "Zoho-authtoken " + savedConnectionInfo.AuthToken);
                    httpClient.DefaultRequestHeaders.Add("X-com-zoho-subscriptions-organizationid", savedConnectionInfo.OrganizationId);
                    var response = await httpClient.GetAsync(url);
                    var rawResponseContent = await response.Content.ReadAsStringAsync();
                    var InvoiceListDetails = JsonConvert.DeserializeObject<ZohoInvoiceListResponse>(rawResponseContent);
                    int Count = 0;
                    foreach (var item in InvoiceListDetails.invoices)
                    {
                        response = await httpClient.GetAsync(_thirdPartyConfiguration.Zoho.InvoiceDetailsUrl + item.invoice_id);
                        rawResponseContent = await response.Content.ReadAsStringAsync();
                        ZohoInvoiceResponse InvoiceDetails = JsonConvert.DeserializeObject<ZohoInvoiceResponse>(rawResponseContent);
                        ZohoInvoiceEntity zohoInvoiceDetails = new ZohoInvoiceEntity();
                        zohoInvoiceDetails.MappingId = mappingId;
                        zohoInvoiceDetails.invoice_id = InvoiceDetails.invoice.invoice_id;
                        zohoInvoiceDetails.invoice_date = InvoiceDetails.invoice.invoice_date;
                        zohoInvoiceDetails.email = InvoiceDetails.invoice.email;
                        zohoInvoiceDetails.status = InvoiceDetails.invoice.status;
                        zohoInvoiceDetails.customer_name = InvoiceDetails.invoice.customer_name;
                        zohoInvoiceDetails.currency_code = InvoiceDetails.invoice.currency_code;
                        zohoInvoiceDetails.total = InvoiceDetails.invoice.total;
                        zohoInvoiceDetails.status = InvoiceDetails.invoice.status;
                        if (InvoiceDetails.invoice.shipping_address != null)
                        {
                            zohoInvoiceDetails.shipping_address_street = InvoiceDetails.invoice.shipping_address.street;
                            zohoInvoiceDetails.shipping_address_city = InvoiceDetails.invoice.shipping_address.city;
                            zohoInvoiceDetails.shipping_address_country = InvoiceDetails.invoice.shipping_address.country;
                            zohoInvoiceDetails.shipping_address_state = InvoiceDetails.invoice.shipping_address.state;
                            zohoInvoiceDetails.shipping_address_zip = InvoiceDetails.invoice.shipping_address.zip;
                        }
                        if (InvoiceDetails.invoice.billing_address != null)
                        {
                            zohoInvoiceDetails.billing_address_city = InvoiceDetails.invoice.billing_address.city;
                            zohoInvoiceDetails.billing_address_country = InvoiceDetails.invoice.billing_address.country;
                            zohoInvoiceDetails.billing_address_state = InvoiceDetails.invoice.billing_address.state;
                            zohoInvoiceDetails.billing_address_zip = InvoiceDetails.invoice.billing_address.zip;
                        }

                      var saveZohoInvoiceAndGetId = await  _zohoConnectionRepository.AddZohoInvoiceAsync(zohoInvoiceDetails);

                        if (InvoiceDetails.invoice.invoice_items != null)
                        {
                            foreach (var invoiceItems in InvoiceDetails.invoice.invoice_items)
                            {
                                ZohoInvoiceItemEntity zohoInvoiceItems = new ZohoInvoiceItemEntity();
                                zohoInvoiceItems.ZohoInvoiceId = saveZohoInvoiceAndGetId;
                                zohoInvoiceItems.item_id = invoiceItems.item_id;
                                zohoInvoiceItems.name = invoiceItems.name;
                                zohoInvoiceItems.price = invoiceItems.price;
                                zohoInvoiceItems.quantity = invoiceItems.quantity;
                                zohoInvoiceItems.item_total = invoiceItems.item_total;
                                zohoInvoiceItems.description = invoiceItems.description;
                                zohoInvoiceItems.discount_amount = invoiceItems.discount_amount;
                                var saveZohoInvoiceItemAndGetId = await  _zohoConnectionRepository.AddZohoInvoiceItemAsync(zohoInvoiceItems);
                            }
                        }
                        Count++;
                        if (Count == 10)
                            break;
                    }
                }
                catch (Exception e)
                {
                    var ex = e.Message;
                }
            }
        }

        public async Task<IList<ZohoInvoiceEntity>> GetInvoiceListAsync(int mappingId)
        {
            var invoiceList = await _zohoConnectionRepository.FindInvoiceByMappingId(mappingId);
            return invoiceList;
        }
    }
}
