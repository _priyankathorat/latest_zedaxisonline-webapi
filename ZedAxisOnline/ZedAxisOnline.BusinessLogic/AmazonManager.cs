﻿using System;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using ZedAxisOnline.Configuration.AppSettingConfiguration;
using System.Net.Http;
using Newtonsoft.Json;
using ZedAxisOnline.BusinessObjects.User;
using ZedAxisOnline.Data.Abstraction;
using ZedAxisOnline.BusinessObjects.Connection;

namespace ZedAxisOnline.BusinessLogic
{
    public class AmazonManager : IAmazonManager
    {
        private readonly ThirdPartyConfigurations _thirdPartyConfiguration;
        private readonly IAmazonConnectionRepository _amazonConnectionRepository;

        public AmazonManager(IOptions<ThirdPartyConfigurations> thirdPartyConfiguration, IAmazonConnectionRepository amazonConnectionRepository)
        {
            _thirdPartyConfiguration = thirdPartyConfiguration.Value;
            _amazonConnectionRepository = amazonConnectionRepository;
        }

        public string GetAmazonSignInUrl()
        {
            return _thirdPartyConfiguration.Amazon.GetAmazonSignInUrl();
        }

        public async Task<bool> AmazonGetCode(string accessToken, int userId)
        {
            using (var httpClient1 = new HttpClient())
            {
                using (var request = new HttpRequestMessage(new HttpMethod("GET"), _thirdPartyConfiguration.Amazon.TokenInfoUrl))
                {
                    request.Headers.TryAddWithoutValidation("Authorization", "Bearer " + accessToken);
                    request.Headers.TryAddWithoutValidation("Content-Type", "application/json");

                    var response = await httpClient1.SendAsync(request);
                    var rawResponseContent = await response.Content.ReadAsStringAsync();
                    var AmazonResponseObject = JsonConvert.DeserializeObject<AmazonAuthResponseModel>(rawResponseContent);
                    if (string.IsNullOrEmpty(AmazonResponseObject.error))
                    {
                        AmazonConnectionEntity entity = new AmazonConnectionEntity();
                        entity.AccessToken = accessToken;
                        entity.AmazonUserId = AmazonResponseObject.user_id;
                        entity.CreatedOn = DateTime.Now;
                        entity.ModifiedOn = DateTime.Now;
                        entity.UserId = userId;
                        await _amazonConnectionRepository.SaveOrUpdateAmazonConnection(entity);
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
        }
    }
}
