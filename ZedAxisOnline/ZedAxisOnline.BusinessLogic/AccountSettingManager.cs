﻿using System.Collections.Generic;
using ZedAxisOnline.Data.Abstraction;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using ZedAxisOnline.BusinessObjects.QuickBook;

namespace ZedAxisOnline.BusinessLogic
{
    public class AccountSettingManager : IAccountSettingManager
    {
        private readonly IFunctionRepository _functionRepository;
        private readonly IConfiguration _config;

        public AccountSettingManager(IConfiguration config, IFunctionRepository functionRepository)
        {
            this._functionRepository = functionRepository;
            this._config = config;
        }

        public async Task<bool> GetAccountSettingData(int userId)
        {
            return true ;
        }

        

    }
}
