﻿using Microsoft.Extensions.Options;
using System;
using System.Threading.Tasks;
using Intuit.Ipp.Data;
using System.Linq;
using Intuit.Ipp.Core;
using Microsoft.EntityFrameworkCore;
using Intuit.Ipp.QueryFilter;
using System.Collections.ObjectModel;
using Intuit.Ipp.DataService;
using Intuit.Ipp.Exception;
using Microsoft.Extensions.Logging;
using ZedAxisOnline.Configuration.AppSettingConfiguration;
using ZedAxisOnline.Data.EF;
using ZedAxisOnline.Data.Abstraction;
using ZedAxisOnline.BusinessObjects.QuickBook;
using ZedAxisOnline.Data.EF.Models;

namespace ZedAxisOnline.BusinessLogic
{
    public class InvoiceFieldMapping :IInvoiceFieldMapping
    {
        private readonly IMappingRepository _mappingRepository;
        private readonly IZohoInvoiceDetailsRepository _ZohoInvoiceDetailRepository;
        private readonly IQuickbookDepartment _quickbookDepartment;
        private readonly IQuickbookCustomer _quickbookCustomer;
        private readonly IQuickbookItem _quickbookItem;
        private readonly IQuickbookAccount _quickbookAccount;
        private readonly IQuickbookTerm _quickbookTerm;
        private readonly IQuickbookImportSummary _quickbookImportSummary;
        private readonly IQuickbookClassRef _quickbookClassRef;
        private readonly ThirdPartyConfigurations _thirdPartyConfiguration;
        private readonly IQuickbookServiceContext _serviceContext;
        private readonly IQuickbookConnectionDetails _quickbookConnectionDetails;
        private readonly ApplicationDbContext _context;
        private readonly IQuickbookRepository _quickbook;
        //private readonly IQuickbookManager _quickbookManager;

        public InvoiceFieldMapping(
            ApplicationDbContext context,
            IMappingRepository mappingRepository,
            IOptions<ThirdPartyConfigurations> thirdPartyConfiguration,
            IZohoInvoiceDetailsRepository zohoInvoiceRepository,
            IQuickbookDepartment quickbookDepartment,
            IQuickbookCustomer quickbookCustomer,
            IQuickbookItem quickbookItem,
            IQuickbookClassRef quickbookClassRef,
            IQuickbookRepository quickbook,
            IQuickbookAccount quickbookAccount,
            IQuickbookTerm quickbookTerm,
            IQuickbookImportSummary quickbookImportSummary,
            IQuickbookServiceContext qboServiceContext,
            IQuickbookConnectionDetails quickbookConnectionDetails
            )
        {
            this._mappingRepository = mappingRepository;
            this._ZohoInvoiceDetailRepository = zohoInvoiceRepository;
            this._quickbookDepartment = quickbookDepartment;
            this._quickbookCustomer = quickbookCustomer;
            this._quickbookItem = quickbookItem;
            this._quickbookAccount = quickbookAccount;
            this._quickbookTerm = quickbookTerm;
            this._quickbookImportSummary = quickbookImportSummary;
            this._quickbookClassRef = quickbookClassRef;
            this._thirdPartyConfiguration = thirdPartyConfiguration.Value;
            this._serviceContext = qboServiceContext;
            this._quickbookConnectionDetails = quickbookConnectionDetails;
            this._context = context;
            this._quickbook = quickbook;
        }

        public async Task<bool> MapData(int userId, int mappingId)
        {
            try
            {
                QuickbookImportSummaryEntity quickbookImportSummaryModel = new QuickbookImportSummaryEntity();
            Invoice invoiceEntry = new Invoice();
            string tempName = "";
            var qboLoginDetails = await _context.UserQuickbookConnection.Where(x => x.UserId == userId).FirstAsync();
            var UserQboLogins = await _quickbookConnectionDetails.GetQboLoginDetails(qboLoginDetails.Id);
            string countryVersion = UserQboLogins.CountryVersion;
            string realmId = UserQboLogins.RealmId;
            ServiceContext serviceContext = _serviceContext.GetServiceContext(UserQboLogins.AccessToken, realmId);
            DataService dataService = new DataService(serviceContext);

              var details = await _mappingRepository.FindMappingByIdAsync(mappingId);
          //  var details = await _mappingRepository.GetMappingDetails(mappingId);

            var InvoiceDetailsList = await _ZohoInvoiceDetailRepository.GetZohoInvoiceDetails(mappingId);
            if (InvoiceDetailsList != null && InvoiceDetailsList.Count != 0)
            {
                foreach (var CurrentInvoiceDetails in InvoiceDetailsList)
                {
                    quickbookImportSummaryModel = new QuickbookImportSummaryEntity();
                    quickbookImportSummaryModel.MappingId = mappingId;
                    quickbookImportSummaryModel.ImportType = "Invoice";
                    quickbookImportSummaryModel.UserId = userId;
                    invoiceEntry = new Invoice();
                    invoiceEntry = await CreateItemLineObjects(invoiceEntry, details, CurrentInvoiceDetails, serviceContext, countryVersion);
                    invoiceEntry = CreateCustomFileds(invoiceEntry, details, CurrentInvoiceDetails);
                    invoiceEntry = CreateBillingAddress(invoiceEntry, details, CurrentInvoiceDetails);
                    invoiceEntry = CreateShippingAddresesFields(invoiceEntry, details, CurrentInvoiceDetails);
                    foreach (var mappingDetail in details.MappingDetails)
                    {
                        switch (mappingDetail.QuickbookTransactionColumn.Name)
                        {
                            case "DocNumber":
                                {
                                    if (!string.IsNullOrEmpty(mappingDetail.ConstantValue))
                                    {
                                        invoiceEntry.DocNumber = mappingDetail.ConstantValue;
                                    }
                                    else
                                    {
                                        invoiceEntry.DocNumber = getZohoInvoiceData(mappingDetail.ConnectionTransactionColumn.Name, CurrentInvoiceDetails);
                                    }
                                    break;
                                }
                            case "TxnDate":
                                {
                                    try
                                    {
                                        if (!string.IsNullOrEmpty(mappingDetail.ConstantValue))
                                        {
                                            invoiceEntry.TxnDate = Convert.ToDateTime(mappingDetail.ConstantValue);
                                            invoiceEntry.TxnDateSpecified = true;

                                        }
                                        else
                                        {
                                            var value = getZohoInvoiceData(mappingDetail.ConnectionTransactionColumn.Name,
                                                CurrentInvoiceDetails);
                                            invoiceEntry.TxnDate = Convert.ToDateTime(value);
                                            invoiceEntry.TxnDateSpecified = true;

                                        }
                                    }
                                    catch (Exception ex)
                                        {

                                        }
                                    
                                    break;
                                }
                            case "PrivateNote":
                                {
                                    if (!string.IsNullOrEmpty(mappingDetail.ConstantValue))
                                    {
                                        invoiceEntry.PrivateNote = mappingDetail.ConstantValue;
                                    }
                                    else
                                    {
                                        invoiceEntry.PrivateNote = getZohoInvoiceData(mappingDetail.ConnectionTransactionColumn.Name, CurrentInvoiceDetails);
                                    }
                                    break;
                                }
                            case "DueDate":
                                {
                                    if (!string.IsNullOrEmpty(mappingDetail.ConstantValue))
                                    {
                                        invoiceEntry.DueDate = Convert.ToDateTime(mappingDetail.ConstantValue);
                                        invoiceEntry.DueDateSpecified = true;
                                    }
                                    else
                                    {
                                        var value = getZohoInvoiceData(mappingDetail.ConnectionTransactionColumn.Name, CurrentInvoiceDetails);
                                        invoiceEntry.DueDate = Convert.ToDateTime(value);
                                        invoiceEntry.DueDateSpecified = true;
                                    }
                                    break;
                                }
                            case "AllowOnlineACHPayment":
                                {
                                    if (!string.IsNullOrEmpty(mappingDetail.ConstantValue))
                                    {
                                        invoiceEntry.AllowOnlineACHPayment = Convert.ToBoolean(mappingDetail.ConstantValue);
                                    }
                                    else
                                    {
                                        var value = getZohoInvoiceData(mappingDetail.ConnectionTransactionColumn.Name, CurrentInvoiceDetails);
                                        invoiceEntry.AllowOnlineACHPayment = Convert.ToBoolean(value);
                                    }
                                    break;
                                }
                            case "AllowOnlineCreditCardPayment":
                                {
                                    if (!string.IsNullOrEmpty(mappingDetail.ConstantValue))
                                    {
                                        invoiceEntry.AllowOnlineCreditCardPayment = Convert.ToBoolean(mappingDetail.ConstantValue);
                                    }
                                    else
                                    {
                                        var value = getZohoInvoiceData(mappingDetail.ConnectionTransactionColumn.Name, CurrentInvoiceDetails);
                                        invoiceEntry.AllowOnlineCreditCardPayment = Convert.ToBoolean(value);
                                    }
                                    break;
                                }
                            case "ShipDate":
                                {
                                    if (!string.IsNullOrEmpty(mappingDetail.ConstantValue))
                                    {
                                        invoiceEntry.ShipDate = Convert.ToDateTime(mappingDetail.ConstantValue);
                                    }
                                    else
                                    {
                                        var value = getZohoInvoiceData(mappingDetail.ConnectionTransactionColumn.Name, CurrentInvoiceDetails);
                                        invoiceEntry.ShipDate = Convert.ToDateTime(value);
                                    }
                                    break;
                                }
                            case "TrackingNum":
                                {
                                    if (!string.IsNullOrEmpty(mappingDetail.ConstantValue))
                                    {
                                        invoiceEntry.TrackingNum = mappingDetail.ConstantValue;
                                    }
                                    else
                                    {
                                        invoiceEntry.TrackingNum = getZohoInvoiceData(mappingDetail.ConnectionTransactionColumn.Name, CurrentInvoiceDetails);
                                    }
                                    break;
                                }
                            case "ApplyTaxAfterDiscount":
                                {
                                    if (!string.IsNullOrEmpty(mappingDetail.ConstantValue))
                                    {
                                        invoiceEntry.ApplyTaxAfterDiscount = Convert.ToBoolean(mappingDetail.ConstantValue);
                                    }
                                    else
                                    {
                                        var value = getZohoInvoiceData(mappingDetail.ConnectionTransactionColumn.Name, CurrentInvoiceDetails);
                                        invoiceEntry.ApplyTaxAfterDiscount = Convert.ToBoolean(value);
                                    }
                                    break;
                                }
                            case "ExchangeRate":
                                {
                                    if (!string.IsNullOrEmpty(mappingDetail.ConstantValue))
                                    {
                                        invoiceEntry.ExchangeRate = Convert.ToDecimal(mappingDetail.ConstantValue);
                                    }
                                    else
                                    {
                                        var value = getZohoInvoiceData(mappingDetail.ConnectionTransactionColumn.Name, CurrentInvoiceDetails);
                                        invoiceEntry.ExchangeRate = Convert.ToDecimal(value);
                                    }
                                    break;
                                }
                            case "BillEmail":
                                {
                                    if (!string.IsNullOrEmpty(mappingDetail.ConstantValue))
                                    {
                                        invoiceEntry.BillEmail = CreateEmailAddress(mappingDetail.ConstantValue);
                                    }
                                    else
                                    {
                                        invoiceEntry.BillEmail =
                                            CreateEmailAddress(getZohoInvoiceData(mappingDetail.ConnectionTransactionColumn.Name, CurrentInvoiceDetails));
                                    }
                                    break;
                                }
                            case "BillEmailCc":
                                {
                                    if (!string.IsNullOrEmpty(mappingDetail.ConstantValue))
                                    {
                                        invoiceEntry.BillEmailCc = CreateEmailAddress(mappingDetail.ConstantValue);
                                    }
                                    else
                                    {
                                        invoiceEntry.BillEmailCc =
                                            CreateEmailAddress(getZohoInvoiceData(mappingDetail.ConnectionTransactionColumn.Name, CurrentInvoiceDetails));
                                    }
                                    break;
                                }
                            case "BillEmailBcc":
                                {
                                    if (!string.IsNullOrEmpty(mappingDetail.ConstantValue))
                                    {
                                        invoiceEntry.BillEmailBcc = CreateEmailAddress(mappingDetail.ConstantValue);
                                    }
                                    else
                                    {
                                        invoiceEntry.BillEmailBcc =
                                            CreateEmailAddress(getZohoInvoiceData(mappingDetail.ConnectionTransactionColumn.Name, CurrentInvoiceDetails));
                                    }
                                    break;
                                }
                            case "Deposit":
                                {
                                    if (!string.IsNullOrEmpty(mappingDetail.ConstantValue))
                                    {
                                        invoiceEntry.Deposit = Convert.ToDecimal(mappingDetail.ConstantValue);
                                    }
                                    else
                                    {
                                        var value = getZohoInvoiceData(mappingDetail.ConnectionTransactionColumn.Name, CurrentInvoiceDetails);
                                        invoiceEntry.Deposit = Convert.ToDecimal(value);
                                    }
                                    break;
                                }
                            case "DepartmentRefName":
                                {
                                    tempName = "";
                                    invoiceEntry.DepartmentRef = new ReferenceType();
                                    if (!string.IsNullOrEmpty(mappingDetail.ConstantValue))
                                    {
                                        tempName = mappingDetail.ConstantValue;
                                    }
                                    else
                                    {
                                        tempName = getZohoInvoiceData(mappingDetail.ConnectionTransactionColumn.Name, CurrentInvoiceDetails);
                                    }
                                    if (tempName != "")
                                    {
                                        invoiceEntry.DepartmentRef = _quickbookDepartment.GetDepartmentDetails(serviceContext, tempName);
                                    }
                                    break;
                                }

                            case "CustomerRefDisplayName":
                                {
                                    tempName = "";
                                    if (!string.IsNullOrEmpty(mappingDetail.ConstantValue))
                                    {
                                        tempName = mappingDetail.ConstantValue;
                                    }
                                    else
                                    {
                                        tempName = getZohoInvoiceData(mappingDetail.ConnectionTransactionColumn.Name, CurrentInvoiceDetails);
                                    }
                                    if (tempName != "")
                                    {
                                        invoiceEntry.CustomerRef = _quickbookCustomer.GetCustomerDetails(serviceContext, tempName);
                                    }
                                    break;
                                }
                            case "CustomerMemo":
                                {
                                    tempName = "";

                                    if (!string.IsNullOrEmpty(mappingDetail.ConstantValue))
                                    {
                                        tempName = mappingDetail.ConstantValue;
                                    }
                                    else
                                    {
                                        tempName = getZohoInvoiceData(mappingDetail.ConnectionTransactionColumn.Name, CurrentInvoiceDetails);
                                    }
                                    invoiceEntry.CustomerMemo = new MemoRef()
                                    {
                                        Value = tempName,
                                    };
                                    break;
                                }
                            case "DepositToAccountRef":
                                {
                                    tempName = "";

                                    if (!string.IsNullOrEmpty(mappingDetail.ConstantValue))
                                    {
                                        tempName = mappingDetail.ConstantValue;
                                    }
                                    else
                                    {
                                        tempName = getZohoInvoiceData(mappingDetail.ConnectionTransactionColumn.Name, CurrentInvoiceDetails);
                                    }
                                    if (tempName != null && tempName != "")
                                    {
                                        invoiceEntry.DepositToAccountRef = _quickbookAccount.GetQuickbookAccountDetails(serviceContext, tempName);
                                    }
                                    break;
                                }
                            case "PrintStatus":
                                {
                                    tempName = "";
                                    if (!string.IsNullOrEmpty(mappingDetail.ConstantValue))
                                    {
                                        tempName = mappingDetail.ConstantValue;
                                    }
                                    else
                                    {
                                        tempName = getZohoInvoiceData(mappingDetail.ConnectionTransactionColumn.Name, CurrentInvoiceDetails);
                                    }
                                    if (tempName != null && tempName != "")
                                    {
                                        if (tempName == PrintStatusEnum.NotSet.ToString())
                                        {
                                            invoiceEntry.PrintStatus = PrintStatusEnum.NotSet;
                                            invoiceEntry.PrintStatusSpecified = true;
                                        }
                                        else if (tempName == PrintStatusEnum.NeedToPrint.ToString())
                                        {
                                            invoiceEntry.PrintStatus = PrintStatusEnum.NeedToPrint;
                                            invoiceEntry.PrintStatusSpecified = true;
                                        }
                                        else if (tempName == PrintStatusEnum.PrintComplete.ToString())
                                        {
                                            invoiceEntry.PrintStatus = PrintStatusEnum.PrintComplete;
                                            invoiceEntry.PrintStatusSpecified = true;
                                        }
                                    }
                                    break;
                                }
                            case "EmailStatus":
                                {
                                    tempName = "";

                                    if (!string.IsNullOrEmpty(mappingDetail.ConstantValue))
                                    {
                                        tempName = mappingDetail.ConstantValue;
                                    }
                                    else
                                    {
                                        tempName = getZohoInvoiceData(mappingDetail.ConnectionTransactionColumn.Name, CurrentInvoiceDetails);
                                    }
                                    if (tempName != null && tempName != "")
                                    {
                                        if (tempName == EmailStatusEnum.NotSet.ToString())
                                        {
                                            invoiceEntry.EmailStatus = EmailStatusEnum.NotSet;
                                            invoiceEntry.EmailStatusSpecified = true;
                                        }
                                        else if (tempName == EmailStatusEnum.EmailSent.ToString())
                                        {
                                            invoiceEntry.EmailStatus = EmailStatusEnum.EmailSent;
                                            invoiceEntry.EmailStatusSpecified = true;
                                        }
                                        else if (tempName == EmailStatusEnum.NeedToSend.ToString())
                                        {
                                            invoiceEntry.EmailStatus = EmailStatusEnum.NeedToSend;
                                            invoiceEntry.EmailStatusSpecified = true;
                                        }
                                    }
                                    break;
                                }
                            case "ShipMethodRef":
                                {
                                    tempName = "";

                                    if (!string.IsNullOrEmpty(mappingDetail.ConstantValue))
                                    {
                                        tempName = mappingDetail.ConstantValue;
                                    }
                                    else
                                    {
                                        tempName = getZohoInvoiceData(mappingDetail.ConnectionTransactionColumn.Name, CurrentInvoiceDetails);
                                    }
                                    if (tempName != null && tempName != "")
                                    {
                                        invoiceEntry.ShipMethodRef = new ReferenceType()
                                        {
                                            name = tempName,
                                            Value = tempName
                                        };
                                    }
                                    break;
                                }
                            case "GlobalTaxCalculation":
                                {
                                    tempName = "";

                                    if (!string.IsNullOrEmpty(mappingDetail.ConstantValue))
                                    {
                                        tempName = mappingDetail.ConstantValue;
                                    }
                                    else
                                    {
                                        tempName = getZohoInvoiceData(mappingDetail.ConnectionTransactionColumn.Name, CurrentInvoiceDetails);
                                    }
                                    if (tempName == GlobalTaxCalculationEnum.TaxInclusive.ToString())
                                    {
                                        invoiceEntry.GlobalTaxCalculation = GlobalTaxCalculationEnum.TaxInclusive;
                                    }

                                    break;
                                }
                            case "CurrencyRef":
                                {
                                    tempName = "";

                                    if (!string.IsNullOrEmpty(mappingDetail.ConstantValue))
                                    {
                                        tempName = mappingDetail.ConstantValue;
                                    }
                                    else
                                    {
                                        tempName = getZohoInvoiceData(mappingDetail.ConnectionTransactionColumn.Name, CurrentInvoiceDetails);
                                    }
                                    if (tempName != null && tempName != "")
                                    {
                                        invoiceEntry.CurrencyRef = new ReferenceType()
                                        {
                                            name = tempName,
                                            Value = tempName
                                        };
                                    }
                                    break;
                                }
                            case "SalesTermRefName":
                                {
                                    tempName = "";
                                    if (!string.IsNullOrEmpty(mappingDetail.ConstantValue))
                                    {
                                        tempName = mappingDetail.ConstantValue;
                                    }
                                    else
                                    {
                                        tempName = getZohoInvoiceData(mappingDetail.ConnectionTransactionColumn.Name, CurrentInvoiceDetails);
                                    }
                                    invoiceEntry.SalesRepRef = _quickbookTerm.GetQuickbookTermDetails(serviceContext, tempName);
                                    break;
                                }

                            //case "SKU":
                            //    {
                            //        if (!string.IsNullOrEmpty(mappingDetails.ConstantValue))
                            //        {
                            //            invoice.DocNumber = mappingDetails.ConstantValue;
                            //        }
                            //        else
                            //        {
                            //            invoice.DocNumber = getInvoiceData(mappingDetails.ConnectionTransactionColumn.Name, CurrentInvoiceDetails);
                            //        }
                            //        break;
                            //    }
                            //case "PrimaryPhone":
                            //    {
                            //        if (!string.IsNullOrEmpty(mappingDetails.ConstantValue))
                            //        {
                            //            invoice.DueDate = mappingDetails.ConstantValue;
                            //        }
                            //        else
                            //        {
                            //            invoice.DocNumber = getInvoiceData(mappingDetails.ConnectionTransactionColumn.Name, CurrentInvoiceDetails);
                            //        }
                            //        break;
                            //    }


                            default:
                                continue;
                        }
                    }
                    try
                    {
                        var addInvoice = dataService.Add<Invoice>(invoiceEntry);
                        quickbookImportSummaryModel.IsImported = true;
                        quickbookImportSummaryModel.Status = "Imported";
                        quickbookImportSummaryModel.TransactionId = addInvoice.Id;
                        quickbookImportSummaryModel.Message = "";
                    }
                    catch (IdsException ex)
                    {
                        quickbookImportSummaryModel.IsImported = false;
                        quickbookImportSummaryModel.Status = "Error";
                        quickbookImportSummaryModel.Error = string.Empty;
                        quickbookImportSummaryModel.Error = (((IdsException)(ex.InnerException)).InnerExceptions[0].Detail).ToString();
                    }
                    catch (Exception exx)
                    {
                        quickbookImportSummaryModel.IsImported = false;
                        quickbookImportSummaryModel.Status = "Error";
                        quickbookImportSummaryModel.Error = exx.InnerException.Message;
                    }

                 bool addImportSummary=  await _quickbookImportSummary.AddQuickbookImportSummary(quickbookImportSummaryModel);

                }
            }

            }
            catch (Exception e)
            {
                

            }
            return true;
        }

        private Invoice CreateCustomFileds(Invoice invoice, Mapping details, ZohoInvoice CurrentInvoiceDetails)
        {
            int customcount = 1;
            Intuit.Ipp.Data.CustomField[] custom_online = new Intuit.Ipp.Data.CustomField[customcount];

            CustomField[] customField = new CustomField[customcount];
            Intuit.Ipp.Data.CustomField custFiled = new Intuit.Ipp.Data.CustomField();
            var filteredData = details.MappingDetails.Where(x => x.QuickbookTransactionColumn.Name.Contains("CustomField"));

            foreach (var mappingDetails in filteredData)
            {

                switch (mappingDetails.QuickbookTransactionColumn.Name)
                {
                    case "CustomFieldName1":
                        {
                            if (!string.IsNullOrEmpty(mappingDetails.ConstantValue))
                            {
                                custFiled.Name = mappingDetails.ConstantValue;
                            }
                            else
                            {
                                custFiled.Name = getZohoInvoiceData(mappingDetails.ConnectionTransactionColumn.Name, CurrentInvoiceDetails);
                            }
                            break;
                        }
                    case "CustomFieldValue1":
                        {
                            invoice.CustomField[0].Type = CustomFieldTypeEnum.StringType;
                            if (!string.IsNullOrEmpty(mappingDetails.ConstantValue))
                            {
                                custFiled.AnyIntuitObject = mappingDetails.ConstantValue;
                            }
                            else
                            {
                                custFiled.AnyIntuitObject = getZohoInvoiceData(mappingDetails.ConnectionTransactionColumn.Name, CurrentInvoiceDetails);
                            }

                            custom_online[customcount - 1] = custFiled;

                            break;
                        }
                    case "CustomFieldName2":
                        {
                            custFiled = new CustomField();

                            custFiled.DefinitionId = customcount.ToString();
                            custFiled.Type = CustomFieldTypeEnum.StringType;
                            Array.Resize(ref custom_online, customcount);
                            customcount++;
                            if (!string.IsNullOrEmpty(mappingDetails.ConstantValue))
                            {
                                custFiled.Name = mappingDetails.ConstantValue;
                            }
                            else
                            {
                                custFiled.Name = getZohoInvoiceData(mappingDetails.ConnectionTransactionColumn.Name, CurrentInvoiceDetails);
                            }
                            break;
                        }
                    case "CustomFieldValue2":
                        {
                            invoice.CustomField[0].Type = CustomFieldTypeEnum.StringType;
                            if (!string.IsNullOrEmpty(mappingDetails.ConstantValue))
                            {
                                custFiled.AnyIntuitObject = mappingDetails.ConstantValue;
                            }
                            else
                            {
                                custFiled.AnyIntuitObject = getZohoInvoiceData(mappingDetails.ConnectionTransactionColumn.Name, CurrentInvoiceDetails);
                            }
                            custom_online[customcount - 1] = custFiled;

                            break;
                        }
                    case "CustomFieldName3":
                        {
                            custFiled = new CustomField();

                            custFiled.DefinitionId = customcount.ToString();
                            custFiled.Type = CustomFieldTypeEnum.StringType;
                            Array.Resize(ref custom_online, customcount);
                            customcount++;
                            if (!string.IsNullOrEmpty(mappingDetails.ConstantValue))
                            {
                                custFiled.Name = mappingDetails.ConstantValue;
                            }
                            else
                            {
                                custFiled.Name = getZohoInvoiceData(mappingDetails.ConnectionTransactionColumn.Name, CurrentInvoiceDetails);
                            }
                            break;
                        }
                    case "CustomFieldValue3":
                        {
                            invoice.CustomField[2].Type = CustomFieldTypeEnum.StringType;
                            if (!string.IsNullOrEmpty(mappingDetails.ConstantValue))
                            {
                                custFiled.AnyIntuitObject = mappingDetails.ConstantValue;
                            }
                            else
                            {
                                custFiled.AnyIntuitObject = getZohoInvoiceData(mappingDetails.ConnectionTransactionColumn.Name, CurrentInvoiceDetails);
                            }
                            custom_online[customcount - 1] = custFiled;

                            break;
                        }
                }
            }
            if (customField[0] != null)
            {
                invoice.CustomField = custom_online;
            }
            return invoice;
        }


        //private void TxnTaxDetailsFields(Invoice invoice, Mapping details, ZohoInvoiceDetail CurrentInvoiceDetails)
        //{
        //    invoice.TxnTaxDetail = new TxnTaxDetail();
        //    invoice.TxnTaxDetail.TxnTaxCodeRef=
        //    invoice.TxnTaxDetail.TxnTaxCodeRef = CreateReferenceType("");
        //    string Name = string.Empty;
        //    //invoice.TxnTaxDetail.TxnTaxCodeRef.
        //    switch (value)
        //    {
        //        case "TxnTaxCodeRefName":
        //            {

        //                if (!string.IsNullOrEmpty(mappingDetails.ConstantValue))
        //                {
        //                    invoice.TxnTaxDetail = mappingDetails.ConstantValue;
        //                }
        //                else
        //                {
        //                    invoice.TxnTaxDetail = getZohoInvoiceData(mappingDetails.ConnectionTransactionColumn.Name, CurrentInvoiceDetails);
        //                }
        //                break;
        //            }
        //        default:
        //            break;
        //    }
        //}




      

        private async Task<Invoice> CreateItemLineObjects(Invoice invoice, Mapping details, ZohoInvoice CurrentInvoiceDetails, ServiceContext serviceContext, string countryVersion)
        {
            SalesItemLineDetail lineSalesItemLineDetail = new SalesItemLineDetail();
           
            Line Line = new Line();
            string[] Array_taxcodeName = new string[1];
            decimal[] Array_taxAmount = new decimal[1];
            decimal[] Array_AmountWithTax = new decimal[1];
            int array_count = 0;
            bool taxFlag = true;
            int linecount1 = 0;
            int linecount = 1;

            Line[] lines_online = new Line[linecount];
            string Name = string.Empty;
            var filteredData = details.MappingDetails.Where(x => x.QuickbookTransactionColumn.Name.Contains("Line"));
            foreach (var itemData in CurrentInvoiceDetails.ZohoInvoiceItems)
            {
                lineSalesItemLineDetail = new SalesItemLineDetail();
                Line = new Line();
                foreach (var mappingDetails in filteredData)
                {
                    switch (mappingDetails.QuickbookTransactionColumn.Name)
                    {
                        #region Cases of Line details
                        case "LineDescription":
                            {
                                if (!string.IsNullOrEmpty(mappingDetails.ConstantValue))
                                {
                                    Line.Description = mappingDetails.ConstantValue;
                                }
                                else
                                {
                                    Line.Description = getZohoInvoiceLineData(mappingDetails.ConnectionTransactionColumn.Name, itemData);
                                }
                                break;
                            }
                        case "LineAmount":
                            {
                                decimal LineAmount = 0;
                                if (!string.IsNullOrEmpty(mappingDetails.ConstantValue))
                                {
                                    Name = mappingDetails.ConstantValue;
                                }
                                else
                                {
                                    Name = getZohoInvoiceLineData(mappingDetails.ConnectionTransactionColumn.Name, itemData);
                                }
                                if(Name!="")
                                {
                                    decimal.TryParse(Name,out LineAmount);
                                }
                                if (countryVersion == "US")
                                {
                                    Line.Amount = Convert.ToDecimal(LineAmount);
                                    Line.AmountSpecified = true;
                                }
                                else if (countryVersion != "US")
                                {
                                    #region Gross Net Amt

                                    Line.Amount = Math.Round(Convert.ToDecimal(LineAmount), 2, MidpointRounding.AwayFromZero);
                                    Line.AmountSpecified = true;
                                }
                                else
                                {
                                    Line.Amount = Convert.ToDecimal(LineAmount);
                                    Line.AmountSpecified = true;
                                }

                                #endregion

                                Line.AmountSpecified = true;
                                break;
                            }
                        case "LineSalesItemRefName":
                            {
                                Name = string.Empty;

                                if (!string.IsNullOrEmpty(mappingDetails.ConstantValue))
                                {
                                    Name = mappingDetails.ConstantValue;
                                }
                                else
                                {
                                    Name = getZohoInvoiceLineData(mappingDetails.ConnectionTransactionColumn.Name, itemData);
                                }
                                lineSalesItemLineDetail.ItemRef = await _quickbookItem.GetQuickbookItemDetails(serviceContext, Name, details.UserId);
                                break;
                            }
                        case "LineSalesItemClassRef":
                            {
                                Name = string.Empty;

                                if (!string.IsNullOrEmpty(mappingDetails.ConstantValue))
                                {
                                    Name = mappingDetails.ConstantValue;
                                }
                                else
                                {
                                    Name = getZohoInvoiceLineData(mappingDetails.ConnectionTransactionColumn.Name, itemData);
                                }
                                lineSalesItemLineDetail.ClassRef = _quickbookClassRef.GetQuickbookClassRefDetails(serviceContext, Name);
                                break;
                            }
                        case "LineSalesItemUnitPrice":
                            {
                                decimal LineAmount = 0;
                                if (!string.IsNullOrEmpty(mappingDetails.ConstantValue))
                                {
                                    Name = mappingDetails.ConstantValue;
                                }
                                else
                                {
                                    Name = getZohoInvoiceLineData(mappingDetails.ConnectionTransactionColumn.Name, itemData);
                                }
                                if (Name != "")
                                {
                                    decimal.TryParse(Name, out LineAmount);
                                }
                                if (LineAmount != 0)
                                {
                                    lineSalesItemLineDetail.AnyIntuitObject = decimal.Round(Convert.ToDecimal(getZohoInvoiceLineData(mappingDetails.ConnectionTransactionColumn.Name, itemData)), 7);

                                    lineSalesItemLineDetail.ItemElementName = ItemChoiceType.@UnitPrice;
                                }
                                break;
                            }
                        case "LineSalesItemQty":
                            {
                                if (!string.IsNullOrEmpty(mappingDetails.ConstantValue))
                                {
                                    lineSalesItemLineDetail.Qty = Convert.ToDecimal(mappingDetails.ConstantValue);
                                }
                                else
                                {
                                    lineSalesItemLineDetail.Qty = Convert.ToDecimal(getZohoInvoiceLineData(mappingDetails.ConnectionTransactionColumn.Name, itemData));
                                }
                                lineSalesItemLineDetail.QtySpecified = true;

                                break;
                            }
                        case "LineSalesItemTaxCodeRefValue":
                            {
                                Name = string.Empty;
                                if (!string.IsNullOrEmpty(mappingDetails.ConstantValue))
                                {
                                    Name = mappingDetails.ConstantValue;
                                }
                                else
                                {
                                    Name = getZohoInvoiceLineData(mappingDetails.ConnectionTransactionColumn.Name, itemData);
                                }
                                if (Name != null)
                                {
                                    string id = string.Empty;
                                    if (countryVersion == "US")
                                    {
                                        if (Name == "TAX")
                                        {
                                            id = "TAX";
                                        }
                                        else if (Name == "NON")
                                        {
                                            id = "NON";
                                        }
                                    }
                                    else if (countryVersion != "US")
                                    {
                                        if (invoice.GlobalTaxCalculation.ToString() == GlobalTaxCalculationEnum.TaxInclusive.ToString())
                                        {
                                            if (array_count == 0)
                                            {
                                                Array_taxcodeName[0] = Name;
                                                array_count++;
                                                Array_taxAmount[0] = Line.Amount;
                                                Array_AmountWithTax[0] = Convert.ToDecimal(Line.Amount);

                                            }
                                            else
                                            {
                                                int i = 0;
                                                for (i = 0; i < array_count; i++)
                                                {
                                                    if (Name == Array_taxcodeName[i])
                                                    {
                                                        taxFlag = false;
                                                        break;
                                                    }
                                                    else
                                                    {
                                                        taxFlag = true;

                                                    }

                                                }
                                                if (taxFlag == false)
                                                {
                                                    if (Name == Array_taxcodeName[i])
                                                    {
                                                        Array_taxAmount[i] += Line.Amount;
                                                        Array_AmountWithTax[i] += Convert.ToDecimal(Line.Amount);
                                                    }
                                                }
                                                else
                                                {
                                                    array_count = array_count + 1;
                                                    Array.Resize(ref Array_taxcodeName, array_count);
                                                    Array.Resize(ref Array_taxAmount, array_count);
                                                    Array.Resize(ref Array_AmountWithTax, array_count);
                                                    Array_taxcodeName[array_count - 1] = Name;
                                                    Array_taxAmount[array_count - 1] = Line.Amount;
                                                    Array_AmountWithTax[array_count - 1] = Convert.ToDecimal(Line.Amount);
                                                    taxFlag = false;
                                                }
                                            }
                                        }
                                        QueryService<TaxCode> TAxService = new QueryService<TaxCode>(serviceContext);
                                        if (Name.Contains("'"))
                                        {
                                            Name = Name.Replace("'", "\\'");
                                        }
                                        ReadOnlyCollection<TaxCode> dataTaxcode = new ReadOnlyCollection<TaxCode>(TAxService.ExecuteIdsQuery("select * from TaxCode where Name='" + Name.Trim() + "'"));
                                        foreach (var tax in dataTaxcode)
                                        {
                                            id = tax.Id;
                                        }
                                    }

                                    lineSalesItemLineDetail.TaxCodeRef = new ReferenceType()
                                    {
                                        name =Name,

                                        Value = id
                                    };
                                }
                                break;
                            }
                        case "LineSalesItemServiceDate":
                            {
                                if (!string.IsNullOrEmpty(mappingDetails.ConstantValue))
                                {
                                    lineSalesItemLineDetail.ServiceDate = Convert.ToDateTime(mappingDetails.ConstantValue);
                                }
                                else
                                {
                                    lineSalesItemLineDetail.ServiceDate = Convert.ToDateTime(getZohoInvoiceLineData(mappingDetails.ConnectionTransactionColumn.Name, itemData));
                                }
                                lineSalesItemLineDetail.ServiceDateSpecified = true;
                                break;
                            }
                        
                        default:
                            continue;
                            #endregion
                    }
                }
                if (linecount == 1)
                {
                    Line.AnyIntuitObject = lineSalesItemLineDetail;
                    Line.DetailType = LineDetailTypeEnum.SalesItemLineDetail;
                    Line.DetailTypeSpecified = true;
                    lines_online[linecount - 1] = Line;
                }
                else
                {
                    linecount++;
                    Line.AnyIntuitObject = lineSalesItemLineDetail;

                    Line.DetailType = LineDetailTypeEnum.SalesItemLineDetail;
                    Line.DetailTypeSpecified = true;
                    Array.Resize(ref lines_online, linecount);
                    lines_online[linecount - 1] = Line;
                }

            }
            invoice.Line = AddLine(lines_online, Line, linecount);
            invoice = DiscountLineObjects(invoice, details, CurrentInvoiceDetails, serviceContext, countryVersion, lines_online, linecount);

            return invoice;
        }

        private Invoice DiscountLineObjects(Invoice invoice, Mapping details, ZohoInvoice CurrentInvoiceDetails, ServiceContext serviceContext, string countryVersion, Line[] lines_online,int linecount)
        {
            Line Line = new Line();
            Intuit.Ipp.Data.DiscountLineDetail DiscountLineDetail = new Intuit.Ipp.Data.DiscountLineDetail();

            string Name = string.Empty;
            decimal amount = 0;
            int DiscountLineNum = 1;

            var filteredData = details.MappingDetails.Where(x => x.QuickbookTransactionColumn.Name.Contains("LineDiscount"));
            foreach (var itemData in CurrentInvoiceDetails.ZohoInvoiceItems)
            {
                Line = new Line();
                foreach (var mappingDetails in filteredData)
                {
                    switch (mappingDetails.QuickbookTransactionColumn.Name)
                    {

                        case "LineDiscountAmount":
                            {
                                if (!string.IsNullOrEmpty(mappingDetails.ConstantValue))
                                {
                                    amount =Convert.ToDecimal( mappingDetails.ConstantValue);
                                }
                                else
                                {
                                   amount = Convert.ToDecimal( getZohoInvoiceLineData(mappingDetails.ConnectionTransactionColumn.Name, itemData));
                                }
                                Line.Amount = amount;
                                Line.AmountSpecified = true;
                                Line.LineNum = DiscountLineNum.ToString();
                                break;
                            }
                        case "LineDiscountPercentBased":
                            {
                                if (!string.IsNullOrEmpty(mappingDetails.ConstantValue))
                                {
                                    DiscountLineDetail.PercentBased = Convert.ToBoolean(mappingDetails.ConstantValue);
                                }
                                else
                                {
                                    DiscountLineDetail.PercentBased = Convert.ToBoolean(getZohoInvoiceLineData(mappingDetails.ConnectionTransactionColumn.Name, itemData));
                                }
                                DiscountLineDetail.PercentBasedSpecified = true;

                                break;
                            }
                        case "LineDiscountPercent":
                            {
                                if (!string.IsNullOrEmpty(mappingDetails.ConstantValue))
                                {
                                    DiscountLineDetail.DiscountPercent = Convert.ToDecimal(mappingDetails.ConstantValue);
                                }
                                else
                                {
                                    DiscountLineDetail.DiscountPercent = Convert.ToDecimal(getZohoInvoiceLineData(mappingDetails.ConnectionTransactionColumn.Name, itemData));
                                }
                                break;
                            }
                        case "LineDiscountAccountRefName":
                            {
                                Name = string.Empty;
                                if (!string.IsNullOrEmpty(mappingDetails.ConstantValue))
                                {
                                    Name = mappingDetails.ConstantValue;
                                }
                                else
                                {
                                    Name = getZohoInvoiceLineData(mappingDetails.ConnectionTransactionColumn.Name, itemData);
                                }
                                DiscountLineDetail.DiscountAccountRef = _quickbookAccount.GetQuickbookAccountDetails(serviceContext, Name);
                                break;
                            }
                    }
                }
                if (Line.Amount != 0)
                {
                    linecount++;
                    Line.AnyIntuitObject = DiscountLineDetail;
                    Array.Resize(ref lines_online, linecount);
                    lines_online[linecount - 1] = Line;
                    DiscountLineNum++;
                    invoice.Line = AddLine(lines_online, Line, linecount);
                }
            }
            return invoice; 
        }

        

        //private void shippingRefTypeFileds(string value)
        //{
        //    switch (value)
        //    {
        //        case "Shipping":
        //            {
        //                if (!string.IsNullOrEmpty(mappingDetails.ConstantValue))
        //                {
        //                    invoice.shipping = mappingDetails.ConstantValue;
        //                }
        //                else
        //                {
        //                    invoice.DocNumber = getInvoiceData(mappingDetails.ConnectionTransactionColumn.Name, CurrentInvoiceDetails);
        //                }
        //                break;
        //            }
        //        case "ShippingTaxCode":
        //            {
        //                if (!string.IsNullOrEmpty(mappingDetails.ConstantValue))
        //                {
        //                    invoice.shi = mappingDetails.ConstantValue;
        //                }
        //                else
        //                {
        //                    invoice.DocNumber = getInvoiceData(mappingDetails.ConnectionTransactionColumn.Name, CurrentInvoiceDetails);
        //                }
        //                break;
        //            }
        //        default:
        //            break;
        //    }
        //}

       

        private Invoice CreateBillingAddress(Invoice invoice, Mapping details, ZohoInvoice CurrentInvoiceDetails)
        {
            PhysicalAddress BillAddr = new PhysicalAddress();
            var filteredData = details.MappingDetails.Where(x => x.QuickbookTransactionColumn.Name.Contains("BillAddr"));
            foreach (var mappingDetails in filteredData)
            {

                switch (mappingDetails.QuickbookTransactionColumn.Name)
                {

                    case "BillAddrLine1":
                        {
                            if (!string.IsNullOrEmpty(mappingDetails.ConstantValue))
                            {
                                BillAddr.Line1 = mappingDetails.ConstantValue;
                            }
                            else
                            {
                                BillAddr.Line1 = getZohoInvoiceData(mappingDetails.ConnectionTransactionColumn.Name, CurrentInvoiceDetails);
                            }
                            break;
                        }
                    case "BillAddrLine2":
                        {
                            if (!string.IsNullOrEmpty(mappingDetails.ConstantValue))
                            {
                                BillAddr.Line2 = mappingDetails.ConstantValue;
                            }
                            else
                            {
                                BillAddr.Line2 = getZohoInvoiceData(mappingDetails.ConnectionTransactionColumn.Name, CurrentInvoiceDetails);
                            }
                            break;
                        }

                    case "BillAddrLine3":
                        {
                            if (!string.IsNullOrEmpty(mappingDetails.ConstantValue))
                            {
                                BillAddr.Line3 = mappingDetails.ConstantValue;
                            }
                            else
                            {
                                BillAddr.Line3 = getZohoInvoiceData(mappingDetails.ConnectionTransactionColumn.Name, CurrentInvoiceDetails);
                            }
                            break;
                        }
                    case "BillAddrLine4":
                        {
                            if (!string.IsNullOrEmpty(mappingDetails.ConstantValue))
                            {
                                BillAddr.Line4 = mappingDetails.ConstantValue;
                            }
                            else
                            {
                                BillAddr.Line4 = getZohoInvoiceData(mappingDetails.ConnectionTransactionColumn.Name, CurrentInvoiceDetails);
                            }
                            break;
                        }
                    case "BillAddrLine5":
                        {
                            if (!string.IsNullOrEmpty(mappingDetails.ConstantValue))
                            {
                                BillAddr.Line5 = mappingDetails.ConstantValue;
                            }
                            else
                            {
                                BillAddr.Line5 = getZohoInvoiceData(mappingDetails.ConnectionTransactionColumn.Name, CurrentInvoiceDetails);
                            }
                            break;
                        }
                    case "BillAddrCity":
                        {
                            if (!string.IsNullOrEmpty(mappingDetails.ConstantValue))
                            {
                                BillAddr.City = mappingDetails.ConstantValue;
                            }
                            else
                            {
                                BillAddr.City = getZohoInvoiceData(mappingDetails.ConnectionTransactionColumn.Name, CurrentInvoiceDetails);
                            }
                            break;
                        }
                    case "BillAddrCountry":
                        {
                            if (!string.IsNullOrEmpty(mappingDetails.ConstantValue))
                            {
                                BillAddr.Country = mappingDetails.ConstantValue;
                            }
                            else
                            {
                                BillAddr.Country = getZohoInvoiceData(mappingDetails.ConnectionTransactionColumn.Name, CurrentInvoiceDetails);
                            }
                            break;
                        }
                    case "BillAddrSubDivisionCode":
                        {
                            if (!string.IsNullOrEmpty(mappingDetails.ConstantValue))
                            {
                                BillAddr.CountrySubDivisionCode = mappingDetails.ConstantValue;
                            }
                            else
                            {
                                BillAddr.CountrySubDivisionCode = getZohoInvoiceData(mappingDetails.ConnectionTransactionColumn.Name, CurrentInvoiceDetails);
                            }
                            break;
                        }

                    case "BillAddrLat":
                        {
                            if (!string.IsNullOrEmpty(mappingDetails.ConstantValue))
                            {
                                BillAddr.Lat = mappingDetails.ConstantValue;
                            }
                            else
                            {
                                BillAddr.Lat = getZohoInvoiceData(mappingDetails.ConnectionTransactionColumn.Name, CurrentInvoiceDetails);
                            }
                            break;
                        }
                    case "BillAddrLong":
                        {
                            if (!string.IsNullOrEmpty(mappingDetails.ConstantValue))
                            {
                                BillAddr.Long = mappingDetails.ConstantValue;
                            }
                            else
                            {
                                BillAddr.Long = getZohoInvoiceData(mappingDetails.ConnectionTransactionColumn.Name, CurrentInvoiceDetails);
                            }
                            break;
                        }
                    default:
                        continue;
                }
            }
            invoice.BillAddr = BillAddr;
            return invoice;
        }

        private Invoice CreateShippingAddresesFields(Invoice invoice, Mapping details, ZohoInvoice CurrentInvoiceDetails)
        {
            PhysicalAddress ShipAddr = new PhysicalAddress();
            var filteredData = details.MappingDetails.Where(x => x.QuickbookTransactionColumn.Name.Contains("ShipAddr"));
            foreach (var mappingDetails in filteredData)
            {

                switch (mappingDetails.QuickbookTransactionColumn.Name)
                {

                    case "ShipAddrLine1":
                        {
                            if (!string.IsNullOrEmpty(mappingDetails.ConstantValue))
                            {
                                ShipAddr.Line1 = mappingDetails.ConstantValue;
                            }
                            else
                            {
                                ShipAddr.Line1 = getZohoInvoiceData(mappingDetails.ConnectionTransactionColumn.Name, CurrentInvoiceDetails);
                            }
                            break;
                        }
                    case "ShipAddrLine2":
                        {
                            if (!string.IsNullOrEmpty(mappingDetails.ConstantValue))
                            {
                                ShipAddr.Line2 = mappingDetails.ConstantValue;
                            }
                            else
                            {
                                ShipAddr.Line2 = getZohoInvoiceData(mappingDetails.ConnectionTransactionColumn.Name, CurrentInvoiceDetails);
                            }
                            break;
                        }
                    case "ShipAddrLine3":
                        {
                            if (!string.IsNullOrEmpty(mappingDetails.ConstantValue))
                            {
                                ShipAddr.Line3 = mappingDetails.ConstantValue;
                            }
                            else
                            {
                                ShipAddr.Line3 = getZohoInvoiceData(mappingDetails.ConnectionTransactionColumn.Name, CurrentInvoiceDetails);
                            }
                            break;
                        }
                    case "ShipAddrCity":
                        {
                            if (!string.IsNullOrEmpty(mappingDetails.ConstantValue))
                            {
                                ShipAddr.City = mappingDetails.ConstantValue;
                            }
                            else
                            {
                                ShipAddr.City = getZohoInvoiceData(mappingDetails.ConnectionTransactionColumn.Name, CurrentInvoiceDetails);
                            }
                            break;
                        }
                    case "ShipAddrCountry":
                        {
                            if (!string.IsNullOrEmpty(mappingDetails.ConstantValue))
                            {
                                ShipAddr.Country = mappingDetails.ConstantValue;
                            }
                            else
                            {
                                ShipAddr.Country = getZohoInvoiceData(mappingDetails.ConnectionTransactionColumn.Name, CurrentInvoiceDetails);
                            }
                            break;
                        }
                    case "ShipAddrSubDivisionCode":
                        {
                            if (!string.IsNullOrEmpty(mappingDetails.ConstantValue))
                            {
                                ShipAddr.CountrySubDivisionCode = mappingDetails.ConstantValue;
                            }
                            else
                            {
                                ShipAddr.CountrySubDivisionCode = getZohoInvoiceData(mappingDetails.ConnectionTransactionColumn.Name, CurrentInvoiceDetails);
                            }
                            break;
                        }
                    case "ShipAddrPostalCode":
                        {
                            if (!string.IsNullOrEmpty(mappingDetails.ConstantValue))
                            {
                                ShipAddr.PostalCode = mappingDetails.ConstantValue;
                            }
                            else
                            {
                                ShipAddr.PostalCode = getZohoInvoiceData(mappingDetails.ConnectionTransactionColumn.Name, CurrentInvoiceDetails);
                            }
                            break;
                        }
                    case "ShipAddrNote":
                        {
                            if (!string.IsNullOrEmpty(mappingDetails.ConstantValue))
                            {
                                ShipAddr.Note = mappingDetails.ConstantValue;
                            }
                            else
                            {
                                ShipAddr.Note = getZohoInvoiceData(mappingDetails.ConnectionTransactionColumn.Name, CurrentInvoiceDetails);
                            }
                            break;
                        }
                    case "ShipAddrLine4":
                        {
                            if (!string.IsNullOrEmpty(mappingDetails.ConstantValue))
                            {
                                ShipAddr.Line4 = mappingDetails.ConstantValue;
                            }
                            else
                            {
                                ShipAddr.Line4 = getZohoInvoiceData(mappingDetails.ConnectionTransactionColumn.Name, CurrentInvoiceDetails);
                            }
                            break;
                        }
                    case "ShipAddrLine5":
                        {
                            if (!string.IsNullOrEmpty(mappingDetails.ConstantValue))
                            {
                                ShipAddr.Line5 = mappingDetails.ConstantValue;
                            }
                            else
                            {
                                ShipAddr.Line5 = getZohoInvoiceData(mappingDetails.ConnectionTransactionColumn.Name, CurrentInvoiceDetails);
                            }
                            break;
                        }
                    case "ShipAddrLat":
                        {
                            if (!string.IsNullOrEmpty(mappingDetails.ConstantValue))
                            {
                                ShipAddr.Lat = mappingDetails.ConstantValue;
                            }
                            else
                            {
                                ShipAddr.Lat = getZohoInvoiceData(mappingDetails.ConnectionTransactionColumn.Name, CurrentInvoiceDetails);
                            }
                            break;
                        }
                    case "ShipAddrLong":
                        {
                            if (!string.IsNullOrEmpty(mappingDetails.ConstantValue))
                            {
                                ShipAddr.Long = mappingDetails.ConstantValue;
                            }
                            else
                            {
                                ShipAddr.Long = getZohoInvoiceData(mappingDetails.ConnectionTransactionColumn.Name, CurrentInvoiceDetails);
                            }
                            break;
                        }
                    default:
                        continue;
                }
            }

            invoice.ShipAddr = ShipAddr;
            return invoice;
        }

      

        

       

       

        private EmailAddress CreateEmailAddress(string address)
        {
            var email = new EmailAddress();
            email.Address = address;
            return email;
        }

        private ReferenceType CreateReferenceType(string name, string value, string type)
        {
            var obj = new ReferenceType();
            obj.name = name;
            obj.type = type;
            obj.Value = value;
            return obj;
        }

        public static Line[] AddLine(Line[] lines, Line line, int linecount)
        {
            Array.Resize(ref lines, linecount);
            lines[linecount - 1] = line;
            return lines;
        }
        private string getZohoInvoiceData(string name, ZohoInvoice currentInvoiceDetails)
        {
            switch (name)
            {
                case "billing_address_city":
                    return currentInvoiceDetails.billing_address_city;
                case "billing_address_country":
                    return currentInvoiceDetails.billing_address_country;
                case "billing_address_state":
                    return currentInvoiceDetails.billing_address_state;
                case "billing_address_zip":
                    return currentInvoiceDetails.billing_address_zip;
                case "currency_code":
                    return currentInvoiceDetails.currency_code;
                case "customer_name":
                    return currentInvoiceDetails.customer_name;
                case "email":
                    return currentInvoiceDetails.email;
                case "invoice_date":
                    return currentInvoiceDetails.invoice_date.ToShortDateString();
                case "invoice_id":
                    return currentInvoiceDetails.invoice_id;
                case "shipping_address_city":
                    return currentInvoiceDetails.shipping_address_city;
                case "shipping_address_country":
                    return currentInvoiceDetails.shipping_address_country;
                case "shipping_address_state":
                    return currentInvoiceDetails.shipping_address_state;
                case "shipping_address_street":
                    return currentInvoiceDetails.shipping_address_street;
                case "shipping_address_zip":
                    return currentInvoiceDetails.shipping_address_zip;
                case "status":
                    return currentInvoiceDetails.status;
                case "total":
                    return currentInvoiceDetails.total.ToString();
                default:
                    return string.Empty;
            }
        }

        private string getZohoInvoiceLineData(string name, ZohoInvoiceItem currentInvoiceDetails)
        {
            switch (name)
            {
                case "invoice_items_item_id":
                    return currentInvoiceDetails.item_id;
                case "invoice_items_name":
                    return currentInvoiceDetails.name;
                case "invoice_items_description":
                    return currentInvoiceDetails.description;
                case "invoice_items_price":
                    return currentInvoiceDetails.price;
                case "invoice_items_quantity":
                    return currentInvoiceDetails.quantity;
                case "invoice_items_total":
                    return currentInvoiceDetails.item_total;
                case "invoice_items_discount_amount":
                    return currentInvoiceDetails.discount_amount;
                default:
                    return string.Empty;
            }
        }


    }
}
