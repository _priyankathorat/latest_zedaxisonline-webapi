﻿using System.Threading.Tasks;

namespace ZedAxisOnline.BusinessLogic
{
    public interface IAmazonManager
    {
        string GetAmazonSignInUrl();
        Task<bool> AmazonGetCode(string accessToken, int userId);
    }
}