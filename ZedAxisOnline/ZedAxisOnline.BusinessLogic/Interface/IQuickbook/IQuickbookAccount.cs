﻿using Intuit.Ipp.Core;
using Intuit.Ipp.Data;
using System;
using System.Collections.Generic;
using System.Text;

namespace ZedAxisOnline.BusinessLogic
{
   public interface IQuickbookAccount
    {
        ReferenceType GetQuickbookAccountDetails(ServiceContext serviceContext, string accountName);
        ReferenceType GetQuickbookExpenseAccountDetails(ServiceContext serviceContext, string Name);
    }
}
