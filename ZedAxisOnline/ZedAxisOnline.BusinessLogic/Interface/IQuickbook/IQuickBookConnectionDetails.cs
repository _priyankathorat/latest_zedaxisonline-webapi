﻿using System.Collections.Generic;
using System.Threading.Tasks;
using ZedAxisOnline.BusinessObjects.RequestModels;
using ZedAxisOnline.BusinessObjects.SignIn;
using ZedAxisOnline.BusinessObjects.QuickBook;

namespace ZedAxisOnline.BusinessLogic
{
    public interface IQuickbookConnectionDetails
    {
        Task<QuickBookEntity> GetQboLoginDetails(int userQboLoginId);
    }
}
