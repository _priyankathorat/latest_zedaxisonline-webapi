﻿using System.Collections.Generic;
using System.Threading.Tasks;
using ZedAxisOnline.BusinessObjects.RequestModels;
using ZedAxisOnline.BusinessObjects.SignIn;
using ZedAxisOnline.BusinessObjects.QuickBook;

namespace ZedAxisOnline.BusinessLogic
{
    public interface IQuickbookManager
    {
        string AuthorizationUrl(string UrlType);
        Task<SignInResponse> AccessTokensAsync(RegisterResource registerResource);
        Task<QuickBookEntity> QuickbookConnectionDetails(int userId);
        Task<QuickBookEntity> GetQuickbookCompanyDetails(int userId);
        Task<bool> AddUserQuickbookLogin(QuickbookCodeEntity codeEntity);
        Task<bool> ImportToQuickbook(int userId, int mappingId);

        Task<bool> UndoTransactionFromQuickbook(int userId, int importSummaryId);

        Task<IEnumerable<QuickbookImportSummaryResponse>> GetQboImportSummaryRecordsAsync(int userId, int mappingId);

        Task<QuickBookEntity> GetQboLoginDetails(int userQboLoginId);
        Task<bool> ValidateAndCreateQboConnectionAsync(QuickBookEntity quickBookEntity);

        Task<AccountSettingEntity> GetListForAccountSetting(int userId);

        Task<bool> SaveItemSetting(SaveSettingEntity saveSetting);

        Task<bool> DisconnectQboConnection(int userId);
    }
}
