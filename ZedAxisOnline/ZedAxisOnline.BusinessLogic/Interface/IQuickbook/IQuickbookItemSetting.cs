﻿using Intuit.Ipp.Core;
using Intuit.Ipp.Data;
using System.Collections.Generic;
using ZedAxisOnline.BusinessObjects.QuickBook;

namespace ZedAxisOnline.BusinessLogic
{
    public interface IQuickbookItemSetting
    {
        List<IdNameEntity> GetQuickbookTaxDetails(ServiceContext serviceContext);
        AccountSettingEntity GetQuickbookAccountDetailsForSetting(ServiceContext serviceContext);
        
    }
}
