﻿using Intuit.Ipp.Core;
using Intuit.Ipp.Data;
using System.Threading.Tasks;

namespace ZedAxisOnline.BusinessLogic
{
    public interface IQuickbookItem
    {
        Task<ReferenceType> GetQuickbookItemDetails(ServiceContext serviceContext, string customerName, int userId);
    }
}
