﻿using System.Threading.Tasks;
using ZedAxisOnline.BusinessObjects.QuickBook;

namespace ZedAxisOnline.BusinessLogic
{
    public interface IQuickbookImportSummary
    {
        Task<bool> AddQuickbookImportSummary(QuickbookImportSummaryEntity importSummary);
    }
}
