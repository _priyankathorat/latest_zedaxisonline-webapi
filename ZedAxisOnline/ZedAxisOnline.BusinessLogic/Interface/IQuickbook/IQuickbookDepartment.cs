﻿using Intuit.Ipp.Core;
using Intuit.Ipp.Data;
using System;
using System.Collections.Generic;
using System.Text;

namespace ZedAxisOnline.BusinessLogic
{
   public interface IQuickbookDepartment
    {
        ReferenceType GetDepartmentDetails(ServiceContext serviceContext, string departmentName);
    }
}
