using System.Collections.Generic;
using System.Threading.Tasks;

namespace ZedAxisOnline.BusinessLogic
{
    public interface IPaymentsEntry
    {
        Task<bool> AddPaymentsEntry(int userId ,int payPalResponseId, int mappingId);
         
    }
}