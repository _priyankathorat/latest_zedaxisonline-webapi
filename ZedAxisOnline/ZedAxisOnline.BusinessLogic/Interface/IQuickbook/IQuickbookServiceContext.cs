﻿using Intuit.Ipp.Core;
using System;
using System.Collections.Generic;
using System.Text;

namespace ZedAxisOnline.BusinessLogic
{
  public  interface IQuickbookServiceContext
    {
         ServiceContext GetServiceContext(string accessToken, string realmId);
    }
}
