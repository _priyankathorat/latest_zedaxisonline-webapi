﻿using Intuit.Ipp.Core;
using ZedAxisOnline.BusinessObjects.QuickBook;

namespace ZedAxisOnline.BusinessLogic
{
    public interface IQuickbookCompanyInfo
    {
        QuickbooksCompanyDetailsEntity GetQuickbookComapnyDetails(ServiceContext serviceContext);
    }
}
