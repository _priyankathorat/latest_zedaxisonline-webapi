﻿using Intuit.Ipp.Core;
using Intuit.Ipp.Data;

namespace ZedAxisOnline.BusinessLogic
{
    public interface IQuickbookTerm
    {
        ReferenceType GetQuickbookTermDetails(ServiceContext serviceContext, string termName);
    }
}
