﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using ZedAxisOnline.BusinessObjects.Connection;
using ZedAxisOnline.BusinessObjects.QuickBook;
using ZedAxisOnline.BusinessObjects.RequestModels;
using ZedAxisOnline.BusinessObjects.SignIn;
using ZedAxisOnline.BusinessObjects.User;
using ZedAxisOnline.Data.EF;
using ZedAxisOnline.Data.EF.Models;

namespace ZedAxisOnline.BusinessLogic
{
    public interface IFieldMappingManager
    {
        Task<List<DataForMapppingEntity>> GetDataForMappping(int qboTransactionTypeId);
        Task<bool> IsMappingNameValid(string MappingName, int UserId);
        Task<int> SaveMapping(SaveMappingEntity saveMappingEntity);
        Task<IList<QuickbookTransactionTypeEntity>> GetQuickbookTransactionTypes();
        Task<List<ConnectionTransactionType>> GetConnectionTransactionType(int conTypeId);
        Task<IList<ConnectionTransactionColumnEntity>> GetConnectionTransactionColumnAsync(int connectionTypeId);
    }
}
