﻿using System.Threading.Tasks;
using ZedAxisOnline.BusinessObjects.RequestModels;
using ZedAxisOnline.BusinessObjects.ResponseModel;

namespace ZedAxisOnline.BusinessLogic
{
    public interface IUserManager
    {
        Task<UserResponse> GetUserProfileAsync(int userId);
        Task<UserResponse> UpdateUserInformation(ProfileUpdateRequest model, int userId);
    }
}