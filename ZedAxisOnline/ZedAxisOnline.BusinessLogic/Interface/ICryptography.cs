namespace ZedAxisOnline.BusinessLogic
{
    public interface ICryptography
    {
        string Encrypt(string clearText);
        string Decrypt(string cipherText);
    }
}