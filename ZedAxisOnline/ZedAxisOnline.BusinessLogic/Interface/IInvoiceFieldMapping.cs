using System.Threading.Tasks;
using ZedAxisOnline.BusinessObjects.QuickBook;

namespace ZedAxisOnline.BusinessLogic
{
    public interface IInvoiceFieldMapping
    {
        Task<bool> MapData(int userId, int mappingId);

    }
}