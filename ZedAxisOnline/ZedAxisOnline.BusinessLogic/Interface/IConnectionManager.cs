﻿using System.Collections.Generic;
using System.Threading.Tasks;
using ZedAxisOnline.BusinessObjects.ResponseModel;

namespace ZedAxisOnline.BusinessLogic
{
    public interface IConnectionManager
    {
        Task<List<ConnectionResponse>> GetAllConnectionsAsync(int userId);
    }
}