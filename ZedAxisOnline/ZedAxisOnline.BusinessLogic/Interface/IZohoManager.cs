﻿using System.Collections.Generic;
using System.Threading.Tasks;
using ZedAxisOnline.BusinessObjects.Connection;
using ZedAxisOnline.BusinessObjects.RequestModels;
using ZedAxisOnline.BusinessObjects.ResponseModel;

namespace ZedAxisOnline.BusinessLogic
{
    public interface IZohoManager
    {
        Task<ZohoOrganizationResponse> OrgnizationDetailsAsync(string authToken, int userId);
        Task SaveConnectionAsync(ZohoSaveConnectionRequest saveConnection, int userId);
        Task SaveWebhookAsync(ZohoSaveWebhookRequest saveWebhookModel, int userId);

        Task GetInvoiceListFromZohoAPI(int connectionId, int mappingId);
        Task<IList<ZohoInvoiceEntity>> GetInvoiceListAsync(int mappingId);
    }
}