﻿using System.Threading.Tasks;
using ZedAxisOnline.BusinessObjects.QuickBook;
using ZedAxisOnline.BusinessObjects.RequestModels;
using ZedAxisOnline.BusinessObjects.SignIn;

namespace ZedAxisOnline.BusinessLogic
{
    public interface ISignInManager
    {
        Task<SignInResponse> LoginAsync(SignInRequest signInResource);
        Task<SignInResponse> Register(RegisterResource registerResource);
        Task<SignInResponse> IntuitSignIn(QuickbookCodeEntity codeEntity);
    }
}
