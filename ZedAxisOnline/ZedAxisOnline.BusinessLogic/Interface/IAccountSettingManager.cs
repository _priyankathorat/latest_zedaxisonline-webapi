﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using ZedAxisOnline.BusinessObjects.QuickBook;
using ZedAxisOnline.BusinessObjects.RequestModels;
using ZedAxisOnline.BusinessObjects.SignIn;
using ZedAxisOnline.BusinessObjects.User;
using ZedAxisOnline.Data.EF;

namespace ZedAxisOnline.BusinessLogic
{
    public interface IAccountSettingManager
    {
        Task<bool> GetAccountSettingData(int MappingFunctionId);
     

    }
}
