﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using ZedAxisOnline.BusinessObjects.QuickBook;
using ZedAxisOnline.BusinessObjects.RequestModels;
using ZedAxisOnline.BusinessObjects.SignIn;
using ZedAxisOnline.BusinessObjects.User;
using ZedAxisOnline.Data.EF;

namespace ZedAxisOnline.BusinessLogic
{
    public interface IFunctionManager
    {
        Task<List<FunctionDetailsEntity>> GetMappingFunctionData(int MappingFunctionId);
        Task<List<MappingFunctionListEntity>> GetMappingFunctionListForUser(int userId);
        Task<MappingFunctionDetailsEntity> AddFunction(MappingFunctionDetailsEntity MappingFunction);
        Task<int> UpdateFunction(MappingFunctionDetailsEntity MappingFunction);
        Task<bool> DeleteFunctionDetails(int id);

    }
}
