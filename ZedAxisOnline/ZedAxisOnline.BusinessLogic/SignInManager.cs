﻿using System;
using System.Collections.Generic;
using System.Text;
using ZedAxisOnline.Data.Abstraction;
using System.Security.Claims;
using System.Threading.Tasks;
using ZedAxisOnline.BusinessObjects.RequestModels;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using ZedAxisOnline.BusinessObjects.SignIn;
using ZedAxisOnline.BusinessObjects.User;
using ZedAxisOnline.Configuration;
using ZedAxisOnline.BusinessObjects.ResponseModel;
using ZedAxisOnline.Configuration.AppSettingConfiguration;
using Microsoft.Extensions.Options;
using ZedAxisOnline.BusinessObjects.QuickBook;
using Intuit.Ipp.OAuth2PlatformClient;
using System.Linq;
using Intuit.Ipp.Core;

namespace ZedAxisOnline.BusinessLogic
{
    public class SignInManager : ISignInManager
    {
        private readonly IQuickbookManager _quickbookManager;
        readonly IUserRepository _userRepository;
        private readonly IQuickbookServiceContext _quickbookServiceContext;
        private readonly IQuickbookCompanyInfo _quickbookcompanyInfo;
        private readonly AppSettings _appSettings;
        private readonly IConfiguration _config;
        private readonly ThirdPartyConfigurations _thirdPartyConfiguration;
        private readonly ICryptography _crypt;


        public SignInManager(IConfiguration config, IUserRepository userRepository,
            IOptions<AppSettings> appSettings, IOptions<ThirdPartyConfigurations> thirdPartyConfiguration,
            IQuickbookManager quickbookManager,
            IQuickbookCompanyInfo quickbookCompanyInfo,
            IQuickbookServiceContext quickbookServiceContext,
            ICryptography crypt
        )

        {
            _userRepository = userRepository;
            _quickbookManager = quickbookManager;
            _quickbookServiceContext = quickbookServiceContext;
            _crypt = crypt;
            _appSettings = appSettings.Value;
            _config = config;
            _thirdPartyConfiguration = thirdPartyConfiguration.Value;
            _quickbookcompanyInfo = quickbookCompanyInfo;

        }

        public async Task<SignInResponse> LoginAsync(SignInRequest signInResource)
        {

            var user = await _userRepository.GetUserAsync(signInResource.Email);
            if (user == null)
                return null;

            if (!VerifyPasswordHash(signInResource.Password, user.PasswordHash, user.PasswordSalt))
                return null;

            return CreateSignInResponse(user);
        }

        public async Task<SignInResponse> Register(RegisterResource registerResource)
        {
            var user = await _userRepository.GetUserAsync(registerResource.Email);
            if (user != null)
                throw new ArgumentException(message: MessagesConstant.UserExist);
            UserEntity UserToRegister = new UserEntity()
            {
                Email = registerResource.Email,
                FirstName = registerResource.FirstName,
                LastName = registerResource.LastName
            };
            byte[] passwordSalt, passwordHash;
            CreatePasswordHash(registerResource.Password, out passwordHash, out passwordSalt);
            UserToRegister.PasswordHash = passwordHash;
            UserToRegister.PasswordSalt = passwordSalt;

            var userRegistered = await _userRepository.CreateUserAsync(UserToRegister);
            if (userRegistered == null)
                return null;
            else
                return CreateSignInResponse(userRegistered);
        }

        public async Task<SignInResponse> IntuitSignIn(QuickbookCodeEntity codeEntity)
        {
            OAuth2Client auth2Client = new OAuth2Client(_thirdPartyConfiguration.QBO.ClientId, _thirdPartyConfiguration.QBO.ClientSecret,
                    _thirdPartyConfiguration.QBO.RedirectUrlIntuitSignIn, _thirdPartyConfiguration.QBO.Environment);
            var tokenResponse = await auth2Client.GetBearerTokenAsync(codeEntity.code);
            var isTokenValid = await auth2Client.ValidateIDTokenAsync(tokenResponse.IdentityToken);
            if (!isTokenValid)
                return null;

            var userInfoResp = await auth2Client.GetUserInfoAsync(tokenResponse.AccessToken);

            var QBOsub = userInfoResp.Claims.FirstOrDefault(a => a.Type == "sub").Value;
            var FirstName = userInfoResp.Claims.FirstOrDefault(a => a.Type == "givenName") == null ? "" : userInfoResp.Claims.FirstOrDefault(a => a.Type == "givenName").Value;
            var Familyname = userInfoResp.Claims.FirstOrDefault(a => a.Type == "familyName") == null ? "" : userInfoResp.Claims.FirstOrDefault(a => a.Type == "familyName").Value;
            var email = userInfoResp.Claims.FirstOrDefault(a => a.Type == "email").Value;

            ServiceContext serviceContext = _quickbookServiceContext.GetServiceContext(tokenResponse.AccessToken, codeEntity.realmId);
            QuickbooksCompanyDetailsEntity ComapnyDetails = _quickbookcompanyInfo.GetQuickbookComapnyDetails(serviceContext);
            QuickBookEntity quickBookEntity = new QuickBookEntity()
            {
                RealmId = _crypt.Encrypt(codeEntity.realmId),
                RefreshToken = _crypt.Encrypt(tokenResponse.RefreshToken),
                AccessToken = _crypt.Encrypt(tokenResponse.AccessToken),
                CountryVersion = ComapnyDetails.Country,
                CompanyName = ComapnyDetails.Name,
                IsCurrentConnection = true,
            };

            var user = await _userRepository.GetUserByQboSubAsync(QBOsub);
            if (user == null)
            {
                UserEntity UserToRegister = new UserEntity()
                {
                    Email = email,
                    FirstName = FirstName,
                    LastName = Familyname,
                    QuickbookSub = QBOsub
                };
                var userRegistered = await _userRepository.CreateUserAsync(UserToRegister);
                if (userRegistered == null)
                    return null;
                else
                {
                    quickBookEntity.UserId = userRegistered.Id;
                    await _quickbookManager.ValidateAndCreateQboConnectionAsync(quickBookEntity);
                    return CreateSignInResponse(userRegistered);
                }
            }
            else
            {
                quickBookEntity.UserId = user.Id;
                await _quickbookManager.ValidateAndCreateQboConnectionAsync(quickBookEntity);
                return CreateSignInResponse(user);
            }
        }

        #region Private Methods
        private void CreatePasswordHash(string password, out byte[] passwordHash, out byte[] passwordSalt)
        {
            using (var hmac = new System.Security.Cryptography.HMACSHA512())
            {
                passwordSalt = hmac.Key;
                passwordHash = hmac.ComputeHash(System.Text.Encoding.UTF8.GetBytes(password));
            }
        }

        private string JwtTokenHandler(UserEntity user)
        {
            List<Claim> claims = new List<Claim>();
            claims.Add(new Claim(ClaimTypes.NameIdentifier, user.Id.ToString()));
            claims.Add(new Claim(ClaimTypes.Role, user.Role));

            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_config.GetSection("AppSettings:Token").Value));
            var cred = new SigningCredentials(key, SecurityAlgorithms.HmacSha512Signature);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(claims),
                Expires = DateTime.Now.AddDays(1),
                SigningCredentials = cred
            };

            var tokenHandler = new JwtSecurityTokenHandler();
            var token = tokenHandler.CreateToken(tokenDescriptor);
            var tokenString = tokenHandler.WriteToken(token);

            return tokenString;
        }

        private bool VerifyPasswordHash(string password, byte[] passwordHash, byte[] passwordSalt)
        {
            using (var hmac = new System.Security.Cryptography.HMACSHA512(passwordSalt))
            {
                var Computehash = hmac.ComputeHash(System.Text.Encoding.UTF8.GetBytes(password));
                for (int i = 0; i < Computehash.Length; i++)
                {
                    if (Computehash[i] != passwordHash[i]) return false;
                }
            }
            return true;
        }

        private SignInResponse CreateSignInResponse(UserEntity user)
        {
            return new SignInResponse
            {
                Token = JwtTokenHandler(user),
                User = CerateUserResponse(user)
            };
        }

        private UserResponse CerateUserResponse(UserEntity user)
        {
            return new UserResponse()
            {
                Email = user.Email,
                FirstName = user.FirstName,
                LastName = user.LastName,
                PhotoUrl = user.Photo != null ? _appSettings.BaseUrl + "profilePhotos/" + user.Photo : user.Photo,
                Role = user.Role
            };
        }
        #endregion
    }
}
