﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ZedAxisOnline.BusinessObjects.Connection;
using ZedAxisOnline.BusinessObjects.ResponseModel;
using ZedAxisOnline.Data.Abstraction;

namespace ZedAxisOnline.BusinessLogic
{
    public class ConnectionManager : IConnectionManager
    {
        private readonly IAmazonConnectionRepository _amazonRepository;
        private readonly IZohoConnectionRepository _zohoRepository;

        public ConnectionManager(IAmazonConnectionRepository amazonRepository, IZohoConnectionRepository zohoRepository)
        {
            _amazonRepository = amazonRepository;
            _zohoRepository = zohoRepository;
        }

        public async Task<List<ConnectionResponse>> GetAllConnectionsAsync(int userId)
        {
            IList<AmazonConnectionEntity> amazonList = await _amazonRepository.GetAmazonConnections(userId);
            IList<ZohoConnectionEntity> zohoList = await _zohoRepository.GetZohoConnectionsAsync(userId);

            var ConnectionResponseList = amazonList.Select(x => new ConnectionResponse()
            {
                Id = x.Id,
                ConnectionName = x.AmazonUserId,
                ConnectionType = "Amazon",
                ConnectionTypeId = 1
            }).ToList();
            var ConnectionResponseZohoList = zohoList.Select(x => new ConnectionResponse()
            {
                Id = x.Id,
                ConnectionName = x.ConnectionName,
                ConnectionType = "Zoho",
                ConnectionTypeId = 4
            }).ToList();
           ConnectionResponseZohoList.AddRange(ConnectionResponseList);
            return ConnectionResponseZohoList;
        }
    }
}
