﻿using System;
using System.Threading.Tasks;
using ZedAxisOnline.BusinessLogic;
using ZedAxisOnline.BusinessObjects.QuickBook;
using ZedAxisOnline.Data.Abstraction;

namespace ZedAxisOnline.BusinessLogic.Quickbook
{
    public   class QuickbookImportSummary : IQuickbookImportSummary
    {

        private readonly IQuickbookImportSummaryRepository _quickbookImportSummaryRepository;

        public QuickbookImportSummary(IQuickbookImportSummaryRepository qboImportSummaryRepository)
        {
            _quickbookImportSummaryRepository = qboImportSummaryRepository;
        }
       public async Task<bool> AddQuickbookImportSummary(QuickbookImportSummaryEntity importSummary)
        {
            await _quickbookImportSummaryRepository.CreateQuickBookImportSummaryAsync(importSummary);

            return true;

        }



      
    }
}
