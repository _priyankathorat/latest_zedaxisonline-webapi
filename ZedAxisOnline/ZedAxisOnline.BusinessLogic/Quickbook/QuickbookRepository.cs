//using System.Linq;
//using System.Threading.Tasks;
//using Intuit.Ipp.OAuth2PlatformClient;
//using Microsoft.EntityFrameworkCore;
//using Microsoft.Extensions.Configuration;
//using ZedAxisOnline.Data.EF;

//namespace ZedAxisOnline.BusinessLogic.Quickbook
//{
//    public class QuickbookConfiguration : IQuickbookRepository
//    {
//        private readonly ApplicationDbContext _context;
//        public string clientId;
//        public string clientSecret;
//        public string qboRedirectUrl;
//        public string environment;
//        public OAuth2Client auth2Client;
//        private readonly ICryptography _crypto;

//        private readonly IConfiguration _config;

//        public QuickbookConfiguration(ApplicationDbContext context, IConfiguration config, ICryptography crypto)
//        {
//            this._config = config;
//            this._context = context;
//            _crypto = crypto;
//            clientId = _config.GetSection("QBO:ClientId").Value;
//            clientSecret = _config.GetSection("QBO:ClientSecret").Value;
//            qboRedirectUrl = _config.GetSection("QBO:RedirectUrlIntuitSignIn").Value;
//            environment = _config.GetSection("QBO:Environment").Value;
//        }

//        public async Task<string> RefreshAccessToken(int userQboLoginId)
//        {
//            string accessToken = string.Empty;
//            try
//            {
//                string refreshToken = string.Empty;
//                auth2Client = new OAuth2Client(clientId, clientSecret, qboRedirectUrl, environment);
//                var qboLoginDetails = await _context.UserQboLogins.Where(x => x.Id == userQboLoginId).FirstAsync();
//                refreshToken = _crypto.Decrypt(qboLoginDetails.RefreshToken);
//                var RefreshResponse = await auth2Client.RefreshTokenAsync(refreshToken);
//                accessToken = RefreshResponse.AccessToken;
//                qboLoginDetails.AccessToken = _crypto.Encrypt(RefreshResponse.AccessToken);
//                if (RefreshResponse.RefreshToken != null && RefreshResponse.RefreshToken != "")
//                {
//                    qboLoginDetails.RefreshToken = _crypto.Encrypt(RefreshResponse.RefreshToken);
//                }
//                await _context.SaveChangesAsync();
//                return accessToken;
//            }
//            catch (System.Exception ex)
//            {
//                var exx = ex.Message;
//            }
//            return accessToken;
//        }



//        public async Task<QboLoginDetailsDto> GetQboLoginDetails(int userQboLoginId)
//        {
//            QboLoginDetailsDto QboLoginDetails = new QboLoginDetailsDto();
//            auth2Client = new OAuth2Client(clientId, clientSecret, qboRedirectUrl, environment);
//            var qboLogin = await _context.UserQboLogins.Where(x => x.Id == userQboLoginId).FirstAsync();
//            QboLoginDetails.RefreshToken = _crypto.Decrypt(qboLogin.RefreshToken);
//            QboLoginDetails.RealmId = _crypto.Decrypt(qboLogin.RealmId);
//            var RefreshResponse = await auth2Client.RefreshTokenAsync(QboLoginDetails.RefreshToken);
//            QboLoginDetails.AccessToken = RefreshResponse.AccessToken;
//            QboLoginDetails.CountryVersion = qboLogin.CountryVersion;
//            qboLogin.AccessToken = _crypto.Encrypt(RefreshResponse.AccessToken);
//            await _context.SaveChangesAsync();
//            return QboLoginDetails;
//        }


//    }

//}