using System;
using System.Linq;
using System.Security.Cryptography;
using System.Threading.Tasks;
using Intuit.Ipp.Core;
using Intuit.Ipp.Data;
using Intuit.Ipp.DataService;
using Intuit.Ipp.Exception;
using Intuit.Ipp.OAuth2PlatformClient;
using Intuit.Ipp.Security;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using ZedAxisOnline.BusinessLogic;
using ZedAxisOnline.Data.EF;

namespace ZedAxisOnline.QuickbooksRepository
{
    // Comments by Sandeep
    // Refactor this method. Create seprate methods for each step.
    public class PaymentsEntry : IPaymentsEntry
    {
        private readonly ApplicationDbContext _context;
        private readonly IMappingRepository _mapping;
        private readonly IConfiguration _config;
        public string clientId;
        public string clientSecret;
        public string qboRedirectUrl;
        public string environment;
        public OAuth2Client auth2Client;
        private readonly IQuickbookRepository _quickbook;
        private readonly IQuickbookServiceContext _serviceContext;
        private readonly IQuickbookCustomer _customer;

        public PaymentsEntry(ApplicationDbContext context, IMappingRepository mapping, IConfiguration config, IQuickbookRepository quickbook,IQuickbookServiceContext qboServiceContext,IQuickbookCustomer customer)
        {
            this._quickbook = quickbook;
            this._serviceContext = qboServiceContext;
            this._customer = customer;
            this._mapping = mapping;
            this._config = config;
            this._context = context;
        }

        public async Task<bool> AddPaymentsEntry(int userId, int payPalResponseId, int mappingId)
        {

            var qboLoginDetails = await _context.UserQboLogins.Where(x => x.UserId == userId).FirstAsync();

             string accessToken = await _quickbook.RefreshAccessToken(qboLoginDetails.Id);
            var UserQboLogins = await _quickbook.GetQboLoginDetails(qboLoginDetails.Id);
            string realmId = UserQboLogins.RealmId;
            TimeSpan retryInterval = new TimeSpan(0, 0, 0, 1);
         // var e=    payments.AddPaymentsEntry(UserQboLogins.AccessToken, UserQboLogins.RealmId);

            OAuth2RequestValidator oauthRequestValidator = new OAuth2RequestValidator(UserQboLogins.AccessToken);
            oauthRequestValidator.Key = AsymmetricAlgorithm.Create("RSA");
            ServiceContext serviceContext = _serviceContext.GetServiceContext(UserQboLogins.AccessToken, realmId);
            DataService dataService = new DataService(serviceContext);

            Payment addPayment = new Payment();
            var payPalResponseDetails = await _context.PaypalResponsePayments.Where(x => x.Id == payPalResponseId).FirstAsync();

            var MappingDetails = await _mapping.GetMappingDetails(mappingId);
            try
            {
                addPayment.TxnStatus = "PAID";
                foreach (var item in MappingDetails)
                {
                    switch (item.QBOTransColumn)
                    {
                        case "TxnDate":
                            {
                                if (item.ConstantValue != "")
                                {
                                    DateTime date;
                                    var isValidDate = DateTime.TryParse(item.ConstantValue, out date);
                                    if (isValidDate)
                                        addPayment.TxnDate = date;
                                }
                                else
                                {
                                    if (item.ConnectionTransColumn == "create_time")
                                    {
                                        addPayment.TxnDate = payPalResponseDetails.CreateTime;
                                    }
                                    if (item.ConnectionTransColumn == "update_time")
                                    {
                                        addPayment.TxnDate = payPalResponseDetails.CreateTime;
                                    }
                                }
                                break;
                            }
                        case "TotalAmount":
                            {
                                if (item.ConstantValue != "")
                                {
                                    decimal TotalAmt;
                                    var isValidDate = decimal.TryParse(item.ConstantValue, out TotalAmt);
                                    if (isValidDate)
                                        addPayment.TotalAmt = TotalAmt;
                                }
                                else
                                {
                                    addPayment.TotalAmt = payPalResponseDetails.TotalAmount;
                                }
                                addPayment.TotalAmtSpecified = true;
                                break;
                            }
                        case "PrivateNote":
                            {
                                if (item.ConstantValue != null)
                                {
                                    addPayment.PrivateNote = item.ConstantValue;
                                }
                                else
                                {
                                    addPayment.PrivateNote = payPalResponseDetails.Summary;

                                }
                                break;
                            }
                        case "CurrencyRef":
                            {
                                if (item.ConstantValue != "")
                                {
                                    addPayment.CurrencyRef = new ReferenceType()
                                    {
                                        name = item.ConstantValue
                                    };
                                }
                                else
                                {

                                    addPayment.CurrencyRef = new ReferenceType()
                                    {
                                        name = payPalResponseDetails.Currency
                                    };
                                }
                                break;
                            }
                        case "CustomerRef":
                            {
                                string customerName = string.Empty;
                                if (item.ConstantValue != "")
                                {
                                    customerName = item.ConstantValue;
                                }
                                else
                                {
                                    customerName = payPalResponseDetails.CustomerName;
                                }
                                ReferenceType customerInfo = _customer.GetCustomerDetails(serviceContext, customerName);
                                addPayment.CustomerRef = customerInfo;


                                break;
                            }

                        default:
                            break;
                    }
                    // Payment PaymentAdded = new Payment();
                }
                var PaymentAdded =  dataService.Add<Payment>(addPayment);
            }
            catch (IdsException ex)
            {

                var PaymentAddedex = ex.Message;
            }
            catch (System.Exception ex)
            {

                var PaymentAddedex = ex.StackTrace;
            }
            return true;
        }


    }
}