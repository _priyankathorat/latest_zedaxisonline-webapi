﻿using System.Collections.Generic;
using System.Threading.Tasks;
using ZedAxisOnline.BusinessObjects.RequestModels;
using ZedAxisOnline.BusinessObjects.SignIn;
using ZedAxisOnline.BusinessObjects.QuickBook;
using Intuit.Ipp.OAuth2PlatformClient;
using ZedAxisOnline.Configuration.AppSettingConfiguration;
using Microsoft.Extensions.Options;
using ZedAxisOnline.Data.Abstraction;

namespace ZedAxisOnline.BusinessLogic
{
    public class QuickbookConnectionDetails: IQuickbookConnectionDetails
    {
        private readonly ThirdPartyConfigurations _thirdPartyConfiguration;
        private readonly ICryptography _crypto;
        private readonly IQuickbookRepository _quickbookRepository;

        public QuickbookConnectionDetails(IOptions<ThirdPartyConfigurations> thirdPartyConfiguration, 
            ICryptography crypto,
            IQuickbookRepository quickbookRepository)
        {
            _thirdPartyConfiguration = thirdPartyConfiguration.Value;
            this._crypto = crypto;
            _quickbookRepository = quickbookRepository;
        }


        public async Task<QuickBookEntity> GetQboLoginDetails(int userQboLoginId)
        {
            string clientId = _thirdPartyConfiguration.QBO.ClientId;
            string clientSecret = _thirdPartyConfiguration.QBO.ClientSecret;
            string qboRedirectUrl = _thirdPartyConfiguration.QBO.RedirectUrl;
            string environment = _thirdPartyConfiguration.QBO.Environment;
            OAuth2Client auth2Client = new OAuth2Client(clientId, clientSecret, qboRedirectUrl, environment);

            var qboLogin = await _quickbookRepository.GetUserQuickbookConnectionsById(userQboLoginId);
            var RefreshResponse = await auth2Client.RefreshTokenAsync(_crypto.Decrypt(qboLogin.RefreshToken));
            qboLogin.AccessToken = _crypto.Encrypt(RefreshResponse.AccessToken);
            if (RefreshResponse.RefreshToken != null && RefreshResponse.RefreshToken != "")
            {
                qboLogin.RefreshToken = _crypto.Encrypt(RefreshResponse.RefreshToken);
            }

            qboLogin.IsCurrentConnection = true;
            var updatedQboLogin = await _quickbookRepository.UpdateQuickbookConnections(qboLogin);
            updatedQboLogin.RefreshToken = _crypto.Decrypt(updatedQboLogin.RefreshToken);
            updatedQboLogin.AccessToken = _crypto.Decrypt(updatedQboLogin.AccessToken);
            updatedQboLogin.RealmId = _crypto.Decrypt(updatedQboLogin.RealmId);
            return updatedQboLogin;
        }
    }
}
