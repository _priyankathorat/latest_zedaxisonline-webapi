﻿using Intuit.Ipp.Core;
using Intuit.Ipp.Security;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using ZedAxisOnline.Configuration.AppSettingConfiguration;

namespace ZedAxisOnline.BusinessLogic.Quickbook
{
    public  class QuickbookServiceContext:IQuickbookServiceContext
    {
        private readonly ThirdPartyConfigurations _thirdPartyConfiguration;

        public QuickbookServiceContext(
            IOptions<ThirdPartyConfigurations> thirdPartyConfiguration
            
            )
        {
            this._thirdPartyConfiguration = thirdPartyConfiguration.Value;

        }
        public  ServiceContext GetServiceContext(string accessToken,string realmId)
        {
           
            OAuth2RequestValidator oauthRequestValidator = new OAuth2RequestValidator(accessToken);
            ServiceContext serviceContext = new ServiceContext(realmId, IntuitServicesType.QBO, oauthRequestValidator);
            serviceContext.IppConfiguration.MinorVersion.Qbo = _thirdPartyConfiguration.QBO.MinorVersion;
            serviceContext.IppConfiguration.BaseUrl.Qbo = _thirdPartyConfiguration.QBO.ServiceContextBaseUrl;

            serviceContext.IppConfiguration.Logger.RequestLog.EnableRequestResponseLogging = true;
            serviceContext.IppConfiguration.Logger.RequestLog.ServiceRequestLoggingLocation = _thirdPartyConfiguration.QBO.LogPath;
            return serviceContext;
          
        }
    }
}
