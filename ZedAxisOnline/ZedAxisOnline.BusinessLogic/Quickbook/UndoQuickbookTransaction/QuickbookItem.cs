﻿using Intuit.Ipp.Core;
using Intuit.Ipp.Data;
using Intuit.Ipp.DataService;
using Intuit.Ipp.QueryFilter;
using System;
using System.Collections.ObjectModel;

namespace ZedAxisOnline.BusinessLogic.Quickbook
{
    public class UndoQuickbookInvoice : IUndoQuickbookInvoice
    {
        
        public bool InvoiceDeleteRequest(ServiceContext serviceContext, string txID)
        {
            DataService dataService = new DataService(serviceContext);
            QueryService<Invoice> invoiceQueryService = new QueryService<Invoice>(serviceContext);

            ReadOnlyCollection<Invoice> dataInvoice    = new ReadOnlyCollection<Invoice>(invoiceQueryService.ExecuteIdsQuery("select * from Invoice where Id= '" + txID + "'"));
            Invoice Invoice = new Invoice();
            Line Line1 = new Line();
            Line[] invoiceLineArray = new Line[1];
            if (dataInvoice.Count > 0)
            {
                foreach (var invoice in dataInvoice)
                {
                    Array.Resize(ref invoiceLineArray, invoice.Line.Length);
                    Invoice.Id = invoice.Id;
                    Invoice.SyncToken = invoice.SyncToken;

                    if (invoice.Line != null)
                    {
                        int tempCount = 0;
                        foreach (var deleteLine in invoice.Line)
                        {
                            Line1.Id = deleteLine.Id;
                            if (Line1.Id != null)
                            {
                                invoiceLineArray[tempCount] = Line1;
                                break;
                            }
                        }
                    }
                    Invoice.Line = invoiceLineArray;
                }
                try
                {
                    Invoice invoiceAdded = dataService.Delete(Invoice);
                    if (invoiceAdded != null && invoiceAdded.status.ToString() == "Deleted")
                    {
                            return true;
                    }
                }
                #region error
                catch (Exception ex)
                {
                    return false;
                }
                #endregion
            }
            return false;
        }
    }
}
