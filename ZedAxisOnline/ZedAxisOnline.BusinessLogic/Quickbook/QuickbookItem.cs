﻿using Intuit.Ipp.Core;
using Intuit.Ipp.Data;
using Intuit.Ipp.DataService;
using Intuit.Ipp.QueryFilter;
using System;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using ZedAxisOnline.Configuration;
using ZedAxisOnline.Data.Abstraction;

namespace ZedAxisOnline.BusinessLogic.Quickbook
{
    public class QuickbookItem : IQuickbookItem
    {
        private readonly IQuickbookRepository _quickbookRepository;
        private readonly IQuickbookAccount _quickbookAccount;

        public QuickbookItem(IQuickbookRepository quickbookRepository,IQuickbookAccount quickbookAccount)
        {
            this._quickbookRepository = quickbookRepository;
            this._quickbookAccount = quickbookAccount;
        }
        public async Task<ReferenceType> GetQuickbookItemDetails(ServiceContext serviceContext, string name, int userId)
        {
            QueryService<Item> customerQueryService = new QueryService<Item>(serviceContext);
            ReadOnlyCollection<Item> dataCustomer;
            string value = string.Empty;

            string searchName = name;
            if (searchName.Contains("'"))
            {
                searchName = searchName.Replace("'", "\\'");
            }
            dataCustomer = new ReadOnlyCollection<Item>(customerQueryService.ExecuteIdsQuery("Select * From Item where FullyQualifiedName='" + searchName.Trim() + "'"));
            foreach (var customer1 in dataCustomer)
            {
                value = customer1.Id;
            }
            if (value != "")
            {
                return new ReferenceType()
                {
                    name = name,
                    Value = value
                };
            }
            else
            {
                // return null;
                return await CreateItemInQuickbook(serviceContext, name, userId);
            }
        }

        public async Task<ReferenceType> CreateItemInQuickbook(ServiceContext serviceContext, string Name, int userId)
        {

            try
            {
                DataService dataService = new DataService(serviceContext);
                Item newItem = new Item();
                var ItemSetting = await _quickbookRepository.GetItemSettings(userId);
                if (ItemSetting == null) return null;
                if (ItemSetting.Type == MessagesConstant.ItemService)
                {
                    newItem.Type = ItemTypeEnum.Service;
                    newItem.TypeSpecified = true;
                }
                else if (ItemSetting.Type == MessagesConstant.ItemNonInventory)
                {
                    newItem.Type = ItemTypeEnum.NonInventory;
                    newItem.TypeSpecified = true;
                }
                else if (ItemSetting.Type == MessagesConstant.ItemInventory)
                {
                    newItem.Type = ItemTypeEnum.Inventory;
                    newItem.TypeSpecified = true;
                }

                if (ItemSetting.IncomeAccount!=null)
                {
                    newItem.IncomeAccountRef = new ReferenceType()
                    {
                        name = ItemSetting.IncomeAccount,
                        Value = ItemSetting.IncomeAccountId
                    };
                }
                if (ItemSetting.COGSAccount != null)
                {
                    newItem.ExpenseAccountRef= _quickbookAccount.GetQuickbookExpenseAccountDetails(serviceContext, ItemSetting.COGSAccount);
                }
                if (ItemSetting.AssetAccount != null)
                {
                    newItem.AssetAccountRef = new ReferenceType()
                    {
                        name = ItemSetting.AssetAccount,
                        Value = ItemSetting.AssetAccountId
                    };
                }

                newItem.FullyQualifiedName = Name;
                newItem.Name = Name;
                newItem.Active = true;
                newItem.ActiveSpecified = true;
                newItem.Taxable = true;
                newItem.TaxableSpecified = true;
                newItem.UnitPrice = 0;
                newItem.UnitPriceSpecified = true;
                // newItem.IncomeAccountRef = _quickbookAccount.GetQuickbookAccountDetails(serviceContext, "Billable Expense Income");


                var addedItem = dataService.Add<Item>(newItem);

                ReferenceType itemInfo = new ReferenceType
                {
                    name = addedItem.FullyQualifiedName,
                    Value = addedItem.Id
                };
                return itemInfo;
            }
            catch (Exception ex)
            {
                var exx = ex.Message;

            }
            return null;
        }

    }
}
