﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using ZedAxisOnline.BusinessObjects.RequestModels;
using Microsoft.Extensions.Configuration;
using ZedAxisOnline.BusinessObjects.SignIn;
using ZedAxisOnline.BusinessObjects.QuickBook;
using Intuit.Ipp.OAuth2PlatformClient;
using Intuit.Ipp.Core;
using ZedAxisOnline.Configuration.AppSettingConfiguration;
using ZedAxisOnline.Data.Abstraction;
using Microsoft.Extensions.Options;
using ZedAxisOnline.Configuration;

namespace ZedAxisOnline.BusinessLogic.Quickbook
{
    public class QuickbookManager : IQuickbookManager
    {
        private readonly IQuickbookRepository _quickbookRepository;
        private readonly IQuickbookImportSummaryRepository _quickbookImportSummaryRepository;
        private readonly IQuickbookItemSetting _quickbookItemSetting;
        private readonly ThirdPartyConfigurations _thirdPartyConfiguration;
        private readonly IInvoiceFieldMapping _invoiceFieldMapping;
        private readonly IUndoQuickbookInvoice _undoQuickbookInvoice;
        private readonly ICryptography _crypto;
        private readonly IQuickbookCompanyInfo _quickbookcompanyInfo;
        private readonly IQuickbookServiceContext _serviceContext;
        private string clientId;
        private string clientSecret;
        private string qboRedirectUrl;
        private string environment;

        public QuickbookManager(IConfiguration config,
            IQuickbookCompanyInfo quickbookCompanyInfo,
            IQuickbookRepository quickbookRepository,
            IQuickbookServiceContext serviceContext,
            IOptions<ThirdPartyConfigurations> thirdPartyConfiguration,
            IInvoiceFieldMapping invoiceFieldMapping,
            IUndoQuickbookInvoice undoQuickbookInvoice,
            IQuickbookImportSummaryRepository quickbookImportSummaryRepository,
            IQuickbookItemSetting quickbookItemSetting,
            ICryptography cryptography
            )
        {
            _quickbookRepository = quickbookRepository;
            _serviceContext = serviceContext;
            _thirdPartyConfiguration = thirdPartyConfiguration.Value;
            _invoiceFieldMapping = invoiceFieldMapping;
            _undoQuickbookInvoice = undoQuickbookInvoice;
            _quickbookImportSummaryRepository = quickbookImportSummaryRepository;
            _quickbookItemSetting = quickbookItemSetting;
            _crypto = cryptography;
            _quickbookcompanyInfo = quickbookCompanyInfo;
        }


        /// <summary>
        /// get Authorization url by using clientId, ClientSecret, And return URL
        /// return URL will be different for SignUp Authorization and  dashboard (Connect to QuickBook )
        /// </summary>
        /// <param name="UrlType"></param>
        /// <returns></returns>
        public string AuthorizationUrl(string UrlType)
        {
            clientId = _thirdPartyConfiguration.QBO.ClientId;
            clientSecret = _thirdPartyConfiguration.QBO.ClientSecret;
            qboRedirectUrl = string.Empty;
            environment = _thirdPartyConfiguration.QBO.Environment;
            if (UrlType == "RedirectUrl")
            {
                qboRedirectUrl = _thirdPartyConfiguration.QBO.RedirectUrl;
            }
            else
            {
                qboRedirectUrl = _thirdPartyConfiguration.QBO.RedirectUrlIntuitSignIn;
            }
            var auth2Client = new OAuth2Client(clientId, clientSecret, qboRedirectUrl, environment);
            List<OidcScopes> scopes = new List<OidcScopes>();
            scopes.Add(OidcScopes.OpenId);
            scopes.Add(OidcScopes.Email);
            scopes.Add(OidcScopes.Profile);
            scopes.Add(OidcScopes.Accounting);
            string authorizeUrl = auth2Client.GetAuthorizationURL(scopes);
            return authorizeUrl;
        }

        public Task<SignInResponse> AccessTokensAsync(RegisterResource registerResource)
        {
            throw new NotImplementedException();
        }

        public async Task<bool> AddUserQuickbookLogin(QuickbookCodeEntity codeEntity)
        {
            clientId = _thirdPartyConfiguration.QBO.ClientId;
            clientSecret = _thirdPartyConfiguration.QBO.ClientSecret;
            qboRedirectUrl = _thirdPartyConfiguration.QBO.RedirectUrl;
            environment = _thirdPartyConfiguration.QBO.Environment;
            var auth2Client = new OAuth2Client(clientId, clientSecret, qboRedirectUrl, environment);
            var tokenResponse = await auth2Client.GetBearerTokenAsync(codeEntity.code);

            //retrieve access_token and refresh_token
            var AccessToken = _crypto.Encrypt(tokenResponse.AccessToken);
            var RefreshToken = _crypto.Encrypt(tokenResponse.RefreshToken);
            var idToken = tokenResponse.IdentityToken;
            string realmId = _crypto.Encrypt(codeEntity.realmId);

            var isTokenValid = await auth2Client.ValidateIDTokenAsync(idToken);
            if (!isTokenValid)
                return false;
            QuickBookEntity quickBookEntity = new QuickBookEntity()
            {
                UserId = codeEntity.userId,
                RealmId = realmId,
                RefreshToken = RefreshToken,
                AccessToken = AccessToken,
                IsCurrentConnection = true
            };
            await ValidateAndCreateQboConnectionAsync(quickBookEntity);

            return true;
        }

        public async Task<bool> ValidateAndCreateQboConnectionAsync(QuickBookEntity quickBookEntity)
        {
            OAuth2Client auth2Client = new OAuth2Client(
               _thirdPartyConfiguration.QBO.ClientId, _thirdPartyConfiguration.QBO.ClientSecret,
               _thirdPartyConfiguration.QBO.RedirectUrlIntuitSignIn, _thirdPartyConfiguration.QBO.Environment);
           
            var RefreshResponse = await auth2Client.RefreshTokenAsync(_crypto.Decrypt(quickBookEntity.RefreshToken));
            quickBookEntity.AccessToken = _crypto.Encrypt(RefreshResponse.AccessToken);
            if (RefreshResponse.RefreshToken != null && RefreshResponse.RefreshToken != "")
            {
                quickBookEntity.RefreshToken = _crypto.Encrypt(RefreshResponse.RefreshToken);
            }

            ServiceContext serviceContext = _serviceContext.GetServiceContext(_crypto.Decrypt(quickBookEntity.AccessToken), _crypto.Decrypt(quickBookEntity.RealmId));
            QuickbooksCompanyDetailsEntity ComapnyDetails = _quickbookcompanyInfo.GetQuickbookComapnyDetails(serviceContext);
            quickBookEntity.CountryVersion = ComapnyDetails.Country;
            quickBookEntity.CompanyName = ComapnyDetails.Name;
            await _quickbookRepository.ValidateAndCreateQboConnectionAsync(quickBookEntity);
            return true;
        }


        public async Task<QuickBookEntity> QuickbookConnectionDetails(int userId)
        {
            var QboLoginList = await _quickbookRepository.GetUserQuickbookConnections(userId);
            return QboLoginList;
        }

        public async Task<QuickBookEntity> GetQuickbookCompanyDetails(int userId)
        {
            var QboLoginList = await _quickbookRepository.GetUserQuickbookCompany(userId);
            return QboLoginList;
        }


        public async Task<bool> ImportToQuickbook(int userId, int mappingId)
        {
            var importStatus = await _invoiceFieldMapping.MapData(userId, mappingId);
            return importStatus;
        }
        
        public async Task<IEnumerable<QuickbookImportSummaryResponse>> GetQboImportSummaryRecordsAsync(int userId, int mappingId)
        {
            var QboLoginList = await _quickbookRepository.GetQboImportSummaryRecordsAsync(userId, mappingId);
            return QboLoginList;
        }


        public async Task<QuickBookEntity> GetQboLoginDetails(int userQboLoginId)
        {
            QboLoginDetailsEntity QboLoginDetails = new QboLoginDetailsEntity();
            OAuth2Client auth2Client = new OAuth2Client(
                _thirdPartyConfiguration.QBO.ClientId, _thirdPartyConfiguration.QBO.ClientSecret,
                _thirdPartyConfiguration.QBO.RedirectUrlIntuitSignIn, _thirdPartyConfiguration.QBO.Environment);

            var qboLogin = await _quickbookRepository.GetUserQuickbookConnectionsById(userQboLoginId);
            QboLoginDetails.RefreshToken = _crypto.Decrypt(qboLogin.RefreshToken);
            QboLoginDetails.RealmId = _crypto.Decrypt(qboLogin.RealmId);
            var RefreshResponse = await auth2Client.RefreshTokenAsync(QboLoginDetails.RefreshToken);
            QboLoginDetails.AccessToken = RefreshResponse.AccessToken;
            qboLogin.AccessToken = _crypto.Encrypt(RefreshResponse.AccessToken);
            if (RefreshResponse.RefreshToken != null && RefreshResponse.RefreshToken != "")
            {
                qboLogin.RefreshToken = _crypto.Encrypt(RefreshResponse.RefreshToken);
            }

            qboLogin.IsCurrentConnection = true;
            var updatedQboLogin = await _quickbookRepository.UpdateQuickbookConnections(qboLogin);

            return updatedQboLogin;
        }

        public async Task<bool> UndoTransactionFromQuickbook(int userId, int importSummaryId)
        {
            var qboLogin = await _quickbookRepository.GetUserQuickbookConnections(userId);
            qboLogin.RefreshToken = _crypto.Decrypt(qboLogin.RefreshToken);
            qboLogin.AccessToken = _crypto.Decrypt(qboLogin.AccessToken);
            qboLogin.RealmId = _crypto.Decrypt(qboLogin.RealmId);
            ServiceContext serviceContext = _serviceContext.GetServiceContext(qboLogin.AccessToken, qboLogin.RealmId);
            var quickbookImportSummary = await _quickbookImportSummaryRepository.GetQuickbookImportSummaryEntity(importSummaryId);
            switch (quickbookImportSummary.ImportType)
            {
                case "Invoice":
                    {
                        bool quickbookImportSummaryResponse = _undoQuickbookInvoice.InvoiceDeleteRequest(serviceContext, quickbookImportSummary.TransactionId);
                        return quickbookImportSummaryResponse;
                    }
                default:
                    break;
            }
            return false;

        }

        public async Task<AccountSettingEntity> GetListForAccountSetting(int userId)
        {
            var userQboConnections = await QuickbookConnectionDetails(userId);
            var connectionDetails = await GetQboLoginDetails(userQboConnections.Id);
            connectionDetails.AccessToken = _crypto.Decrypt(connectionDetails.AccessToken);
            connectionDetails.RealmId = _crypto.Decrypt(connectionDetails.RealmId);
            var qboServiceContext = _serviceContext.GetServiceContext(connectionDetails.AccessToken, connectionDetails.RealmId);
            var QuickbookAccountDetailsList = _quickbookItemSetting.GetQuickbookAccountDetailsForSetting(qboServiceContext);
            QuickbookAccountDetailsList.ItemType = QuickbookItemTypeListConstant.ItemTypeList;
            var ItemSetting = await _quickbookRepository.GetItemSettings(userId);
            if (ItemSetting != null)
            {
                QuickbookAccountDetailsList.ExistingSettings = ItemSetting;
            }
            return QuickbookAccountDetailsList;
        }

        public async Task<bool> SaveItemSetting(SaveSettingEntity saveSetting)
        {
            var updatedQboLogin = await _quickbookRepository.SaveItemSettings(saveSetting);
            return updatedQboLogin;
        }

        public async Task<bool> DisconnectQboConnection(int userId)
        {
            OAuth2Client auth2Client = new OAuth2Client(
                _thirdPartyConfiguration.QBO.ClientId, _thirdPartyConfiguration.QBO.ClientSecret,
                _thirdPartyConfiguration.QBO.RedirectUrlIntuitSignIn, _thirdPartyConfiguration.QBO.Environment);

            var userQboConnections = await QuickbookConnectionDetails(userId);
            var refreshToken = _crypto.Decrypt(userQboConnections.RefreshToken);
            var RefreshResponse = await auth2Client.RefreshTokenAsync(refreshToken);
            userQboConnections.AccessToken = _crypto.Encrypt(RefreshResponse.AccessToken);

            if (RefreshResponse.RefreshToken != null && RefreshResponse.RefreshToken != "")
            {
                userQboConnections.RefreshToken = _crypto.Encrypt(RefreshResponse.RefreshToken);
            }

            var tokenResp = await auth2Client.RevokeTokenAsync(refreshToken);
            if (tokenResp.HttpStatusCode.ToString() == "OK")
            {
                userQboConnections.Id = userQboConnections.Id;
                userQboConnections.UserId = userId;
                userQboConnections.IsActive = true;
                userQboConnections.IsCurrentConnection = false;
                var updatedQboLogin = await _quickbookRepository.UpdateQuickbookConnections(userQboConnections);
                return true;
            }
            
            return false;
        }
    }
}
