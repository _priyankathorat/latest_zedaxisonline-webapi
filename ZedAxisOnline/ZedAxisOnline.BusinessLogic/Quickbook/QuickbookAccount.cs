﻿using Intuit.Ipp.Core;
using Intuit.Ipp.Data;
using Intuit.Ipp.QueryFilter;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace ZedAxisOnline.BusinessLogic.Quickbook
{
    public   class QuickbookAccount : IQuickbookAccount
    {
        public  ReferenceType GetQuickbookAccountDetails(ServiceContext serviceContext,string Name)
        {
            ReadOnlyCollection<Account> dataAccount;

            dataAccount = null;

            List<Account> AccDetail = new List<Account>();
            QueryService<Account> AccountQueryService = new QueryService<Account>(serviceContext);
            ReferenceType AccDetails = new ReferenceType();
            if (Name != null && Name != string.Empty && Name != "")
            {
                Name = Name.Trim();
                
                    try
                    {
                        string[] Accno = new string[2];
                        if (Name.Contains(" "))
                        {
                           
                            if (Name.Contains("'"))
                            {
                                Name = Name.Replace("'", "\\'");
                            }
                        }
                        dataAccount = new ReadOnlyCollection<Account>(AccountQueryService.ExecuteIdsQuery("select * from Account where FullyQualifiedName='" + Name + "'"));
                        if (dataAccount == null)
                        {
                            AccDetails.name = Name;
                        }
                        foreach (var Account in dataAccount)
                        {
                            AccDetails.Value = Account.Id;
                            AccDetails.name = Account.FullyQualifiedName;
                        }


                    }
                    catch (Exception ex) { AccDetails.name = Name; }
                
            }
            return AccDetails;
        }


        public ReferenceType GetQuickbookExpenseAccountDetails(ServiceContext serviceContext, string Name)
        {
            ReferenceType AccDetails = new ReferenceType();

            QueryService<Account> expQueryService = new QueryService<Account>(serviceContext);
            ReadOnlyCollection<Account> dataExpAccount = new ReadOnlyCollection<Account>(expQueryService.ExecuteIdsQuery("Select * From Account where FullyQualifiedName ='" + Name + "'"));
            foreach (var accdata in dataExpAccount)
            {
                AccDetails = new ReferenceType()
                {
                    name = accdata.FullyQualifiedName,
                    Value = accdata.Id
                };
            }
            return AccDetails;
        }



    }
}
