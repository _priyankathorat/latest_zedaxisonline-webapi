﻿using Intuit.Ipp.Core;
using Intuit.Ipp.Data;
using Intuit.Ipp.QueryFilter;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using ZedAxisOnline.BusinessObjects.QuickBook;

namespace ZedAxisOnline.BusinessLogic.Quickbook
{
    public class QuickbookItemSetting : IQuickbookItemSetting
    {
        public AccountSettingEntity GetQuickbookAccountDetailsForSetting(ServiceContext serviceContext)
        {
            AccountSettingEntity accountSettingEntity = new AccountSettingEntity();
            IdNameEntity idNameDetails = new IdNameEntity();
            List<IdNameEntity> incomeAccountDetailsList = new List<IdNameEntity>();
            List<IdNameEntity> COGSAccountDetailsList = new List<IdNameEntity>();
            List<IdNameEntity> assetAccountDetailsList = new List<IdNameEntity>();
            QueryService<Account> accountQueryService = new QueryService<Account>(serviceContext);
            try
            {
                ReadOnlyCollection<Account> dataItem = new ReadOnlyCollection<Account>(accountQueryService.ExecuteIdsQuery("Select * from Account STARTPOSITION 1 MAXRESULTS 1000"));
            
            foreach (var account in dataItem)
            {
                if (!account.Active)
                    continue;
                if (account.AccountSubType == AccountSubTypeEnum.SalesOfProductIncome.ToString())
                {
                    idNameDetails.Name = account.FullyQualifiedName;
                    idNameDetails.Id = account.Id;
                    incomeAccountDetailsList.Add(idNameDetails);
                    idNameDetails = new IdNameEntity();
                }
                else if ( account.AccountType == AccountTypeEnum.CostofGoodsSold)
                {
                    idNameDetails.Name = account.FullyQualifiedName;
                    idNameDetails.Id = account.Id;
                    COGSAccountDetailsList.Add(idNameDetails);
                    idNameDetails = new IdNameEntity();
                }
                else if (account.AccountType == AccountTypeEnum.OtherCurrentAsset)
                {
                    idNameDetails.Name = account.FullyQualifiedName;
                    idNameDetails.Id = account.Id;
                    assetAccountDetailsList.Add(idNameDetails);
                    idNameDetails = new IdNameEntity();
                }
            }
            accountSettingEntity.AssetAccountEntities = assetAccountDetailsList;
            accountSettingEntity.COGSAccountEntities = COGSAccountDetailsList;
            accountSettingEntity.IncomeAccountEntities = incomeAccountDetailsList;
            accountSettingEntity.TaxEntities = GetQuickbookTaxDetails(serviceContext);
            return accountSettingEntity;
            }
            catch (Exception ex)
            {
                
            }
            return null;
        }


        public List<IdNameEntity> GetQuickbookTaxDetails(ServiceContext serviceContext)
        {
            QueryService<TaxCode> TaxCodeQueryService = new QueryService<TaxCode>(serviceContext);
            ReadOnlyCollection<TaxCode> dataTax;
            string value = string.Empty;
            IdNameEntity taxCode = new IdNameEntity();
            List<IdNameEntity> taxCodeList = new List<IdNameEntity>();


            dataTax = new ReadOnlyCollection<TaxCode>(TaxCodeQueryService.ExecuteIdsQuery("Select * From TaxCode"));
            foreach (var TaxCodeNode in dataTax)
            {
                if (TaxCodeNode.Active == true)
                {
                    taxCode.Name = TaxCodeNode.Name.ToString();
                    taxCode.Id = TaxCodeNode.Id.ToString();
                    taxCodeList.Add(taxCode);
                    taxCode = new IdNameEntity();
                }
            }
            return taxCodeList;
        }
    }
}
