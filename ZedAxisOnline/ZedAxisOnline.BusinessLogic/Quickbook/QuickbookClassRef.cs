﻿using Intuit.Ipp.Core;
using Intuit.Ipp.Data;
using Intuit.Ipp.QueryFilter;
using System.Collections.ObjectModel;

namespace ZedAxisOnline.BusinessLogic.Quickbook
{
    public   class QuickbookClassRef : IQuickbookClassRef
    {
        public  ReferenceType GetQuickbookClassRefDetails(ServiceContext serviceContext,string Name)
        {
            QueryService<Class> customerQueryService = new QueryService<Class>(serviceContext);
            ReadOnlyCollection<Class> dataClass;
            string value = string.Empty;
             

            if (Name.Contains("'"))
            {
                Name = Name.Replace("'", "\\'");
            }
            dataClass = new ReadOnlyCollection<Class>(customerQueryService.ExecuteIdsQuery("Select * From Class where FullyQualifiedName='" + Name.Trim() + "'"));
            foreach (var class1 in dataClass)
            {
                value = class1.Id;
            }
            if (value != null)
            {
                return new ReferenceType()
                {
                    name = Name,
                    Value = value
                };
            }
           else
                return null;
        }

      
    }
}
