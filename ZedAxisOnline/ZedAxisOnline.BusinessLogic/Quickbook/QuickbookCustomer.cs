﻿using Intuit.Ipp.Core;
using Intuit.Ipp.Data;
using Intuit.Ipp.DataService;
using Intuit.Ipp.QueryFilter;
using System;
using System.Collections.ObjectModel;

namespace ZedAxisOnline.BusinessLogic.Quickbook
{
    public   class QuickbookCustomer:IQuickbookCustomer
    {
        public  ReferenceType GetCustomerDetails(ServiceContext serviceContext,string customerName)
        {
            QueryService<Customer> customerQueryService = new QueryService<Customer>(serviceContext);
            ReadOnlyCollection<Customer> dataCustomer;
            string customervalue = string.Empty;

            string SearchName = customerName;
            if (SearchName.Contains("'"))
            {
                SearchName = SearchName.Replace("'", "\\'");
            }
            dataCustomer = new ReadOnlyCollection<Customer>(customerQueryService.ExecuteIdsQuery("Select * From Customer where FullyQualifiedName='" + SearchName.Trim() + "'"));
            foreach (var customer1 in dataCustomer)
            {
                customervalue = customer1.Id;
            }
            if (customervalue != "")
            {
                return new ReferenceType()
                {
                    name = customerName,
                    Value = customervalue
                };
            }
            else
            {
                ReferenceType newCustomer= CreateCustomerInQuickbook(serviceContext, customerName);

                return newCustomer;
            }
        }

        public ReferenceType  CreateCustomerInQuickbook(ServiceContext serviceContext, string customerName)
        {
            DataService dataService = new DataService(serviceContext);
            try
            {

           
            Customer customer = new Customer();
            customer.FullyQualifiedName = customerName;
            customer.GivenName = customerName;
            var CustomerAdded = dataService.Add<Customer>(customer);

            ReferenceType customerInfo = new ReferenceType
            {
                name = CustomerAdded.FullyQualifiedName,
                Value = CustomerAdded.Id
            };
            return customerInfo;
            }
            catch (Exception ex)
            {

                return null;
            }
        }

    }
}
