﻿using Intuit.Ipp.Core;
using Intuit.Ipp.Data;
using Intuit.Ipp.QueryFilter;
using System.Collections.ObjectModel;

namespace ZedAxisOnline.BusinessLogic.Quickbook
{
    public   class QuickbookDepartment:IQuickbookDepartment
    {
        public  ReferenceType GetDepartmentDetails(ServiceContext serviceContext,string departmentName)
        {
            ReadOnlyCollection<Intuit.Ipp.Data.Department> dataDepartment = null;
            QueryService<Department> ItemQueryService = new QueryService<Department>(serviceContext);
            string deptValue = string.Empty;

            if (departmentName.Contains("'"))
            {
                departmentName = departmentName.Replace("'", "\\'");
            }
            dataDepartment = new ReadOnlyCollection<Intuit.Ipp.Data.Department>(ItemQueryService.ExecuteIdsQuery("select * from Department where FullyQualifiedName='" + departmentName.Trim() + "'"));
            foreach (var item in dataDepartment)
            {
                deptValue = item.Id;
                break;
            }
                ReferenceType DepartmentRef = new ReferenceType()
                {
                    name = departmentName,
                    Value = deptValue
                };
            return DepartmentRef;
        }

      
    }
}
