﻿using Intuit.Ipp.Core;
using Intuit.Ipp.Data;
using Intuit.Ipp.QueryFilter;
using System.Collections.ObjectModel;

namespace ZedAxisOnline.BusinessLogic.Quickbook
{
    public   class QuickbookTerm : IQuickbookTerm
    {
        public  ReferenceType GetQuickbookTermDetails(ServiceContext serviceContext,string Name)
        {
            QueryService<Term> customerQueryService = new QueryService<Term>(serviceContext);
            ReadOnlyCollection<Term> dataTerm;
            string value = string.Empty;
             

            if (Name.Contains("'"))
            {
                Name = Name.Replace("'", "\\'");
            }
            dataTerm = new ReadOnlyCollection<Term>(customerQueryService.ExecuteIdsQuery("Select * From Term where Name ='" + Name.Trim() + "'"));
            foreach (var term in dataTerm)
            {
                value = term.Id;
            }
            if (value != null)
            {
                return new ReferenceType()
                {
                    name = Name,
                    Value = value
                };
            }
           else
                return null;
        }

     
    }
}
