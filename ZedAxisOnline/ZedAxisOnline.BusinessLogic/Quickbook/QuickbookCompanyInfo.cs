﻿using Intuit.Ipp.Core;
using Intuit.Ipp.Data;
using Intuit.Ipp.QueryFilter;
using System.Collections.ObjectModel;
using ZedAxisOnline.BusinessObjects.QuickBook;

namespace ZedAxisOnline.BusinessLogic.Quickbook
{
    public class QuickbookCompanyInfo : IQuickbookCompanyInfo
    {
        public QuickbooksCompanyDetailsEntity GetQuickbookComapnyDetails(ServiceContext serviceContext)
        {
            QuickbooksCompanyDetailsEntity quickbooksCompanyDetailsDto = new QuickbooksCompanyDetailsEntity();
            QueryService<CompanyInfo> ItemQueryService = new QueryService<CompanyInfo>(serviceContext);
            ReadOnlyCollection<CompanyInfo> dataItem = new ReadOnlyCollection<CompanyInfo>(ItemQueryService.ExecuteIdsQuery("select * from CompanyInfo"));
            foreach (var company in dataItem)
            {
                quickbooksCompanyDetailsDto.Name = company.CompanyName;
                quickbooksCompanyDetailsDto.Country = company.Country;
                break;
            }
            return quickbooksCompanyDetailsDto;
        }
    }
}
