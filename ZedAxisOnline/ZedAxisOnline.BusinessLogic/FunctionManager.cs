﻿using System.Collections.Generic;
using ZedAxisOnline.Data.Abstraction;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using ZedAxisOnline.BusinessObjects.QuickBook;

namespace ZedAxisOnline.BusinessLogic
{
    public class FunctionManager : IFunctionManager
    {
        private readonly IFunctionRepository _functionRepository;
        private readonly IConfiguration _config;

        public FunctionManager(IConfiguration config, IFunctionRepository functionRepository)
        {
            this._functionRepository = functionRepository;
            this._config = config;
        }

        public async Task<List<FunctionDetailsEntity>> GetMappingFunctionData(int MappingFunctionId)
        {
            return await _functionRepository.GetMappingFunctionData(MappingFunctionId);
        }

        public async Task<List<MappingFunctionListEntity>> GetMappingFunctionListForUser(int userId)
        {
            return await _functionRepository.GetMappingFunctionListForUser(userId);
        }

        public async Task<MappingFunctionDetailsEntity> AddFunction(MappingFunctionDetailsEntity MappingFunction)
        {

            MappingFunctionDetailsEntity entity = await _functionRepository.AddFunctionDetails(MappingFunction);
            return entity;
        }

        public async Task<int> UpdateFunction(MappingFunctionDetailsEntity MappingFunction)
        {
            int functionId = 0;

            if (MappingFunction.Id != 0)
            {
                functionId = await _functionRepository.UpdateFunctionDetails(MappingFunction);
            }
            else if (MappingFunction.FunctionId != 0)
            {
                functionId = await _functionRepository.AddNewFunctionDetailsToFunction(MappingFunction);
            }
            else
            {

            }
            return functionId;
        }

        public async Task<bool> DeleteFunctionDetails(int id)
        {
            bool isDeleted = await _functionRepository.DeleteFunctionDetails(id);
            return isDeleted;
        }

    }
}
