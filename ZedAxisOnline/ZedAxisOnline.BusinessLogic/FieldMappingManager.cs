﻿using System.Collections.Generic;
using ZedAxisOnline.Data.Abstraction;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using ZedAxisOnline.BusinessObjects.QuickBook;
using ZedAxisOnline.Data.EF.Models;
using ZedAxisOnline.BusinessObjects.Connection;

namespace ZedAxisOnline.BusinessLogic
{
    public class FieldMappingManager : IFieldMappingManager
    {
        IMappingRepository _mappingRepository;
        private readonly IQuickbookTransactionTypeRepository _quickbookTransactionTypeRepository;
        private readonly IConfiguration _config;

        public FieldMappingManager(IConfiguration config, IMappingRepository mappingRepository, IQuickbookTransactionTypeRepository quickbookTransactionTypeRepository)
        {
            _mappingRepository = mappingRepository;
            _quickbookTransactionTypeRepository = quickbookTransactionTypeRepository;
            _config = config;
        }

        public async Task<List<DataForMapppingEntity>> GetDataForMappping(int qboTransactionTypeId)
        {
            List<DataForMapppingEntity> dataForMapppingEntities = await _mappingRepository.GetDataForMappping(qboTransactionTypeId);
            return dataForMapppingEntities;
        }

        public async Task<bool> IsMappingNameValid(string mappingName, int userId)
        {

            return  await _mappingRepository.IsMappingNameValid(mappingName, userId);
        }

        public async Task<int> SaveMapping(SaveMappingEntity saveMappingEntity)
        {
            return await _mappingRepository.SaveMapping(saveMappingEntity);
        }


        public async Task<List<ConnectionTransactionType>> GetConnectionTransactionType(int conTypeId)
        {

            return await _mappingRepository.GetConnectionTransactionType(conTypeId);

           
        }
        public async Task<List<QuickbookTransactionColumn>> GetQuickbookTransactionColumn(int qboTransTypeId)
        {

         return await _mappingRepository.GetQuickbookTransactionColumn(qboTransTypeId);

        }

        public async Task<IList<QuickbookTransactionTypeEntity>> GetQuickbookTransactionTypes()
        {
            return await _quickbookTransactionTypeRepository.GetAllAsync();
        }

        public async Task<IList<ConnectionTransactionColumnEntity>> GetConnectionTransactionColumnAsync(int connectionTypeId)
        {
            return await _mappingRepository.GetConnectionTransactionColumnsAsync(connectionTypeId);
        }
    }
}
