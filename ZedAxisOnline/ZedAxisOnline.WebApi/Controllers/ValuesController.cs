﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using ZedAxisOnline.Configuration.AppSettingConfiguration;
using ZedAxisOnline.Data.EF;

namespace ZedAxisOnline.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ValuesController : ControllerBase
    {
        private readonly ThirdPartyConfigurations _thirdPartyConfiguration;
        private readonly IApplicationDbContextSeeder applicationDbContextSeeder;

        public ValuesController(IOptions<ThirdPartyConfigurations> thirdPartyConfiguration, IApplicationDbContextSeeder applicationDbContextSeeder)
        {
            this._thirdPartyConfiguration = thirdPartyConfiguration.Value;
            this.applicationDbContextSeeder = applicationDbContextSeeder;

        }
        // GET api/values
        [HttpGet]
        public ActionResult<IEnumerable<string>> Get()
        {
             // var AddZohoConnections = applicationDbContextSeeder.SeedZohoColumnDetails();

            return new string[] { "value1", "value2" };
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public ActionResult<string> Get(int id)
        {
            return "value";
        }

        // POST api/values
        [HttpPost]
        public void Post([FromBody] string value)
        {
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
