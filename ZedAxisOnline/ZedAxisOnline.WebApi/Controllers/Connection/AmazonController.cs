﻿using System;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ZedAxisOnline.BusinessLogic;
using ZedAxisOnline.BusinessObjects.RequestModels;
using ZedAxisOnline.Configuration;

namespace ZedAxisOnline.WebApi.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class AmazonController : ControllerBase
    {
        private readonly IAmazonManager _amazonManager;

        public AmazonController(IAmazonManager amazonManager)
        {
            _amazonManager = amazonManager;
        }

        /// <summary>
        /// get SignIn Url for user
        /// </summary>
        /// <returns></returns>
        [HttpGet("signInUrl")]
        public IActionResult SignInUrl()
        {
            string AuthorizationUrl = _amazonManager.GetAmazonSignInUrl();
            return Ok(new { AuthorizationUrl } );
        }

        /// <summary>
        /// Get access token based on code received from Amazon response
        /// </summary>
        /// <param name="amazonRequest"></param>
        /// <returns></returns>
        [HttpPost("accessToken")]
        public async Task<IActionResult> AccessTokensAsync(AmazonRequest amazonRequest)
        {
            int userId = Convert.ToInt32(User.FindFirst(ClaimTypes.NameIdentifier)?.Value);
            var result = await _amazonManager.AmazonGetCode(amazonRequest.Code, userId);
            if (result)
                return Ok(new { message = MessagesConstant.AmazonAccountCreatedMessage });
            else
                return Ok(null);
        }
    }
}