﻿using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ZedAxisOnline.BusinessLogic;
using ZedAxisOnline.BusinessObjects.QuickBook;

namespace ZedAxisOnline.WebApi.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class QuickBookController : ControllerBase
    {
        private readonly IQuickbookManager _quickBookManager;

        public QuickBookController(IQuickbookManager quickBookManager)
        {
            this._quickBookManager = quickBookManager;
        }

       
        /// <summary>
        /// get Code from quickbook and genrate accessToken
        /// </summary>
        /// <param name="code"></param>
        /// <returns></returns>
        [HttpPost("accessToken")]
        public async Task<IActionResult> AccessTokens(QuickbookCodeEntity codeEntity)
        {
            codeEntity.userId = Convert.ToInt32(User.FindFirst(ClaimTypes.NameIdentifier)?.Value);
            var QuickbookConnection = await _quickBookManager.AddUserQuickbookLogin(codeEntity);
            return Ok(QuickbookConnection);
        }



        /// <summary>
        ///  get user quickbook data  for user
        /// </summary>
        /// <returns></returns>
        [HttpGet("connection/companyname")]
        public async Task<IActionResult> Connection()
        {
            int userId = Convert.ToInt32(User.FindFirst(ClaimTypes.NameIdentifier)?.Value);
            var QuickbookConnection = await _quickBookManager.GetQuickbookCompanyDetails(userId);
            if (QuickbookConnection != null)
                return Ok(new { QuickbookConnection?.CompanyName });
            else
                return Ok(null);
        }

        /// <summary>
        /// UrlType will have value  "RedirectUrl" or "RedirectUrlIntuitSignIn"
        /// </summary>
        /// <param name="UrlType"></param>
        /// <returns></returns>
        [HttpGet("AuthorizationUrl")]
        public IActionResult AuthorizationUrl()
        {
            var AuthorizationUrl = _quickBookManager.AuthorizationUrl("RedirectUrl");
            return Ok(new { AuthorizationUrl });
        }

        /// <summary>
        /// Import to quickbook with by using mapping Id
        /// </summary>
        /// <param name="UrlType"></param>
        /// <returns></returns>
        [HttpGet("Import")]
        public async Task<IActionResult> Import(int mappingId)
        {
            int userId = Convert.ToInt32(User.FindFirst(ClaimTypes.NameIdentifier)?.Value);

             await _quickBookManager.ImportToQuickbook(userId,mappingId);
            IEnumerable<QuickbookImportSummaryResponse> importSummary = await _quickBookManager.GetQboImportSummaryRecordsAsync(userId, mappingId);
            return Ok(importSummary);
        }

        /// <summary>
        /// Undo (Remove) Imported transaction from quickbook by using transaction Id
        /// </summary>
        /// <param name="UrlType"></param>
        /// <returns></returns>
        [HttpGet("Undo")]
        public async Task<IActionResult> Undo(int importSummaryId)
        {
            int userId = Convert.ToInt32(User.FindFirst(ClaimTypes.NameIdentifier)?.Value);

            var data = await _quickBookManager.UndoTransactionFromQuickbook(userId, importSummaryId);
            return Ok(data);
        }

        /// <summary>
        /// get quickbook account setting list of all accounts, 
        /// Contains List of COGS,Asset,Income accounts and Taxcode
        /// </summary>
        /// <param name="UrlType"></param>
        /// <returns></returns>
        [HttpGet("ItemSetting")]
        public async Task<IActionResult> ItemSetting()
        {
            int userId = Convert.ToInt32(User.FindFirst(ClaimTypes.NameIdentifier)?.Value);
            var data = await _quickBookManager.GetListForAccountSetting(userId);
            return Ok(data);
        }

        /// <summary>
        /// Save quickbook account setting for user ,this will be used to create Items in Quickbook whenever Items are not avaialable in quickbook.
        /// </summary>
        /// <param name="UrlType"></param>
        /// <returns></returns>
        [HttpPost("SaveItemSetting")]
        public async Task<IActionResult> SaveItemSetting(SaveSettingEntity saveSetting)
        {
            saveSetting.UserId = Convert.ToInt32(User.FindFirst(ClaimTypes.NameIdentifier)?.Value);
            var data = await _quickBookManager.SaveItemSetting(saveSetting);
            return Ok(data);
        }

        [HttpGet("DisconnectFromQuickBook")]
        public async Task<IActionResult> DisconnectFromQuickBook()
        {
            int userId = Convert.ToInt32(User.FindFirst(ClaimTypes.NameIdentifier)?.Value);
            var data = await _quickBookManager.DisconnectQboConnection(userId);
            return Ok(data);
        }
    }
}