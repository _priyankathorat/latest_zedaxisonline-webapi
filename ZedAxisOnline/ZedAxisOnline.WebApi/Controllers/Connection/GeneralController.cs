﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ZedAxisOnline.BusinessLogic;
using ZedAxisOnline.BusinessObjects.ResponseModel;

namespace ZedAxisOnline.WebApi.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class GeneralController : ControllerBase
    {
        private readonly IConnectionManager _connectionManager;

        public GeneralController(IConnectionManager connectionManager)
        {
            _connectionManager = connectionManager;
        }

        /// <summary>
        /// Get all connections of users from which we will fetch data and create mapping (Zoho,Amazon,PayPal, etc...)
        /// </summary>
        /// <returns></returns>
        [HttpGet("Connections")]
        public async Task<IActionResult> UsersConnection()
        {
            int userId = Convert.ToInt32(User.FindFirst(ClaimTypes.NameIdentifier)?.Value);
            List<ConnectionResponse> list = await _connectionManager.GetAllConnectionsAsync(userId);
            return Ok(list);
        }
    }
}