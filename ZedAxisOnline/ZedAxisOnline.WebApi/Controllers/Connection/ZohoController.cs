﻿using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ZedAxisOnline.BusinessLogic;
using ZedAxisOnline.BusinessObjects.Connection;
using ZedAxisOnline.BusinessObjects.RequestModels;
using ZedAxisOnline.BusinessObjects.ResponseModel;

namespace ZedAxisOnline.WebApi.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class ZohoController : ControllerBase
    {
        private readonly IZohoManager _zohoManager;

        public ZohoController(IZohoManager zohoManager )
        {
            _zohoManager = zohoManager;
        }
        /// <summary>
        /// Get Organization Details using AuthToken entered by user 
        /// return organization details list if authToken is valid else returns null
        /// </summary>
        /// <param name="authToken"></param>
        /// <returns></returns>
        [HttpGet("OrganizationDetails")]
        public async Task<IActionResult> OrganizationDetails([FromQuery]string authToken)
        {
            int userId = Convert.ToInt32(User.FindFirst(ClaimTypes.NameIdentifier)?.Value);
            ZohoOrganizationResponse data = await _zohoManager.OrgnizationDetailsAsync(authToken, userId);
            return Ok(data);
        }

        /// <summary>
        ///  Save zoho connection with selected organisations and connection name
        /// </summary>
        /// <param name="SaveConnection"></param>
        /// <returns></returns>
        [HttpPost("Connection")]
        public async Task<IActionResult> SaveConnection(ZohoSaveConnectionRequest SaveConnection)
        {
            int userId = Convert.ToInt32(User.FindFirst(ClaimTypes.NameIdentifier)?.Value);
            await _zohoManager.SaveConnectionAsync(SaveConnection, userId);
            return Ok(new { message = "Connection Saved."});
        }

        /// <summary>
        /// Save webhook for zoho connection 
        /// webhook will be used to get data of zoho whenever the reords are added to zoho
        /// </summary>
        /// <param name="saveWebhookModel"></param>
        /// <returns></returns>
        [HttpPost("Webhook")]
        public async Task<IActionResult> SaveWebhook(ZohoSaveWebhookRequest saveWebhookModel)
        {
            int userId = Convert.ToInt32(User.FindFirst(ClaimTypes.NameIdentifier)?.Value);
            await _zohoManager.SaveWebhookAsync(saveWebhookModel, userId);
            return Ok(new{message = "Webhook saved succesfully"});
        }

        /// <summary>
        /// used to save this webhook URL into Zoho setting page and Application  will use this Webhook Url for getting data from Zoho.
        /// </summary>
        /// <returns></returns>
        [HttpGet("webhookguid")]
        public IActionResult CreateGuidForWebhook()
        {
            return Ok(new { guid = Guid.NewGuid().ToString("N") });
        }

        /// <summary>
        /// Fetch Save And Return Invoice list 
        /// </summary>
        /// <param name="connectionId"></param>
        /// <param name="mappingId"></param>
        /// <returns></returns>
        [HttpGet("InvoiceList")]
        public async Task<IActionResult> FetchSaveAndReturnInvoice(int connectionId, int mappingId)
        {
            try
            {
                await _zohoManager.GetInvoiceListFromZohoAPI(connectionId, mappingId);
                IList<ZohoInvoiceEntity> InvoiceList = await _zohoManager.GetInvoiceListAsync(mappingId);
                return Ok(InvoiceList);
            }
            catch (Exception e)
            {
                return BadRequest("Exception");
            }
        }

    }
}