﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;

namespace ZedAxisOnline.WebApi.Controllers
{
    public class FallbackController : Controller
    {
        private readonly IHostingEnvironment _hostingEnvironment;

        public FallbackController(IHostingEnvironment hostingEnvironment)
        {
            _hostingEnvironment = hostingEnvironment;
        }
        public IActionResult Index()
        {
            return PhysicalFile(Path.Combine(_hostingEnvironment.WebRootPath, "index.html"), "text/HTML");
        }
    }
}