﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;
using ZedAxisOnline.BusinessLogic;
using ZedAxisOnline.BusinessObjects.QuickBook;
using ZedAxisOnline.BusinessObjects.RequestModels;
using ZedAxisOnline.BusinessObjects.SignIn;
using ZedAxisOnline.Configuration;

namespace ZedAxisOnline.WebApi.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    [AllowAnonymous]
    public class AuthController : ControllerBase
    {
        #region Constructor and data members
        private readonly ISignInManager _signInManager;
        private readonly IQuickbookManager _quickBookManager;

        public AuthController(ISignInManager signInManager, IQuickbookManager quickBookManager) 
        {
            this._signInManager = signInManager;
            this._quickBookManager = quickBookManager;
        }

        #endregion

        #region SignIn
        /// <summary>
        /// Sign in Controller
        /// </summary>
        /// <param name="signInResource"></param>
        /// <returns></returns>
        [HttpPost("SignIn")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<IActionResult> SignInAsync(SignInRequest signInResource)
        {
            var signinResponse = await _signInManager.LoginAsync(signInResource);
            if (signinResponse == null)
                return Ok(null);
            return Ok(signinResponse);
        }
        #endregion

        #region SignUp
        /// <summary>
        /// Sign Up 
        /// </summary>
        /// <param name="register"></param>
        /// <returns></returns>
        [HttpPost("SignUp")]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> SignUp(RegisterResource register)
        {
            try
            {
                SignInResponse response = await _signInManager.Register(register);
                if (response == null)
                {
                    return BadRequest(new { Message = MessagesConstant.CreationFailedMsg });
                }

                return Created(string.Empty, response);
            }
            catch (Exception ex)
            {
                if (ex.Message == MessagesConstant.UserExist)
                {
                    return BadRequest(MessagesConstant.UserAlreadyExistMsg);
                }
                return StatusCode(500, MessagesConstant.SomethingWentWrongMsg);
            }
        }
        #endregion

        #region Quickbook SignInUrl
        /// <summary>
        /// UrlType will have value  "RedirectUrl" or "RedirectUrlIntuitSignIn"
        /// </summary>
        /// <param name="UrlType"></param>
        /// <returns></returns>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [HttpGet("Intuit/SignInUrl")]
        public IActionResult IntuitSignInUrl()
        {
            var AuthorizationUrl = _quickBookManager.AuthorizationUrl("RedirectUrlIntuitSignIn");
            return Ok(new { AuthorizationUrl });
        }
        #endregion
       

        [HttpPost("Intuit/SignIn")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> IntuitSignIn(QuickbookCodeEntity codeEntity)
        {
            var signinResponse = await _signInManager.IntuitSignIn(codeEntity);
            if (signinResponse == null)
                return Ok(null);
            return Ok(signinResponse);
        }

    }
}