﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ZedAxisOnline.BusinessLogic;
using ZedAxisOnline.BusinessObjects.QuickBook;

namespace ZedAxisOnline.WebApi.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class MappingController : ControllerBase
    {
        private readonly IFieldMappingManager _fieldMappingManager;

        public MappingController(IFieldMappingManager fieldMappingManager)
        {
            this._fieldMappingManager = fieldMappingManager;
        }





        /// <summary>
        /// Check where mapping name exists for user and related quickbook transaction type
        /// </summary>
        /// <param name="qboTransationId"></param>
        /// <returns></returns>
        [HttpGet("ValidateMappingName")]
        public async Task<ActionResult> ValidateMappingName(string mappingName)
        {
            int userId = Convert.ToInt32(User.FindFirst(ClaimTypes.NameIdentifier)?.Value);
            var IsValid = await _fieldMappingManager.IsMappingNameValid(mappingName, userId);
            return Ok(IsValid);
        }

        /// <summary>
        /// Get mapping related columns list and other data  for displaying on grid
        /// </summary>
        /// <param name="qboTransationId"></param>
        /// <returns></returns>
        [HttpGet("CreateMapping")]
        public async Task<ActionResult> CreateMapping(int qboTransationId)
        {
            var MappingData = await _fieldMappingManager.GetDataForMappping(qboTransationId);
            return Ok(MappingData);
        }

        /// <summary>
        /// Save mapping created by user if not exists with same name otherwise update the mapping.
        /// </summary>
        /// <param name="conTypeIdDto"></param>
        /// <returns></returns>
        [HttpPost("SaveMappingData")]
        public async Task<IActionResult> SaveMapping(SaveMappingEntity model)
        {
            model.UserId = Convert.ToInt32(User.FindFirst(ClaimTypes.NameIdentifier)?.Value);
            int MappingId = await _fieldMappingManager.SaveMapping(model);
            return Ok(new { MappingId });
        }


        [HttpGet("Connection/{id}/TransactionTypes")]
        public async Task<ActionResult> TransactionTypesAsync(int id)
        {
            var list = await _fieldMappingManager.GetConnectionTransactionType(id);
            return Ok(list);
        }

        [HttpGet("Quickbook/TransactionTypes")]
        public async Task<ActionResult> TransactionTypesAsync()
        {
            var list = await _fieldMappingManager.GetQuickbookTransactionTypes();
            return Ok(list);
        }

        [HttpGet("connection/{id}/TransactionColumns")]
        public async Task<ActionResult> TransactionColumnAsync(int id)
        {
            var list = await _fieldMappingManager.GetConnectionTransactionColumnAsync(id);
            return Ok(list);
        }
    }
}