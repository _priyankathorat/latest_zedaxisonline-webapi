﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ZedAxisOnline.BusinessLogic;
using ZedAxisOnline.BusinessObjects.QuickBook;

namespace ZedAxisOnline.WebApi.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class FunctionController : ControllerBase
    {
       

        private readonly IFunctionManager _functionManager;

        public FunctionController(IFunctionManager functionManager)
        {
            this._functionManager = functionManager;
        }

        // GET api/Mapping/function/all
        [HttpGet("all")]
        public async Task<IActionResult> Get()
        {
            int userId = Convert.ToInt32(User.FindFirst(ClaimTypes.NameIdentifier)?.Value);
             
            var mappingFunctionList = await _functionManager.GetMappingFunctionListForUser(userId);
            return Ok(mappingFunctionList);
        }

        // GET api/Mapping/function/5
        [HttpGet("get/{id}")]
        public async Task<ActionResult>  Get(int Id)
        {
            var mappingFunctionDetails = await _functionManager.GetMappingFunctionData(Id);
            return Ok(mappingFunctionDetails);
        }

        // POST api/Mapping/function
        [HttpPost("save")]
        public async Task<ActionResult> Post([FromBody] MappingFunctionDetailsEntity mappingFunctionDetailsEntity)
        {
            mappingFunctionDetailsEntity.UserId = Convert.ToInt32(User.FindFirst(ClaimTypes.NameIdentifier)?.Value);
            var savedEntity = await _functionManager.AddFunction(mappingFunctionDetailsEntity);
            List<MappingFunctionDetailsEntity> list = new List<MappingFunctionDetailsEntity>();
            list.Add(savedEntity);
            return Ok(list);
        }

        // PUT api/Mapping/function/5
        [HttpPut("update")]
        public async Task<ActionResult> Put([FromBody] MappingFunctionDetailsEntity mappingFunctionDetailsEntity)
        {
            mappingFunctionDetailsEntity.UserId = Convert.ToInt32(User.FindFirst(ClaimTypes.NameIdentifier)?.Value);
            var savedEntityId = await _functionManager.UpdateFunction(mappingFunctionDetailsEntity);
            return Ok(new { id = savedEntityId});
        }

        // DELETE api/Mapping/function/5
        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(int id)
        {
            var mappingFunctionDetails = await _functionManager.DeleteFunctionDetails(id);
            return Ok(mappingFunctionDetails);
        }
    }
}