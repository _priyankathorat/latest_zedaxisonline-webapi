﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ZedAxisOnline.BusinessLogic;
using ZedAxisOnline.BusinessObjects.RequestModels;
using ZedAxisOnline.Data.Abstraction;

namespace ZedAxisOnline.WebApi.Controllers
{
    [Authorize]
    [Route("api/v1/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly IUserManager _userManager;

        public UserController(IUserManager userManager)
        {
            _userManager = userManager;
        }
        [HttpPut("profile")]
        public async Task<ActionResult> EditProfile([FromForm] ProfileUpdateRequest model)
        {
            int userId = Convert.ToInt32(User.FindFirst(ClaimTypes.NameIdentifier)?.Value);
            var userResponse = await _userManager.UpdateUserInformation(model, userId);
            return Ok(userResponse);
        }

        [HttpGet("profile")]
        public async Task<IActionResult> GetProfileAsync()
        {
            int userId = Convert.ToInt32(User.FindFirst(ClaimTypes.NameIdentifier)?.Value);
            var user = await _userManager.GetUserProfileAsync(userId);
            return Ok(user);
        }
    }
}