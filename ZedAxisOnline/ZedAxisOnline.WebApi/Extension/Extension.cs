﻿
using Microsoft.AspNetCore.Http;

namespace ZedAxisOnline.BusinessLogic.Extension
{
    public static class Extension
    {
        public static void AddApplicationErrors(this HttpResponse response, string message)
        {
            response.Headers.Add("Application-Error", message);
            response.Headers.Add("Access-Control-Expose-Headers", "Application-Error");
            response.Headers.Add("Access-Control_Allow-Origin", "*");
        }
    }
}
