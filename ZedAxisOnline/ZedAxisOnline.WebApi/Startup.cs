﻿using System;
using System.Net;
using System.Text;
using AutoMapper;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using ZedAxisOnline.BusinessLogic;
using ZedAxisOnline.BusinessLogic.Extension;
using ZedAxisOnline.BusinessLogic.Quickbook;
using ZedAxisOnline.Configuration.AppSettingConfiguration;
using ZedAxisOnline.Data.Abstraction;
using ZedAxisOnline.Data.EF;
using ZedAxisOnline.Data.Repository;

namespace ZedAxisOnline.WebApi
{
    public class Startup
    {
        private readonly ILogger<Startup> _logger;
        public Startup(IConfiguration configuration, ILogger<Startup> logger)
        {
            Configuration = configuration;
            _logger = logger;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<ThirdPartyConfigurations>(Configuration.GetSection("ThirdPartyConfigurations"));
            services.Configure<AppSettings>(Configuration.GetSection("AppSettings"));
            services.AddCors();
            services.AddAutoMapper();

            services.AddScoped<ISignInManager, SignInManager>();
            services.AddScoped<IUserManager, UserManager>();
            services.AddScoped<IUserRepository, UserRepository>();
            services.AddScoped<IFieldMappingManager, FieldMappingManager>();
            services.AddScoped<IZohoManager, ZohoManager>();
            services.AddScoped<IAmazonManager, AmazonManager>();
            services.AddScoped<IQuickbookManager, QuickbookManager>();
            services.AddScoped<IQuickbookCompanyInfo, QuickbookCompanyInfo>();
            services.AddScoped<IQuickbookCustomer, QuickbookCustomer>();
            services.AddScoped<IQuickbookDepartment, QuickbookDepartment>();
            services.AddScoped<IQuickbookRepository, QuickbookRepository>();
            services.AddScoped<IInvoiceFieldMapping, InvoiceFieldMapping>();
            services.AddScoped<IMappingRepository, MappingRepository>();
            services.AddScoped<IZohoInvoiceDetailsRepository, ZohoInvoiceDetailsRepository>();
            services.AddScoped<IQuickbookItem, QuickbookItem>();
            services.AddScoped<IQuickbookAccount, QuickbookAccount>();
            services.AddScoped<IQuickbookImportSummary, QuickbookImportSummary>();
            services.AddScoped<IQuickbookClassRef, QuickbookClassRef>();
            services.AddScoped<IQuickbookServiceContext, QuickbookServiceContext>();
            services.AddScoped<IQuickbookTerm, QuickbookTerm>();
            services.AddScoped<IQuickbookImportSummaryRepository, QuickbookImportSummaryRepository>();
            services.AddScoped<ICryptography, Cryptography>();
            services.AddScoped<IQuickbookConnectionDetails, QuickbookConnectionDetails>();
            services.AddScoped<IConnectionManager, ConnectionManager>();
            services.AddScoped<IAmazonConnectionRepository, AmazonConnectionRepository>();
            services.AddScoped<IZohoConnectionRepository, ZohoConnectionRepository>();
            services.AddScoped<IUndoQuickbookInvoice, UndoQuickbookInvoice>();
            services.AddScoped<IQuickbookTransactionTypeRepository, QuickbookTransactionTypeRepository>();
            services.AddScoped<IApplicationDbContextSeeder, ApplicationDbContextSeeder>();
            services.AddScoped<IFunctionManager, FunctionManager>();
            services.AddScoped<IFunctionRepository, FunctionRepository>();
            services.AddScoped<IQuickbookItemSetting, QuickbookItemSetting>();
            


            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                .AddJwtBearer(options =>
                {
                    options.TokenValidationParameters = new TokenValidationParameters
                    {
                        ValidateIssuerSigningKey = true,
                        IssuerSigningKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(Configuration.GetSection("AppSettings:Token").Value)),
                        ValidateIssuer = false,
                        ValidateAudience = false

                    };
                });
            services.AddDbContext<ApplicationDbContext>(x => x.UseSqlServer(Configuration.GetConnectionString("DefaultConnection")));

            // Added Swashbuckle.AspNetCore nuget package manager to support swagger extension.
            services.AddSwaggerGen(options =>
            {
                options.DescribeAllEnumsAsStrings();
                options.SwaggerDoc("v1", new OpenApiInfo
                {
                    Title = "All Plugs APIs",
                    Version = "v1",
                    Description = "This Project contains all apis of the project"
                });
            }
                );

            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1)
                 .AddJsonOptions(opt =>
                 {
                     opt.SerializerSettings.ReferenceLoopHandling =
                     Newtonsoft.Json.ReferenceLoopHandling.Ignore;
                 });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            app.UseExceptionHandler(
                options =>
                {
                    options.Run(
                    async context =>
                    {
                        context.Response.StatusCode = (int)HttpStatusCode.InternalServerError;
                        var error = context.Features.Get<IExceptionHandlerFeature>();
                        if (error != null)
                        {
                            context.Response.AddApplicationErrors(error.Error.Message);
                            await context.Response.WriteAsync(error.Error.Message);
                            _logger.LogError("\n");
                            _logger.LogError(DateTime.Now.ToString());
                            _logger.LogError("\n");
                            _logger.LogError(context.Response.StatusCode.ToString());
                            _logger.LogError("\n");
                            _logger.LogError(error.Error.Message);
                            _logger.LogError("\n");
                            _logger.LogError(error.Error.InnerException.Message);
                        }
                    });
                }
            );


            app.UseSwagger().UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint($"/swagger/v1/swagger.json", "Axis online APIs");
            });

            app.UseCors(config =>
            {
                config.AllowAnyOrigin()
                    .AllowAnyMethod()
                    .AllowAnyHeader();
            });
            app.UseAuthentication();
            app.UseDefaultFiles();
            app.UseStaticFiles();
            app.UseMvc(routes =>
            {
                routes.MapSpaFallbackRoute(
                    name: "spa-fallback",
                    defaults: new { controller = "Fallback", action = "Index" });
            });
        }
    }
}
